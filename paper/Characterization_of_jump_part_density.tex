\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{thmtools}
\usepackage{thm-restate}
\usepackage{cleveref}
\usepackage{etex, etoolbox, ifthen, url,csquotes}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, autopunct=true, authordate]{biblatex-chicago}
\usepackage{placeins}
\usepackage[page]{appendix}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage{csquotes}
\usepackage{doc}


\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}{Lemma}
\pagestyle{plain}

\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\mathbb{V}\mathrm{ar}}
\newcommand{\Cov}{\mathbb{C}\mathrm{ov}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\lequals}{\stackrel{\mathcal{L}}{=}}
\newcommand{\aequals}{\stackrel{a.s.}{=}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\qt}[1]{\lq\lq#1\rq\rq}



\title{Characterization of the Jump Part of the Distribution}
\date{\today}
\author{Paul Sangrey}
\thanks{\textit{Email}: sangrey@sas.upenn.edu}



\begin{document}


\begin{theorem}
Let $J(t)$ be a an $\F$-adapted marked point martingale on $\R_{+}$ having finite first moment measure with mark space $\R$ whose $\F$-compensator $A(t)$ admits and $\F$ intensity $\lambda^{\F}(s, k, \omega)$ such that $J$ is $\lambda-\F$ nonterminating, where $\lambda$ is the Lebesgue measure. 
Let $Q(t)$ be the predictable quadratic variation $\langle J \rangle(t)$. 
Then the following holds. 
 $$ J(t) - J(t-1) \lequals
\begin{cases}
0 &\mbox{w.p }  \exp(-(Q(t) - Q(t-1) )) \\
\N\left(0, Q(t) - Q(t-1)\right)  &\mbox{otherwise}
\end{cases}$$
\end{theorem}

\begin{proof}
Define $a(t,k,\omega) = \int_0^t \lambda_{\N}^{\F}(s, k, \omega) ds$, where $\lambda^{\F}_{\N}$ is the intensity of $A(t)$ with respect to $\N(0,1)$ instead of Lebesgue measure, which exists because the two measures are equivalent. 
Define $\tilde{J}$ to be the rescaled MPP defined to have a point at $(a(t, k, \omega), k)$ if and only if $J$ has a point at $(t,k)$. 
This exists and is a stationary compound Poisson process with unit ground intensity and stationary mark distribution $\N(0,1)$ by Daley \& Vere-Jones Theorem $14.6.1V$. 

Consider the following, 
\begin{gather*}
\int_{\R} \int_{t-1}^t  dJ(t,k)
\aequals \int_{R} \int_{a(t-1, k)}^{a(t, k)}  d\tilde{J}(s,k)
\lequals \int_{R} \int_N \sum_{i=1}^N k_i dPoisson(a(t-1, k) - a(t, k)) \phi(k) dk 
\\ 
\lequals 
\end{gather*}



If $N = 0$, then we are done. 
For all $N > 0$ this is a Gaussian random variable because it is a sum of independent Gaussian random variables. 
In addition, it is clearly mean $0$. 
By the law of total variance and independence of the increments of $\tilde{J}$ this is the expectation of the conditional variance. 
By properties of Poisson distributions that equals $A(t) - A(t-1) = A(t, \R) - A(t-1, \R) = \int_k a(t,k) - a(t-1,k) \phi(k) dk.$
This is simply the compensator for the simple point process $J((0,t] \times \R)$.
However, by Proposition $14.1.VIII$ of Daley \& Vere-Jones, that almost surely equals the predictable quadratic variation of $J$.
Consequently, we have the following, and we are done. 

 $$ J(t) - J(t-1) \lequals
\begin{cases}
0 &\mbox{w.p }  \exp(-(Q(t) - Q(t-1) )) \\
\N\left(0, Q(t) - Q(t-1)\right)  &\mbox{otherwise}
\end{cases}$$
\end{proof}

In the following theorem, we will consider a sequence of inverses of monotonic functions, and will adopt the standard definition.
In other words, $f^{-1}(t) =  \inf \lbrace t : f(s) > t\rbrace$. 

\begin{theorem}
Let $J(t)$ be a pure jump local martingale that is bounded in probability for all $t$ with predictable quadratic covariation: $K(t) = \langle J \rangle(t)$.
Let its intrinsic filtration be denoted $\F^J_t$. 
In addition, define the auxiliary processes $K(s)$ and $T(s)$ such that $J(t) = \sum_{s \leq T^{-1}(t)} K(s)$. 
Assume that $T(t)$ is nonterminating.
Assume there exists a sequence of point processes $T_n(t)$ formed by thinning $T(t)$ to have $n$ jumps with continuous $\F^J_t$-compensators $A_n^T(t)$.   
In addition, assume that $T(t)$ and $K(t)$ are mean independent with respect to $\F^{J}_{t-}$. 
Also assume that the predictable quadratic variation of $K(t)$ $Q^K(t)$ is continuous.
Then $J(N^{-1}(t)) \lequals L(t)$, where $L(t)$ is a Laplace process with  drift $0$ and scale parameter $b = \frac{1}{\sqrt{2}}$. 
\end{theorem}


\begin{proof}
We proceed by a series of time changes.
First we fix $n$, and then we rescale the time axis so that the increments of $T_n(t)$ are i.i.d $\exp(1)$. 
Then we consider $K(s)$ as a process in this new space, which we then rescale by its predictable quadratic variation.
Then using a functional martingale central limit theorem, we show that this must be a Weiner process even though we rescaled by the predictable quadratic variation and not the true quadratic variation.
To recover the original process, we integrate out the additional information given by the first time change by noticing that the increments of the original process can be represented as $\exp(1) \N(0,1)$.
This holds for each $n$, and so it holds in the limit and we are done.

Define $T_n^{\dagger}(t) = T_n(\langle T_n \rangle^{-1}(t))$. 
Now $T_n(t)$ is a simple point process with intrinsic history $\F^{T_n}_t$, and so by Proposition 14.6.III of Daley \& Vere-Jones, $T_n^{\dagger}(t)$ is a unit-rate Poissoon process on $\R_{+}$. 
In other words, the increments of $T_n(t)$ are i.i.d $\exp(1)$. 

First, we define $K^{\dagger}(t) = K(PQ^{-1}(t))$, and then we rescale the space so that we can identify this process with a Wiener process.
Note, since we are rescaling by a continuous, predictable process the relevant filtrations are unaffected probabilistic results concerning limits.
Let $\F_{t-}^{K, T^{-1}}$ be the filtration generated by the the $K(t-)$ augmented such that the $T_n^{-1}(t)$ are $\F_{t-}^{K, T^{-1}}$  are stopping times for all $n$.
Since $K^{\dagger}(s)$ and $T(s)$ are mean independent on $\F^J_{t-}$, the conditional means on this space do not change.
Clearly, the requisite stopping times still exist, and so $K^{\dagger}(t)$ is a local martingale with respect to $\F_{t-}^{K,T^{-1}}$
Consider the time-changed process $\tilde{K}^{\dagger}_n(t) = K^{\dagger}(T_n^{-1}(t))$. 
Now, the local martingale property is closed under stopping and since the jumps of $\tilde{K}^{\dagger}(s)$ are the same as the the jumps in $J(s)$ which is almost surely bounded, the supremum of the jumps is uniformly integrable. 

Note that $J(t)$ is bounded in probability, and when we rescale the time axis we do not affect this at all, and so $\tilde{K}^{\dagger}_n(t)$ is also bounded in probability for all $n$. 
Clearly $\lim_{a \uparrow \infty} \limsup_{n} P^{K^{\dagger}_n}(\vert x \vert 1_{\vert x \vert > a} \ast [0,t) > \eta) \leq \lim_{a \uparrow \infty} \limsup_{n} P^{J}(\vert x \vert 1_{\vert x \vert > a} \ast [0,t] > \eta) = 0$ for all $\eta > 0, t > 0$, since $J(t)$ is bounded in probability for all $t$, where $P^{K^{\dagger}_n}$ and $P^{J}$ are the probability measures implied by $K^{\dagger}_n(t)$ and $J(t)$.
It is worth noting that the $t$ variable changes on each side of the first inequality but because the second statement holds for all $t$, that does not matter.
In addition, in the transformed space there are almost surely only finitely many values of $\tilde{K}^{\dagger}t_i)$ which are pinned down in each finite interval.
Consequently, by replacing each value $\tilde{K}^{\dagger}(t_i)$ by duplicates of itself that are rescaled to keep the predictable quadratic covariatoin the same, we can define a process that is almost surely uniformly bounded over intervals in the rescaled space.  
Define $A^K(t) = \langle K \rangle(t)$, which we assumed to be continuous.

We have a local martingale that is almost surely bounded with predictable covariation equal to $t$.
Consequently, $\tilde{K}_n^{\dagger}(t) \stackrel{\L}{\to} W(t)$ where $W(s)$ is a Weiner process on this space by the martingale functional central limit theorem as in Jacod \& Shiryaev Theorem 3.12. 
However, they clearly almost surely converge to $\tilde{K}^{\dagger}(t) := K^{\dagger}(T^{-1})(t)$. 
Consequently $\tilde{K}^{\dagger}(t) \lequals W(t)$. 


However, what we would actually like is the distribution of $J(N^{-1})(t)$, instead of the transformed $K(t)$ and $T(t)$.
So we now turn to characterizing $J(N^{-1})(t)$ in terms of those two processes. 
The tricky part here is that $T^{-1}(t)$ is not a stopping time with respect to the $\F^{t-}$ because it not predictable. 
Instead, consider the following 

$$\lim_{n \to \infty} N^{-1}(A^K(A^T_n(t)) $$

The predictable quadratic variation of a local martingale is simply $\E\left[M(t) \middle| \F^M_{t-} \right]$. 
In a pure jump process as considered here, this can increase by either having more jumps or by having a greater size per jump. 
In other words, $N_n(t) := \langle \int_0^t k(s) 1_{T(s) =t} dT_n(s) dK(s) \rangle $ simply the composition of $A^K$ and $A^T_n$.
Clearly, since the $T_n(t) \to T(t)$, $N_n(t) \to N(t)$, and hence the above limit converges to $t$. 

Now, that we have characterized the time changes, we turn to the distribution. 
Consider the characteristic function of the increment of $J(N^{-1}(t))$.
Clearly, its predictable quadratic variation equals $1$, and its mean equals $1$.
Note, we can replace a variable defined by the process by a variable with the same distribution. 

\begin{gather*}
\E\left[ \exp(i a \left(J(N^{-1}(t_1) - J(N^{-1}(t_0) \right) \middle\vert \F_{t_0} \right] 
= \E\left[ \exp\left(i a \sum_{t_0 < s \leq t_1} J(N^{-1}(s)) \right) \right] \\
=  \E\left[ \exp\left(i a \sum_{t_0 < s \leq t_1} J(A^{-K}(A^{-T}(s)) \right) \right] 
= \E\left[ \exp\left(i a \int_{t_0}^{t_1} \int_K k dA^k(k) dA^t(s) \right) \right]  \\
=  \E\left[ \exp\left(i a \int_{t_0}^{t_1} \N(0,1) dA^{-T}(s) \right) \right] 
=  \E\left[ \exp\left(i a \sum_{t_0 < s \leq t_1} \N(0, A^{-T}(s)) \right) \right] \\
=  \E\left[ \exp\left(i a \int_{t_0}^{t_1} \N(0,1) dA^{-T}(s) \right) \right] 
= \E\left[ \exp(\left( i a \sqrt{t_0 - t_1} \sqrt{\exp(1)} \N(0,1) \right) \right] 
\end{gather*}

However, the last part is the characteristic function of a $\mathcal{L}$aplace distribution. 
In other words, $\int_{t_0}^{t_1} dJ(N^{-1}(t)) \sim \mathcal{L}\mbox{aplace}\left(0, \frac{1}{4} (t_1 - t_0) \right)$. 
This holds for all increments and so $J(N^{-1}(t)) = \mathcal{L}(t)$ where $\mathcal{L}(t)$ is a Laplace martingale with scale parameter $b = \frac{1}{4}$.

\end{proof}

\end{document}


