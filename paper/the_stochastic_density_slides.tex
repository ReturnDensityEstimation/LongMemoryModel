\documentclass[fleqn,t,smaller,xcolor=dvipsnames]{beamer}
\usetheme{Warsaw}
\usecolortheme{dolphin}
\usepackage{datetime}
\usepackage{graphicx}
\usepackage{sidecap}
\usepackage{commath}
\usepackage{mathtools}
\usepackage{caption}
\usepackage{siunitx}


\setbeamertemplate{navigation symbols}{}   % Turns of the navigation symbols in the footer.
\setbeamertemplate{headline}{}      % Turns of the section/subsection names in the header.
\captionsetup{labelformat=empty}
\defbeamertemplate*{footline}{shadow theme}
{%
  \leavevmode%
  \hbox{\begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm plus1fil,rightskip=.3cm]{author in head/foot}%
    \usebeamerfont{author in head/foot}\hfill\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm,rightskip=.3cm plus1fil]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle\hfill\insertframenumber\,/\,\inserttotalframenumber
  \end{beamercolorbox}}%
  \vskip0pt%
}


\newcommand\itembullet{
  \begingroup
  \leavevmode
  \usebeamerfont*{itemize item}%
  \usebeamercolor[fg]{itemize item}%
  \usebeamertemplate**{itemize item}%
  \setlength\labelsep   {\dimexpr\labelsep + 0.5em\relax} 
  \endgroup
}
\newcommand{\spitem}{\vspace{.3cm}\item}


\AtBeginSection[]{
{
  \setbeamertemplate{footline}{}
  \begin{frame}
  \addtocounter{framenumber}{-1}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}
}


\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\mathbb{V}\mathrm{ar}}
\newcommand{\Cov}{\mathbb{C}\mathrm{ov}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\lequals}{\stackrel{\mathcal{L}}{=}}
\newcommand{\aequals}{\stackrel{a.s.}{=}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\F}{\mathcal{F}}


\title[The Stochastic Density]{The Stochastic Density: Characterization, Identification, and Estimation}

\author[Sangrey]{Paul Sangrey}
\institute{University of Pennsylvania \\ \textit{sangrey@sas.upenn.edu}}

\date{April 11, 2016}

\begin{document}

\beamertemplatenavigationsymbolsempty

{
\setbeamertemplate{footline}{}
\begin{frame}[c]
\maketitle
\end{frame}
}
\addtocounter{framenumber}{-1}


\begin{frame}[c]{Preview of results}
\begin{itemize}
	\item Develop the density analogue $SD_t$ of the quadratic variation $\left[Y\right]_t$, which we
	term the stochastic density.
	\item Show that $SD_t = \N\left(\mu_t, \sigma^2_t\right)$ for a stochastic mean $\mu_t$ and stochastic 
	volatility $\sigma^2_t$ under only mild technical conditions. 
	\item Show that $\sigma^2_t \aequals \left[Y\right]_t$ if sample paths are continuous and characterize
	the partial identification of $\mu_t$. 
	\item Model the dynamics of $\mu_t$ and $\sigma^2_t$. 
	\item Estimate the time-varying conditional mean of asset returns rigorously taking into account the partial
	identification.
	\item Use Bayesian methods to semiparametrically estimate $SD_t$, $\sigma^2_t$, $\mu_t$, and the 
	resulting predictive distribution $PD_t$.
\end{itemize}
\end{frame}

%\section{Question}



\begin{frame}[c]{Price process}
Let $Y(t)$ be a locally-bound special It\^{o} semimartingale.
Then $Y(t)$ has the following form. 


$$Y(t) = Y_0 + \int_o^t b(s) d(s) + \int_o^t \sigma(s) dW(s) + \int_0^t \int_E \delta(s, x) (\mu - \nu) (ds, dx) $$

\end{frame}

\begin{frame}[c]{Separating statics and dynamics}
\begin{itemize}
	\item Considered individually $Y(t+1) - Y(t)$ have distributions $SD_t$. 
	\item Define $\Omega_t$ as the the information set generated by $SD_{\tau}, \tau \leq t$.
	\item Clearly, $SD_t$ is measurable with respect to $\Omega_t$, but $SD_{t+1}$ likely is not.
	\item $PD_{t} = \int_{\Omega_{t+1}\vert\Omega_{t}}  SD_t $.
\end{itemize}
\end{frame}


\begin{frame}[c]{The object of interest}
\begin{itemize}
	\item $PD_t$ is the primary object of interest in the risk estimation literature.
	\item Essentially all risk measures are statistic of it.
	\item It is also closely related to the physical measure of the asset pricing literature. (The precise
	relationship will be discussed later.)
\end{itemize}
\end{frame}

\begin{frame}[c]{What has been done}
\begin{itemize}
	\item Parametric models
	\begin{itemize}
		\item Work reasonably well in practice.
		\item Ignore risk arising from model misspecification -- ``black swan events"
	\end{itemize}
	\item Quantile estimation (CAViaR)
	\begin{itemize}
		\item Works reasonably well if you want Value-at-Risk (VaR) measures.
		\item You probably want more than just VaR.
	\end{itemize}
	\item Extreme Value Theory
	\begin{itemize}
		\item Only works with data that are i.i.d. after a known parametric transformation.
	\end{itemize}
\end{itemize}
\end{frame}

%\section{Stochastic Density}


\begin{frame}[c]{What do we know about $SD_t$?}
\begin{itemize}
	\item $SD_t \sim \N(\mu_t, \sigma^2_t)$.
	\item If sample paths are continuous $\sigma^2_t = \int_{t-1}^t \sigma^2(s) ds$.
	\item $\mu_t$ is only partially identified -- drift terms are not identified from high frequency data.
\end{itemize}
\end{frame}

\section{Relationship between high-frequency and daily-frequency}

\begin{frame}[c]{Time-changing martingales}
\begin{theorem}[Dambins Dubins \& Schwarz]
	\label{Dambins Dubins Schwarz}
	Let $M(t)$ be a continuous local martingale with $M_0 = 0$ and $\left[ M \right]_{\infty} = \infty$.
	Then there exists a Wiener process $W(s)$ such that for the change of time $T(t) = \left[ M(t) \right]
	$, i.e. $T(t) = \left[ M \right]_t, M(t) \aequals W(T(t))$. 
\end{theorem}
\begin{theorem}[Monroe's Theorem]
	Let $M(t)$ be a right-continuous martingale. Then there exists a Wiener process $W(t)$ with filtration 
	$\mathcal{G}_t$  and a family of $\mathcal{G}_t$-measurable stopping times $T(t)$ such that the process 
	$W(T(t))$ has the same finite-dimensional joint distributions as $M(t)$. The family $T(t)$ is right-
	continuous for all $s$, increasing, and for each t, $T(t)$ minimal. Moreover, if $M(t)$ has stationary, 
	independent increments, so does $T(t)$. 
\end{theorem}
\end{frame}


\begin{frame}[c]{Time-changing martingales}
\begin{itemize}
	\item Essentially, you can stretch and shrink the time axis so that the process has the right variance.
	\item The martingale property gives you a zero instantaneous mean
	\item You are doing the continuous-time version of representing a fat-tailed distribution by a scale-mixture of 
	normals.
	\item This representation holds for very general martingales.
	\item You do not need continuity.
\end{itemize}
\end{frame}

\begin{frame}[c]{Time-changing semimartingales}
\begin{corollary}
Let $Y(t)$ be a special semimartingale whose local martingale part is a right-continuous martingale, and $Y(0) = 0$. 
Then there exists a predictable process of integrable variation $B(t)$, a Wiener process $W(t)$ with filtration $\mathcal{G}_t$ and a family of $\mathcal{G}_t$for all-measurable stopping times $T(t)$, such that $Y(t)$ and $B(t) + W(T(t))$ have the same finite-dimensional distributions. 
\end{corollary}
\begin{itemize}
	\item Proof follows from splitting the semimartingale into a martingale part and a drift part and 
	time-changing the martingale part.
\end{itemize}
\end{frame}



\begin{frame}[c]{Representing discrete-time densities}
\begin{corollary}
Let $Y(t)$ be a special semimartingale whose local martingale part is a right-continuous martingale, and $Y(0) = 0$. 
Then $Y_t  \lequals \N(B(t), T_t)$ for some family of stopping times $T(t)$. 
\end{corollary}
\begin{itemize}
	\item Proof follows from integrating the martingale part of the process in the new time. Increments of 
	Weiner process are normally distributed.
	\item Then you add the drift term. 
	\item You are doing the continuous-time analog of representing a distribution by a mixture of normals.
\end{itemize}
\end{frame}


\begin{frame}[c]{Characterization of $T(t)$}
\begin{itemize}
	\item Under continuity, $T(t) = IV(t) = \int_0^t \sigma^2(s) ds$.
	\item Therefore, you can estimate $T(t)$ by the realized volatility.
	\item In general, $T(t)$ is not measurable with respect to the filtration generated by the increments 
	of the Wiener process $\mathcal{F}^W_t$ and hence is not identified.
	\item However, $\mathbb{E}\left[QV_t\right] = \mathbb{E}\left[T\right]$ very generally.
	\item Therefore, if you use a realized volatility estimator, you should get reasonable results even if you
	 misspecify the price process as continuous.
\end{itemize}
\end{frame}

\begin{frame}[c]{Consistently estimating $SD_t$ up to location}

\begin{theorem}
	Define $p(x,y) = \frac{1}{\sqrt{2 \pi y}} \exp\left(-\frac{1}{2} \frac{x^2}{y}\right)$. 
	Let $Y(t)$ be a continuous semimartingale with martingale part $M(t)$ such that $\left[ M 
	\right]_{\infty} = \infty$, and $Y(0) = 0$. 
	Let $\tau$ be a fixed time such that $\left[ M \right]_{\tau} > 0$. 
	Let $\Delta^{n} = \lbrace 0 = t_1^n < \cdots < t_n^n = \tau\rbrace$, such that $\lim_{n \to \infty} 
	\sup \vert t_i^n - t_{i-1}^n \vert  \to 0$. 
	Define $RV_{\tau}^n = \sum_{i \geq 1} (Y_{t_i} - Y_{t_{i-1}})^2$. 
	Then $p(x, RV_{\tau}^n) \stackrel{u.c.p.}{\to} p(M_{\tau})$, where $p(M_{\tau})$ is the density of
	$M_{\tau}$. 
\end{theorem}

The proof follows from $SD_t = \N(\mu, \sigma^2_t) = \mu_t + \N(0, \sigma^2_t)$. 
We can consistently estimate $\sigma^2_t$. 
The normal density can be bounded on positive open balls as a function of the variance, and so you can construct an $\epsilon-\delta$ argument.

\end{frame}


%\section{Estimating the Time-Varying Expected Return}


\begin{frame}[c]{Estimating $\mu_t$} 
\begin{itemize}
	\item $\mu_t$ is a stochastic drift term -- which are not identified.
	\item So we cannot consistently estimate it.
	\item However, they are partially identified.
\end{itemize}
\end{frame}


\begin{frame}[c]{Characterization of the density of $\mu_t$}
\begin{itemize}
	\item Assume that the realization of the price process is independent of $\mu_t$ and $\sigma^2_t$, i.e.
	the error term distributions in the model's discrete representation are independent.
	\item Then $Y_t\vert \sigma^2_t, \mu_t  \sim \N(\mu_t, \sigma^2_t)$.
	\item This implies $\mu_t\vert Y_t, \sigma^2_t \propto \N(Y_t, \sigma^2_t) p(\mu_t \vert
	\sigma^2_t)$. 
\end{itemize}
\end{frame}


\begin{frame}[c]
\frametitle{Estimating $\mu_t$}
\begin{itemize}
	\item In other words, $Y_t \sim \N(\mu_t, \sigma^2_t)$ functions as a ``likelihood".
	\item Thus Bayesian methods will give valid inference for $\mu_t$ handling the lack of identification well.
	\item Do we have a prior for $\mu_t$?  
	\begin{itemize}
		\item Yes, that's what a predictive distribution is. 
	\end{itemize}
	\item If we allow for correlation between $\mu_t$ and $\sigma^2_t$, that will show up the prior as well, as we 
	allowed for on the previous slide.
\end{itemize}
\end{frame}


\begin{frame}[c,label=model]{Conservative credible sets for $\mu_t$}
\begin{itemize}
	\item Since the Bayesian posterior accurately reflects the knowledge that we have, it will lead to correct 
	credible sets.
	\item This will be conservative in the sense that if we knew $\mu_t$ we would have smaller intervals.
	\item However, we don't know $\mu_t$, and so we don't want smaller intervals.
\end{itemize}
\end{frame}



\begin{frame}[c]{Physical measure}
\begin{itemize}
	\item The physical measure $PM_t$ is one of the key components of asset pricing. 
	\begin{itemize}
		\item If you also have the risk-neutral measure (which you can get from option prices), you can compute the 
		stochastic discount factor. 
	\end{itemize}
	\item $PM_t$ is the density measure of risk faced by practitioners. 
	\item Presumably, econometricians and practitioners have the same information sets. 
	\item $PM_t = \int_{\mu_t} PD_t p\left(\mu_t \vert \Omega_t'\right) d\mu_t$.
	\item Bayesian methods will deliver a consistent estimate of this if you use all the useful information 
	available
	\item We are estimating this, but we are not using all possible information. 
\end{itemize}
\end{frame}


\section{Model}

\begin{frame}[c]
\frametitle{Stochastic volatility}
\begin{itemize}
	\item This has been studied in great detail.
	\item Therefore, we can simply adapt what is already in the literature.
	\item Andersen et. al. (2001) argue that quadratic variation is approximately log-normal and has 
	long-memory.
	\item As a result, we should do reasonably well by approximating the dynamics by a linear, log-memory normal
	 model for $\log(\sigma^2_t)$ similar to a long-memory EGARCH model.
\end{itemize}
\end{frame}

\begin{frame}[c]{Stochastic volatility}
\begin{itemize}
	\item One can approximate a long-memory model by a small number of dispersed lags.
	\item We adapt the HAR model of Corsi (2009) by using the same lags as he does, but since our
	model is daily, we only use the stochastic volatility at those periods. We also add a second daily
	lag for robustness.
\end{itemize}
\end{frame}

\begin{frame}[c]{Likelihood}
\begin{itemize}
	\item The true likelihood of the data contains an infinite-dimensional nuisance parameter and has an unknown 
	form.
	\item In addition, the estimation procedure is already quite computationally intensive.
	\item Thus, we adopt the qausi-MLE approach argued for by Ait-Sahalia et.al. (2006) and further explored by 
	Xiu (2010). 
	\item They characterize the true likelihood if we assume that the volatility is predetermined and show that the 	resultant MLE estimator (which is just the realized volatility) is consistent rather generally at the optimal 
	rate $n_t^{1/4}$, where $n_t$ is the number of observations in day $t$.
\end{itemize}
\end{frame}

\begin{frame}[c]{Stochastic volatility}

$$\left\lbrace Y_i \right\rbrace_{i=1}^{n_t} \sim \N\left(0, \sigma^2_t (t_i - t_{i-1}) \right) $$
$$ \log(\sigma^2_t) = \beta_0 + \begin{pmatrix} \beta_1 \\ \beta_2 \\ \beta_7 \\ \beta_{30} \end{pmatrix}' \log\begin{pmatrix}\sigma^2_{t-1} \\ \sigma^2_{t-2} \\ \sigma^2_{t-7} \\ \sigma^2_{t-30} \end{pmatrix} + \sigma_{\sigma} \eta_t  $$
$$\eta_t \stackrel{i.i.d}{\sim} \N(0,1) $$
\end{frame}


\begin{frame}[c]{Stochastic mean}
\begin{itemize}
	\item This has not been studied nearly as extensively.
	\item It cannot be heavily correlated because then it would cause the returns to be correlated by a law of 
	iterated expectations argument.
	\item The level and variance are likely correlated. 
	\item Therefore, we use a simple model that allows for autoregressive terms and contemporaneous correlation with 	the stochastic volatility.
	\item Whether you let the leverage depend on log-volatility or the volatility itself does not matter that much.
\end{itemize}
\end{frame}

\begin{frame}[c]{Stochastic mean}
$$\mu_t = \gamma_0 + \gamma_{\sigma} \log(\sigma^2_t) + \gamma_1 \mu_{t-1} + \sigma_{\mu} u_t $$
$$u_t \stackrel{i.i.d}{\sim} \N(0,1)$$
\end{frame}

\section{Estimation}

\begin{frame}[c]{Data}
\begin{itemize}
	\item Arguably the most important index of financial activity is the S\&P 500.
	\item Use the SPY (SPDR S\&P 500 ETF Trust) to imitate it.
	\item It is also worth noting that SPY is by far the most liquid ETF especially in recent years, which should 
	reduce the effect of market microstructure.
	\item We downloaded the prices from TAQ.
\end{itemize}
\end{frame}

\begin{frame}[c]{Data cleaning}
\begin{itemize}
	\item We prefiltered the trades to remove all ones marked as invalid.
	\item We then downsampled the index to $3-$second increments, and filtered away a $MA(1)$ noise term using a 
	Kalman filtering algorithm.
	\item We then downsampled again to $450$ equally-spaced observations per day, which on days where the exchange 	
	was open for the full day is one per minute.
\end{itemize}
\end{frame}

\begin{frame}[c]{SPY log-returns}
\begin{figure}[t!]
	\includegraphics[width=4in]{figures/data.png} 
\end{figure}
\end{frame}




\begin{frame}[c]{Estimation procedure}
\begin{itemize}
	\item Used a standard Metropolis-Hastings within Gibbs sampling algorithm.
	\item To estimate the integrated volatility we use the likelihood as a proposal distribution because the model 
	is not conjugate, but the data are informative, which seems to work well.
	\item The process is like most Bayesian procedures in that it is rather computationally intensive.
\end{itemize}
\end{frame}

\section{Results}


\begin{frame}[c]{Stochastic volatility}
\begin{table}[t!]
\begin{tabular}{lccc} \hline
	Parameter & Prior & \multicolumn{2}{c}{Posterior} \\
	& &  Median & 90\% Interval \\ 
	\hline \hline
	& & \multicolumn{2}{c}{Coefficient} \\
	$\beta_0$  & $\N(-2, 25)$ & $-0.56$ & $[-0.59, -.53]$ \\
	$\beta_1$  & $\N(.5, 1)$   & $.48$ & $[.47, .49]$ \\ 
	$\beta_2$  & $\N(0,1)$    & $0.28$ & $[.27, .29]$ \\
	$\beta_7$  & $\N(0,1)$    & $0.14$ & $[0.13, .14]$ \\
	$\beta_{30}$ & $\N(0,1)$    & $0.05$ & $[0.05, 0.05]$ \\
	& & \multicolumn{2}{c}{Innovation Variance} \\
	$\sigma_{\sigma}^2$ & $\Gamma^{-1}(3, .03)$ & $0.30$ & $[.29, .32]$ \\
	\hline
	\hline
\end{tabular}

{\footnotesize
{\em Notes:} Dates: 2005-2012, number of simulations: 3000 }\setlength{\baselineskip}{4mm}
\end{table}
\end{frame}


\begin{frame}[c]{Stochastic mean}
\begin{table}[t!]
\begin{tabular}{lccc} 
	Parameter & Prior & \multicolumn{2}{c}{Posterior} \\
	\hline
	& &  Median & 90\% Interval \\ 
	\hline \hline
 	& & \multicolumn{2}{c}{Coefficient} \\
	$\gamma_0$        & Flat      & $\num{3.0  e-4}$  & $[\num{-2.5 e-3}, \num{2.9 e-3}]$ \\
	$\gamma_1$        & $U(-1,1)$ & $\num{-4.6  e-2}$ & $[\num{-9.4 e-2}, \num{1.2 e-3}]$ \\
	$\gamma_{\sigma}$ & Flat      & $\num{1.8  e-5}$ & $[\num{-2.4 e-4}, \num{2.6 e-4}]$ \\  
	\hline 
	& & \multicolumn{2}{c}{Innovation Variance} \\
	$\sigma^2_{\mu}$ & $\Gamma^{-1}(3, .03)$ & $\num{6.7 e-5}$ & $[\num{6.3 e-5}, \num{7.1 e-5}]$ \\
	\hline
	\hline
\end{tabular}

{\footnotesize
{\em Notes:} Dates: 2005-2012, number of simulations: 3000 }\setlength{\baselineskip}{4mm}
\end{table}
\end{frame}


\begin{frame}[c]{Stochastic volatility}
	\begin{minipage}{.49\textwidth}
		\centering
		Estimate
		\includegraphics[width=2.3in]{figures/stochastic_vols_estimated.png}
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		\centering
		Forecast
		\includegraphics[width=2.3in]{figures/stochastic_vols_forecasted.png}
	\end{minipage}
\end{frame}


\begin{frame}[c]{Stochastic mean}
	\begin{minipage}{.49\textwidth}
		\centering
		Estimate
		\includegraphics[width=2.3in]{figures/stochastic_means_estimated.png}
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		\centering
		Forecast
		\includegraphics[width=2.3in]{figures/stochastic_means_forecasted.png} 
	\end{minipage}
\end{frame}

\begin{frame}[c]{Predictive density}
	\begin{minipage}{\textwidth}
		\centering
		Forecast \\
		\includegraphics[width=4in]{figures/stochastic_density_forecasted.png}
	\end{minipage}
\end{frame}

\begin{frame}[c]{Probability integral transform}
	\begin{minipage}{.49\textwidth}
		\centering
		Density
		\includegraphics[width=2.3in]{figures/stochastic_density_pit.png}
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		\centering
		Autocorrelation 
		\includegraphics[width=2.3in]{figures/stochastic_density_pit_autocorr.png} 
	\end{minipage}
\end{frame}

\section{Future Work}

\begin{frame}[c]{Simulation results and horse-races}
\begin{itemize}
	\item How robust is this procedure to various specifications of the data generating process?
	\item How does this quantitatively relate to the various procedures already in the literature?  
	\item We are assuming continuity to identify the model. Is there a way of relaxing this assumption without 
	losing identification
	\item How would other potential specifications for the dynamics affect the results?
\end{itemize}
\end{frame}

\begin{frame}[c]{Multivariate extensions}
\begin{itemize}
	\item Risk and asset pricing are a multivariate problems. 
	\item If you orthogonalize the data and then time-change them, you recover a sequence of i.i.d Wiener processes.
	Thus, you should be able to extend this model to the multivariate case in a straightforward fashion.
	\item You should be able to use cross-sectional variation to identify more of the process.
\end{itemize}
\end{frame}

%\section{The Stochastic Density and Realized Volatility}
%
%
%\begin{frame}[c,label=realizedvolcomp, noframenumbering]
%\frametitle{Realized Volatility Literature}
%\begin{itemize}
%	\item Earlier, in discussing nonparametric risk estimation I brushed over the realized volatility literature.
%	\item However, it is arguably the most successful branch of the literature to consider such a topic.
%	\item I was just postponing the discussion to here.
%	\item This paper is very a conscious attempt to generalize that literature to the case of the entire density.
%\end{itemize}
%\end{frame}
%
%
%\begin{frame}[c,noframenumbering]{Realized Volatility's key contribution}
%\begin{itemize}
%	\item The early paper's key contribution was that you did not need a parametric model to estimate the 
%	volatility, you just needed high-frequency data.
%	\item The finite sample quadratic variation converged to the population object very generally.
%	\item The quadratic variation was equal to the integrated volatility.
%	\item However, what is the discrete-time interpretation?
%\end{itemize}
%\end{frame}
%
%
%\begin{frame}[c,noframenumbering]{Volatility of what?}
%\begin{itemize}
%		\item If we assume that the integrated volatility is measurable with respect to the previous day's 
%		information set, this is precisely the variance of the daily return (i.e. it is predetermined).
%		\item However, this implies that the daily return is normally-distributed. This is clearly counterfactual.
%		\item So we drop the predetermined assumption, we can generate fat-tailed distributions rather easily.
%		\item So in general, $SD_t$ is the distribution for which the integrated volatility is the variance.
%\end{itemize}
%\end{frame}
%
%
%\begin{frame}[c,noframenumbering]{Discrete-time interpretation}
%\centering
%Consider the following discrete-time model, where $r_t$ is the day $t$ return.
%$$r_t = \mu_t + \sigma_t \eta_t $$
%$$\mu_t = h(\mu_{t-1}, \mu_{t-2}, \ldots, \sigma^2_{t}, u_t) $$
%$$\sigma^2_t  = g(\sigma_{t-1}^2, \sigma^2_{t-2}, \ldots, \epsilon_t) $$
%$$\eta_t, u_t, \epsilon_t \stackrel{\mbox{indep.}}{\sim} (0,1) $$
%\end{frame}
%
%
%\begin{frame}[c,noframenumbering]{Discrete-time interpretation}
%\begin{itemize}
%	\item The above model is very general. It allows for leverage effects, non-linear dynamics, non-normality and so 
%	on.
%	\item $r_t, \sigma^2_t,$ \& $\mu_t$ are all contemporaneously determined.
%	\item The quadratic variation is $\sigma^2_t$.
%	\item The $SD_t$ is the distribution of $r_t$. 
%	\item By the above arguments, w.l.o.g. $SD_t = \N(\mu_t, \sigma_t^2)$.
%\end{itemize}
%
%\hyperlink{model}{\beamerreturnbutton{Model}}
%\end{frame}
%
\end{document}



