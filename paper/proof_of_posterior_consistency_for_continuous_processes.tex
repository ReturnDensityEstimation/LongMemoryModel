\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{thmtools}
\usepackage{thm-restate}
\usepackage{cleveref}
\usepackage{etex, etoolbox, ifthen, url,csquotes}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, autopunct=true, authordate]{biblatex-chicago}
\usepackage{placeins}
\usepackage[page]{appendix}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage{csquotes}



\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\pagestyle{plain}

\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\mathbb{V}\mathrm{ar}}
\newcommand{\Cov}{\mathbb{C}\mathrm{ov}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\lequals}{\stackrel{\mathcal{L}}{=}}
\newcommand{\aequals}{\stackrel{a.s.}{=}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\qt}[1]{\lq\lq#1\rq\rq}
\newcommand{\X}{\mathcal{X}}


\title{Proof of Posterior Consistency}
\date{\today}
\author{Paul Sangrey}
\thanks{\textit{Email}: sangrey@sas.upenn.edu}
%\addbibresource{density_estimation.bib}


\begin{document}

\begin{theorem}
Let $\phi$ be the Gaussian density and $g$ the transition density of the $V_t$ process.
Assume that $Y_t$ has continuous price-paths and $B_t = A\log(V_t)$.
Then we can represent the the true density $f_{0,t}$ as follows. 
$$f_{0,t}(x) = \int_{z} \phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right) g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) dz$$

\end{theorem}

\begin{theorem}

Define $\Pi^f_{n,t}\left(f_t \middle\vert \mathcal{X}\right)$ as the posterior density over $f_t$ implied by the following.

$$f_t(x) = \int_{\lbrace V_t \rbrace } \int_A \int_G \int_{z} \phi\left(\frac{x - A\log(z)}{\sqrt{z}} \right) g\left(z\middle\vert V_{t-1} \ldots V_{t-k} \right) dz d\Pi^G_{t}(g \vert \lbrace V_t \rbrace ) 
d\Pi^A_{t}(A | \lbrace V_t \rbrace, \mathcal{X}) d\Pi^{\lbrace V_t\rbrace}_{m}(\lbrace V_t \rbrace | \mathcal{X}) $$

Let $B_{\epsilon}$ be an $L_{1}$ neighborhood of $f_{0,t}$. 
That is $B_{\epsilon} = \left\lbrace f_t \in \F_d :  \int_x \left\vert f_t(x) - f_{0,t}(x) \right\vert dx \leq \epsilon \right\rbrace$ where $\F_d$ is the space of densities.
Assume that $\Pi^G_t$ and $\Pi^A_t$ are strongly consistent and that $\Pi^H_m$ is uniformly consistent in probability, i.e. $\Pi^H_{m}\left(\left\lbrace V_t : \sup_{t} \left\vert V_t - V_{0,t} \right\vert < \delta_H \right\rbrace \middle\vert \X \right) \to_{p} 1$. 
Then $\lim_{t,m \to \infty} \Pi_{t,m}^{f}(B_{\epsilon}) \to_p 1$ for all $\epsilon > 0$. 
\end{theorem}


\begin{proof}
Let $\alpha > 0$ and $\epsilon > 0$ be given. 
If we have different number of observations $m_d$ for different days $d$, we can simply consider the $\min_d m_d$, so we can assume without loss of generality that they are all the same.
Note, that strong consistency of $\Pi_t^A$ implies that all draws $A$ from $\Pi^A_t$ satisfy $A \aequals A_0 + o(t)$ and uniform convergence in probability for $\lbrace V_t \rbrace$ implies that for all $V_t$ on a set $\mathcal{V}$ with probability at least $1 - \alpha_1$ with respect to $p\left(\lbrace V_t, \X \rbrace\right)$ satisfy  $V_t = V_{0,t} +o_p(m)$.
For the remainder of the proof, will assume that we are on this set. 
Consider the $L_1$ distance between the true density $f_{0,t}$ and a draw
from its prior $\Pi^f_{t,m}$. 

\begin{align*}
&\int_x \bigg\vert \int_{z} \phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right) g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) dz \\
&- \int_{\lbrace V_t \rbrace } \int_A \int_G \int_{z} \phi\left(\frac{x - A\log(z)}{\sqrt{z}} \right) g\left(z\middle\vert V_{t-1} \ldots V_{t-k} \right) dz d\Pi^G_{n,i}(g \vert \lbrace V_t \rbrace ) 
d\Pi^A_{n,t}(A | \lbrace V_t \rbrace, \mathcal{X}) d\Pi^{\lbrace V_t\rbrace}_{n,t}(\lbrace V_t \rbrace | \mathcal{X})  \bigg\vert dx \\
&\aequals \int_x \int_z \bigg\vert \phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right) g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) \\
&- \int_G \phi\left(\frac{x - (A_0 + o(t)) \log(z)}{\sqrt{z}} \right) g\left(z\middle\vert V_{0, t-1} + o_p(m) \ldots V_{0, t-k} + o_p(m) \right) d\Pi^G_{n,i}(g \vert \lbrace V_t + o_p(m) \rbrace ) \bigg\vert dz dx \\
\end{align*}


We note that the density above $\phi(\cdot)$ is uniformly continuous as a function of $A$ given $x$ and $z$. 
In addition, we are evaluating the two terms at the same $x$ and $z$.
Hence, their difference is is $o(t)$. 
Thus, we can rewrite the above equation as follows. 
We also note that $\Pi^G$ and $\Pi^A$ converge almost surely with respect to $\X$, and that for large enough $m$,$V_{0,t-j} + o_p(m)$ are clearly in the domain of $\X$.
So we assume without loss of generality, we are in the domain of $\X$.

\begin{align*}
&\hspace{.5cm} \int_x \int_z \bigg\vert \phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right) g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) \\
&- \int_G \left(\phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right)  + o(t) \right) g\left(z\middle\vert V_{0, t-1} + o_p(m) \ldots V_{0, t-k} + o_p(m) \right) d\Pi^G_{t,m}(g \vert \lbrace V_t + o_p(m) \rbrace ) \bigg\vert dz dx \\
&\leq \int_x \int_z \phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right) \bigg\vert  g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) - \int_G  g\left(z\middle\vert V_{0, t-1} + o_p(m) \ldots V_{0, t-k} + o_p(m) \right)  \\
&\hspace{.5cm} d\Pi^G_{n,i}(g \vert \lbrace V_t + o_p(m) \rbrace )  \bigg\vert dz dx  + o(t, m)\\
\end{align*}


We note that the Gaussian cdf is bounded by $1$ for all values of its arguments.
Hence $\int_x \phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right) dx <= 1$.
We also note that $g$ is a uniformly bounded continuous function, and so it is
uniformly continuous.
Therefore, the above expression is bounded above by the following. 

\begin{align*}
&\hspace{.5cm} \int_z \bigg\vert  g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) - \int_G  g\left(z\middle\vert V_{0, t-1} + o_p(m) \ldots V_{0, t-k} + o_p(m) \right) d\Pi^G_{t,m}(g \vert \lbrace V_t + o_p(m) \rbrace )  \bigg\vert dz + o(t,m)\\
&= \int_z \bigg\vert  g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) - \int_G  g\left(z\middle\vert V_{0, t-1}\ldots V_{0, t-k}\right)d\Pi^G_{t,m}(g \vert \lbrace V_t + o_p(m) \rbrace )  \bigg\vert dz + o_p(m) + o(t,m)\\
\end{align*}

We know that $dP^G_{t,m}$ converges almost everywhere.
Consequently, the fact that we are using $\lbrace V_t + o_p(m)\rbrace$ instead of $\lbrace V_{0,t} \rbrace$ does not affect the convergence of the estimator as long as $\lbrace V_t + o_p(m)$ is in the domain $\X$ which it will be asymptotically with arbitrarily high probability.
Consider a set $\mathcal{V}$ of $V_{t-1} \ldots V_{t-k}$ with positive probability.
Define a set $S_{\epsilon} = \lbrace g_t : \int | g_{0,t}(\cdot |z_{t-1} \ldots z_{t-k} - g_t(\cdot | z_{t-1} \ldots z_{t-k} | \nu(z_{t-1}\ldots z_{t-k} d(z_{t-1}) \ldots z_{t-k}) > \epsilon \rbrace$ where $\nu$ is the unconditional distribution of $V_{t-1} \ldots V_{t-k}$. 
Strong convergence of $\Pi^G_{t,m}$ means $\Pi^G_{t,m}(S_{\epsilon}) \to 0$ almost surely.
This implies that $\hat{g} \to g_0$ in the $L_1$ norm for any set of $V_{t-1} \ldots V_{t-k}$ with positive probability under $\nu$. 
Let $\mathcal{C}$ be a set such that $\nu(\mathcal{C}) \geq 1 - \alpha_2$. 
Assume that we are in $\mathcal{C}$.
Then clearly since $\nu$ is bounded away from zero, we have the following. 

\begin{gather*}
\int_z \left\vert g_0(z | V_{0,t-1} \ldots V_{0,t-k}) - \hat{g}(z | V_{0, t-1} \ldots V_{0, t-k}) \right\vert \nu(V_{0, t-1} \ldots V_{0,t-k})  dz \\
\implies \int_z \left\vert g_0(z | V_{0,t-1} \ldots V_{0,t-k} - \hat{g}(z | V_{0, t-1} \ldots V_{0, t-k} \right\vert dz
\end{gather*}

However, $\Pi^G_{m,t}$ converges strongly and so we have the previous result almost surely on $\mathcal{C}$.

Then since $\int_z \left\vert g_0(z | V_{0,t-1} \ldots V_{0,t-k} - \hat{g}(z | V_{0, t-1} \ldots V_{0, t-k} \right\vert dz < \epsilon$  almost everywhere with respect to $\Pi^G_{m,t}$ for large enough $m,t$, clearly this difference is bounded on average.
So if we choose $\alpha_1$ and $\alpha_2$ such that $(1 - \alpha_1)(1 - \alpha_2) > (1 - \alpha)$, we have with probability at least $\alpha$ that the initial difference is $o_p(m,t) + o_p(m) + o_p(t,m)$. 



We note that his holds for any draw from the posterior.
Consequently by the above argument for large enough $m$ and $t$, all draws from the prior are in $B_{\epsilon}$ are asympotitically aribitrarily close to the truth in the $L_1$ norm with probability at least $1 - \alpha$. 
In other words, $\lim_{t,m \to \infty} \Pi^f_{m,t}(B_{\epsilon}) \to_p 1$ for all $B_{\epsilon}$. 
\end{proof}

\end{document}