\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{etex}
\usepackage{etoolbox}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage{csquotes}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, authordate]{biblatex-chicago}
\usepackage{placeins}


\addbibresource{density_estimation.bib}


\theoremstyle{definition}
\newtheorem{Ques}{Question}
\theoremstyle{plain}
\newtheorem{Thm}{Theorem}
\pagestyle{plain}


\title{Weekly Return Distributions: Exploiting the Two Infinities}
\author{Paul Sangrey}
\date{August 28, 2015}

\begin{document}



\begin{abstract}
I generalize the realized volatility literature by estimating the time-varying distribution of log-returns in each week instead of just the variance.
I model this distribution as a Gaussian scale mixture, which is the distribution that the majority of parametric models in the stochastic volatility literature give rise to.
By allowing the mixing proportions to vary, but requiring the component distributions to remain constant, I am able to parsimoniously capture a large amount of the complexity of the time-varying distribution. 
The variance estimates that this procedure results in align closely realized volatility estimates. 
The model separates the weekly variances into components of different scales and shows that the volatility is mostly driven by extreme events -- 52.7\% of the variance is driven by the 2.2\% of log-returns drawn from the distribution with the largest variance.
\end{abstract}



\maketitle	

\section{Introduction}
One of the central questions in the macroeconomics and finance is how to parsimoniously model  the evolution of macroeconomic and financial aggregates. 
Some of the most successful models, both structural and reduced form, especially in finance, have modeled the realized variables as draws from a Gaussian distribution with a time-varying variance.
For example, the GARCH model of \cite{Bollerslev1986Gene} and its successors as well a number of the long-run risks models such as \cite{bansal2012long} have this form.  


A related literature by Andersen and others approach the same problem from a different angle. 
They do not model the parametric evolution of the stock return process at all. 
Instead, they exploit the availability of high-frequency data, the second infinity of asset returns, to construct measures of volatility as the average intraperiod squared deviation. 
Essentially among other successes, they are able to better measure the time-varying volatility and, at least in some applications, the noisier measure, the squared return no longer predicts well.

With respect to forecasting one way of viewing the realized volatility literature is simply a parsimonious way of summarizing the distribution intraperiod returns, as period return itself is included in the estimation. 
However, this simply raises the question -- is this the right low-dimensional summary of the data? 
If as, the parametric literature assumes, the process is Gaussian-distributed with stochastic variance, then yes; the Gaussian distribution is completely parameterized by its first two moments. 
One substantial caveat applies though, you must being run the model right frequency. 
If the distribution is a Gaussian scale mixture, but the stochastic volatility moves at a higher frequency than is assumed this will no longer hold. 
Furthermore, if the true distribution is not a Gaussian scale mixture, then this distributional assumption will clearly not work, and we know it is not for Gaussian scale mixtures are symmetric and the data are not. 

In viewing the problem in this way, we are thinking about estimating the time-varying distribution as a two-fold process process. 
First, at what frequency does this process vary, and second how do we estimate the returns in each period? 
As mentioned above, several of the most successful models are Gaussian scale mixtures run various frequencies.  


\section{What Frequency?}

The previous section left the question of the proper frequency at which the scale of the model changes unaddressed. So answer this question, we can turn to  another strain of literature that models the distribution of log-returns directly. 
By doing so, we get related, but not identical results. 
Universally, people find that a Gaussian distribution does not work very well.
Perhaps the most commonly used distribution is Skewed-T distribution.
(See \cite{panayiotis1998financial} and \cite{lee2013advances} for references.)
However, this brings us back to a Gaussian mixture model because a Skewed-T distribution can be thought of as a mixture distribution where the variance and mean are functions of parameters drawn from an inverse-gamma distribution.
The Standard-T distribution, which is a special case, is simply a Gaussian scale mixture. 
However, this model has a different variance drawn for each datapoint.  
In other words, the correct frequency to run the model is at the same frequency as the data. 
One can do this when one is modeling daily returns by using daily realized volatility, but as you consider higher frequency returns, your ability to do this rapidly diminishes. 

However, although Skewed-T distributions provide relatively good fit to the data they are a somewhat ad-hoc.  
The Gaussian distribution was not fat-tailed nor did it allow for skewness, and so we added parameters so that we could fit those features of the data.
Perhaps the biggest competitor to the Skewed-T distribution in modeling these returns is the Asymmetric Laplace distribution.
Again it provides relatively good fit to the data, though by no imeans a perfect one.
Interestingly, enough the Asymmetric Laplace distribution can be represented as a Gaussian scale-skew mixture, where this time the mixing parameters are drawn from an Exponential distribution. 
To be more precise,\citeauthor{kotz2012laplace} show that an Asymmetric Laplace random variable $Y$ can be decomposed as follows where $Z \sim N(0,1)$ and $W \sim \exp(1)$. 

$$Y \sim N(\mu + \gamma W, \sigma^2,W)$$

One can also generalize this further and allow $W$ to be drawn from a $Gamma$ distribution instead. 

If we ignore the asymmetry in log-returns for the moment, we recover the standard Laplace distribution, that is $y \sim N(\mu, \sigma^2 W)$.  
Again, we have a model where random variables are drawn from a mixture distribution, but the frequency at which the variance is evolving is the same frequency at which the returns themselves are being realized.

Interestingly, the Laplace distribution is perhaps easier to motivate theoretically. 
One of the most useful theorems in statistics literature and the fields that have built upon it, such as economics, is the central limit theorem. 
Normally, this approximation holds reasonably well once we have 100,200 datapoints. 
If we consider 10 minute returns there are about 170 different returns in a given week, but the limiting distribution is certainly not Normal.
Why is this?

One possibility is that the individual variables do not have more than two finite moments. 
However, this assumption is usually thought of as rather innocuous, and it would imply that the realized variance isnot measuring something meaningful.


So consider another characterization of the limiting problem.  
The number of summands in the partial sums in the statement of the central limit theorem is deterministic. 
However, financial time does not run at the same time as calendar time.
If we think about the decision to buy and sell as occurring at random times or if we think about the expected profitability evolving according to some random process, we would be following much of the economcis literature but we no longer have a deterministic number of summands in any given period. 
In addition, if we simply take the number of times the return changes in a given period, we will get different numbers in the different days; in other words,  we have an empirical distribution over the number of summands.
However, this imeans that in this environment we should be considering random sums not deterministic ones.

Interestingly, this brings us back to the Laplace distribution. 
If we consider a sequence of non-degenerate identically-distributed, independent random variables with finite variance then the following central limit theorem holds, for some sequence of constants $\alpha_p$ where $\nu_p \sim Geometric(p)$, \cite[37]{kotz2012laplace}. 

$$\lim_{p \to 0} \alpha_p \sum_{i=1}^{\nu_p} Y_p^{(i)}  \stackrel{d}{\to}  Laplace$$

There are two main issues with the above characterization of asset returns.
First, in practice the returns do not look like they are Laplace distributed; for example, they are skewed. 
Second, there is no reason to think that the number of terms is distributed geometric.
It is almost certainly more complicated than that, and a function of time. 
We can generalize the distribution over the number of summands slightly thereby producing Asymmetric Laplace and Generalized Asymmetric Laplace distributions.
Sadly, however, no general characterization of the limit of random summations exists as far as this author know. 
However, the literature that does exist does seem to imply that the characterization of the limiting distribution as a Gaussian scale-skew mixture does hold much more generally. 
For example, we can actually drop the assumption of finite variance in the theorem above without losing the result that the limiting distribution is a Gaussian mixture, which arises as a corollary of theorem in \cite{kozubowski1998mixture}.
Various other papers such as  \cite{Mittnik1993Modeling} and \cite{anderson1993linnik} have also discussed relevant results. 




\section{Data}

I use the Wharton Research Data Services NYSE Trade and Quote database (WRDS TAQ) data. 
This data  contains intraday transactions data (trades and quotes) for all securities listed on the New York Stock Exchange, American Stock Exchange, and NASDAQ National Market System, as well as SmallCap issues.

I compute the intraday market return on the S\&P 100 at the 10 minute frequency. 
I log-difference the prices for each stock, and then simply average across all of the stocks. 
In other words, I compute the 10-minute log-return on an equally-weighted portfolio of the S\&P 100. 


\begin{figure}[thb!]
\includegraphics[width=4in]{../Plots/mkt_rtn_2013.png}
\caption{Log Market Return}
\end{figure}


On a week when the exchange were open for the entire week, this gives 170 ten-minute observations.  
In weeks when the exchange was only open for part of the week, there are fewer, but only one week has less than 100 observations. 
In total, there are about 8500.  

In the entire discussion above, I have not really discussed the mean of the distribution.
Clearly, the mean return in a period is not constant over time.
However, if, as commonly done, we assume that prices are a random walk, then log-differencing results in the the weekly mean simply being white-noise. 
Hence, it should be relatively innocuous to simply demean the data within each period, and so I do this. 


\FloatBarrier
\section{The Model} 

As argued above, it is pretty clear we want a model where log-returns are drawn from a Gaussian mixture. 
I would prefer to use scale-skew mixtures; however, I as of this time I have been unable to do that and so I follow most of the literature and use a scale mixture instead. 

The other big modeling assumption concerns the distribution of the variances. 
As discussed above many different distributions have been considered including the gamma, inverse-gamma, exponential and so on.
We also have papers that argue that realized volatility is distributed log-normal, and so maybe one should use this distribution instead. 
As of the moment, I simply avoid this assumption by estimating the variances directly. 
However, this does come with the cost where I cannot have too many components while still maintaining tractability. 
Ideally, I would estimate the number of mixture components necessary to fit the data well, perhaps by using a Dirichlet process. 
However, as of this moment, I do not do that and instead simply fix it at some reasonable number. 

As far as notation goes, I let $T$ be the total number of observations, $D_t$ be the number of observations in each week, and the $i$-superscripts denote which component a particular observation comes from, with $I$ denoting the total number of components. 
I also use capital letters to refer to the set of all variables with the accompanying lowercase letter.
For example, $X$ refers to the vector of all returns. 


The generative itself model is simple. 
The likelihood takes the standard scale Gaussian mixture form and is as follows, where the returns are denoted $x_{d_t}$. 

$$x_{d_t} \sim \sum_{i} p_t^i \frac{1}{\sigma_i} \phi\left(\frac{x_{d_t}}{\sigma_i}\right)$$


The other key component in my model is that I assume that although the mixture proportions vary across time, they are drawn independently from the same distribution.
This distribution I parameterize, as is standard, using a Dirichlet distribution. 
In other words, I assume that probabilities are drawn as follows. 

$$\left\lbrace p_t^1, \ldots p_t^I \right\rbrace_{t=1}^T \sim \mathcal{D}\left(\dirichlet_param^1 \ldots \dirichlet_param^I \right)$$

To estimate this model, I adopt a practice common in the literature and add an auxiliary variable $z_{d_t}$ for each $x_{d_t}$. 
This $z_{d_t}$ variable simply records which component the relevant mixture is drawn from. 
Using those variables, we can rewrite the model in terms of the $z_{d_t}$ as below.
Where $\delta(\cdot)$ is simply the identity function or can be thought of as a Kronecker's delta. 
The distributions listed are simply the priors that I place upon the various parameters. 


\begin{align*}
I &\sim Fixed = 5 \\
\left(\alpha_1, \ldots, \alpha_I \right) &\sim Flat > 0 \\
\lbrace \sigma^2_i \rbrace  & \sim Flat > 0 \\
\left\lbrace p^1_t,\ldots p^I_t\right\rbrace &\sim \mathcal{D}\left( \dirichlet_param^1,\ldots, \dirichlet_param^I\right) \\
z_{d_t} &\sim Categorical\left(p^1_t,\ldots p^I_t\right) \\
x_{d_t} &\sim \prod_{i=1}^{I} (p^i_t)^{\delta(z_{d_t} = i)} \frac{1}{\sigma_i} 
\phi\left(\frac{x_{t,d}}{\sigma_i }\right)\\ 
\end{align*}


\subsection{Conditional Posterior of $p_t^i$} 


First, I start by deriving the conditional posterior of $p_t^i$ so that I can draw from the posterior distribution using a Gibbs sampler.   
To simplify the notation, let $n^i_t = \sum_{d_t} \delta(z_{d_t} = i)$. 


\begin{align*}
f(P| A, Z) &\propto 
f(P, Z | A) \\
&= \prod_{t}  \left( f(p^1_t\ldots p^I_t  | \dirichlet_param^1 \ldots \dirichlet_param^I ) \prod_{d_t} f(z_{d_t} | p^1_t \ldots p^i_t ) \right)\\
&\propto \prod_{t}  \left( \prod_{i} \left( (p_t^i)^{\dirichlet_param^i -1} \right) \prod_{d_t} \prod_{i} (p_t^i)^{\delta(z_{d_t} = i)} \right)\\
&= \prod_{t}  \prod_{i} (p_t^i)^{\dirichlet_param^i + n^i_t -1}  \\
\end{align*}


It is worth noting that this implies that $P_t | A, Z \sim \mbox{Dirichlet}(\dirichlet_param^1 + n^1_t, \ldots, \dirichlet_param^I + n^I_t)$. By integrating out $p_t^i$ and  using the fact that $P_t \sim \mathcal{D}$,  we can get the distribution of $Z | A$.
This simplifies the distribution above because I no longer have to estimate $p_t^i$ within the Gibbs sampler. 
The $z_{d_t}$ now follow a Dirichlet-Multinomial distribution, and so the resultant likelihood as the following form. 

$$P(Z | A) = \prod_{t} \left( \frac{\Gamma(\sum_i \dirichlet_param^i)}{\Gamma(D_t + \sum_i \dirichlet_param^i )} \prod_{i} \frac{\Gamma(n_t^i + \dirichlet_param^i)}{ \Gamma(\dirichlet_param^i)} \right)$$



\subsection{Conditional Posterior distribution of $z_{d_t}$}
The conditional posterior of $z_{d_t}$ can be written as follows. 
In deriving this I use the fact that conditional distribution of $z_{d_t}^i$ given $A$ is independent of observations in different weeks. 
Having done that, you simply drop all the terms that do not contain $z_{d_t}$, and simplify using properties of the Gamma function.
This gives the following where $(n_t^i)^{-d_t} = \sum_{g_t \neq d_t} \delta(z_{g_t} = i)$.


$$P(z_{d_t} = i | Z^{(-d_t)}, A) \propto (n_{t}^i)^{(-d_t)} + \dirichlet_param^i $$


This allows me to derive the conditional posterior probability $Pr(z_{d_t} = i | X,A)$.


\begin{align*}
P(z_{d_t} = i | Z^{(-d_t)}, X, A, \Sigma) &\propto  p(X | Z^{(-d_t)}, z^i_{d_t}, A, \Sigma) p(z^i_{d_t} = i| Z^{-d_t}, A)\\ 
&= p(z_{d_t} = i | Z^{(-d_t)}, A) p(X |Z,A, \Sigma) \\
&\propto \left((n_{t}^i)^{(-d_t)} + \dirichlet_param^i \right) (\sigma_i^2)^{-\frac{1}{2}} \exp\left\lbrace - \frac{1}{2} \sum_{d_t} \sum_i \left( \frac{\delta(z_{d_t} =i) x_{d_t}^2}{\sigma_i^2} \right) \right\rbrace \\
&\propto \left((n_{t}^i)^{(-d_t)} + \dirichlet_param^i \right)(\sigma_i^2)^{-\frac{1}{2}} \exp \left\lbrace - \frac{ x_{d_t}^2}{2 \sigma_i^2} \right\rbrace
\end{align*} 

\subsection{Conditional Posterior distribution of $\sigma^2_i$}

Since I am using Gaussian mixtures, and each distribution in the mixture has a different $\sigma^2_i$, this part of the problem, conditional on the $\lbrace z_{d_t}\rbrace$, is simply drawing the variance in a distribution with known mean, which rather easy because I can use the fact that the conjugate relationship between the Gaussian distribution and the flat prior over $\sigma^2_i$. 
Because there is no assumed prior relationship between the various parameters except for $Z$ and $P$, and the likelihood is factorizable over $\sigma^2_i$ in terms of $i$. Let $n^i = \sum_t \sum_{d_t} \delta(z_{d_t} = i)$, that is $n^i$ is simply the number of elements in component $i$. 


\begin{align*}
P(\sigma^2_i | X, \Sigma_{-i}, P, X) &= P(\sigma^2_i | X, Z) \\
&\propto (\sigma^2_i)^{-\frac{1}{2} \sum_t \sum_{d_t} \delta(z_{d_t} = i) } 
\exp\left\lbrace -\frac{1}{2 \sigma^2_i} \sum_t \sum_{d_t} \delta(z_{d_t} = i) x_{d_t}^2 \right\rbrace \\
&\implies \sigma^2_i \sim \mbox{Inverse-Gamma}\left( \frac{n^i -2}{2}, \frac{1}{2} \sum_t \sum_{d_t} \delta(z_{d_t} = i) x_{d_t}^2 \right) 
\end{align*}



\subsection{Conditional Posterior distribution of $\dirichlet_param^i$}

I first note that the conditional posterior of  $A | (Z = A | Z, X, \Sigma)$ simply equals the posterior distribution of $A$ given $Z$ because A enters the likelihood of $X$ only through its connection with $Z$.
To ease calculation and computational stability, we derive the conditional log-likelihood instead. 

\begin{align*}
P(A | Z) &\propto P(Z | A) \\
&= \log\left( \prod_t \left( \frac{\Gamma(\sum_i \dirichlet_param^i)}{\Gamma(D_t + \sum_i \dirichlet_param^i)}  \right) \prod_i \frac{\Gamma(n_t^i + \dirichlet_param^i)}{\Gamma(\dirichlet_param^i)} \right) \\
&= T \log\Gamma\left(\sum_i \dirichlet_param^i\right)- \sum_t \log\Gamma\left(D_t + \sum_i \dirichlet_param^i\right) + \sum_t \sum_i \log \Gamma\left(n_t^i + \dirichlet_param^i\right) - T \sum_i \log\Gamma\left(\dirichlet_param^i\right)
\end{align*}

This distribution is not of standard form and cannot be drawn from directly. 
However, one can simply use a Metropolis-Hastings step instead. 
To do this, one has to evaluate this likelihood distribution many times, which can be done efficiently by using the algorithm in \cite{sklar2014fast}.




\section{Estimation and Results}

\subsection{Estimation}
The estimation procedure itself is rather straightforward I simply use a Metropolis-Hastings within Gibbs sampler to draw from the conditional distributions derived above. 

The first main difficulty with the estimation is a lack of identification.
The particular assignment of labels to the various components does not affect the likelihood in any way.
However, this is a relatively easy problem to solve; one simply orders the various components by their scale.

The other issue is theoretically less of an issue, but leads to more practical difficulties. 
The only difference between the various components is their scale, and all of the component distributions actually have the same peak.
In fact, the likelihood only depends on $x_{d_t}^2$, not on the level at all. 
It is true that if we consider a return with a very large deviation, it likely comes from a component distribution with a large scale.
However, if we consider a return with a small deviation, then since all of the component distributions have a peak at zero, since the data was demeaned, it may have come from any of the distributions.

However, since we are using a finite mixture to approximate an infinite one, and do not care about the components directly. 
This is not be a particularly big problem. 
This will result in some level of spurious correlation in the posterior between the $p_i$'s and the $\sigma_i$'s.  
However, as long as we care about the distribution itself, and not the particular structural parameters that drive it,this correlation should arise in a way to fit the distribution of the data. 


\subsection{Results}

The main result is that we replicate the realized volatility estimation while  breaking down the weekly distributions into their components.
This allows me to break the realized volatility down into its components. 
By doing this I show that the volatility, as many people would expect, is primarily driven by tail events. 

In the following table, I present the posterior mean for both the distribution over $\alpha_i$ and $\sigma_i$. 
The $\alpha_i$ govern the distribution from which the probabilities are drawn from.
The relative proportions of the various $\alpha_i$'s governs the probabilities that are drawn, while their average value governs how dispersed the various probabilities are. 
In addition, to make the posterior distributions easier to interpret, I also report the mean of the $p^i_{d_t}$ over both the draws and time. 

\begin{table}[htb!]
\caption{Posterior Means}
\begin{tabular}{cccccc}
& 1 & 2 & 3 & 4 & 5\\
\hline
$\sigma$ &  0.26 & 0.54 & 0.66 & 1.35 & 5.11 \\
$\dirichlet_param$ & 12.55 & 18.71 & 15.43 & 6.57 & 1.41 \\
$p$ & 0.23 & 0.33 & 0.27 & 0.13 & 0.02 \\
\end{tabular}
\end{table}


Gaining a better appreciation how the distribution evolves over time is somewhat harder to consider. 
As a result, I provide two graphs. 
The first is simply multiplies the variance of each component by its time-varying weight, distributing the weekly variance of the distribution into its components. I also graph the realized volatility for the sake of comparison. 

As can be seen the total variance and realized volatility tend to run together quite closely.
In addition, as mentioned in the abstract,the component with the highest variance drives 52.7\% of the variance even though only 2.2\% of the values come from this distribution.
The other main variable that evolves across time is the proportions.
Hence, I present a graph of the proportions in each period labeled by the accompanying standard deviation. 


\begin{figure}[htb!]
\caption{The time-varying distribution}
\begin{tabular}{cc}
The proportions & The weighted variances \\
\includegraphics[width=3in]{../Plots/proportion_across_time.png}&
\includegraphics[width=3in]{../Plots/proportion_weighted_var.png}
\end{tabular}	
\end{figure}

As previously alluded to, since scale mixtures of guassians are just weighted symmetric distributions with all the same mean, the resultant distribution will also be symmetric. 
Hence, this particular construction has no way of duplicating the time-varying skewness in the data. 
However, the construction does allow for excess kurtosis, which I have with the average estimated kurtosis is 14.87.
However, the average realized kurtosis is 22.27.  
In addition, as can be seen from the graph below, my estimate of the weekly kurtosis is much less volatile than that in the realized kurtosis is.


\begin{figure}[htb!]
\caption{Realized and estimated time-varying kurtosis}
\includegraphics[width=3in]{../Plots/proportion_weighted_kurtosis.png}
\end{figure}

There are a couple potential explanations for this, which I will pursue in further research.
One is our model does not allow for time-varying skewness, potentially this is messing up our estimate of the kurtosis as well.
It is also possible that we do not have enough components because I am trying to approximate an infinite mixture model by a finite one. 
One major extension of this paper would be to allow the data to pick the correct number of components perhaps by using Dirichlet processes instead of finite Dirichlet distributions. 
It is also in general harder to estimate the kurtosis than the variance with a given number of data points and so it is possible that this is messing up either our estimation using the model or using the realized kurtosis methods. 


\section{Conclusion}

\subsection{Future Work}
With regard to research in general, many potential interesting avenues remain.
To do this project in a more robust way, one has to allow for skewness in the distribution.
The best way to do this is likely to use Gaussian skew-scale mixtures.
In addition, one is trying to approximate an infinite mixture model by using only a finite number of components.
In general, one would want to estimate this number by using a Dirichlet process one is able to expand upon this paper in a straightforward way and thereby do that.

In addition, the dynamics of the time-varying process were not modeled. 
It would likely be of great interest to do this, possibly trying to connect them to  evolution in fundamental macroeconomic processes. 
However to do this well since macroeconomic processes evolve at a lower frequency than financial ones do, one would need to use more than just a year of data.
However, all of the above ideas would significantly increase the computational difficulty making the resultant problem rather non-trivial. 

As a more general statistical statement, a characterization the limiting behavior of random sums in an environment general enough to account for most of the financial variables would potentially provide for reasonable strictly parametric approximations of the distribution. 

\subsection{Final Thoughts}

To wrap everything  up, modeling the time-varying distribution of weekly return using a Gaussian scale mixture provides a parsimonious method to capture the time-varying nature of the distribution of log-returns. 
By using a small number of components, I am able to replicate the realized variance estimation, and show that the majority of it comes from tail events, and would be expected. 
A great deal of further work needs to be done in order to capture the weekly distributions well, estimate the intertemporal dynamics, and connect the movement to macroeconomic fundamentals. 

\newpage

\nocite{*}
\printbibliography

\end{document}


