function output = jqr_preavg1d_olsqr(dymat,dzmat,un,kn,knp,gfun,TruncJump,TruncCont,qlist,nboot,prebeta)
% This function implements the JQR preaveraging approach in the case with
% 1-dim regressor. We always compute the OLS. The qlist specifies the
% quantile list for compute the QR estimates.
%
% Syntax:
%
% output = jqr_preavg1d_olsqr(dymat,dzmat,un,kn,knp,gfun,TruncJump,TruncCont,qlist,nboot/prebeta)
% 
% dymat: n x m LHS matrix, n is the number of returns each day, m is the number
% of days
%
% un: pre-computed truncation threshold. If empty, the code will compute
% using available data.
%
% dzmat: n x m RHS matrix
%
% kn: preaveraging window
%
% knp: spot variance block size
%
% gfun: preaveraging kernel
%
% TruncJump, TruncCont: Truncation for jumps and continuous
% 
% qlist: the list of quantile regressions
%
% nboot: the number of MC trials for getting CIs, set to zero if CIs are
% not needed.
%
% prebeta: the preliminary beta estimate for computing the CIs. If empty,
% use the OLS estimate.

n = size(dymat,1);
qlist = qlist(:)';

% Record inputs to outputs 
output.TruncJump = TruncJump;
output.TruncCont = TruncCont;
output.n = n;
output.kn = kn;
output.knp = knp;
output.qlist = qlist;
output.nboot = nboot;
%% Compute preaveraged returns
zbar = jqr_preavgret(dzmat,kn,gfun);
ybar = jqr_preavgret(dymat,kn,gfun);

%% Set up a truncation threshold for the preaveraged returns
if isempty(un)
    un = jqr_truncation(zbar,kn);
end

% Output
output.zbar = zbar;
output.ybar = ybar;
output.un = un;

%% Vectorizing
zbar_vec = zbar(:);
ybar_vec = ybar(:);
un_vec = un(:);

%% Jump detection
JI = find(abs(zbar_vec) > TruncJump * un_vec); % Indices for jumps
if isempty(JI)
    % If there are no detected jumps, set key outputs to empty
    output.olsci90 = NaN(2,1);
    output.olsci95 = NaN(2,1);
    output.olsci99 = NaN(2,1);
    % qr results
    if ~isempty(qlist)
        output.bqr_corrected = NaN(1,length(qlist));
        output.qrci90 = NaN(2,length(qlist));
        output.qrci95 = NaN(2,length(qlist));
        output.qrci99 = NaN(2,length(qlist));
    end    
    output.jump_cluster = [];
    return;
end
% The detected jump preavg returns
zbar_jump = zbar_vec(JI);
ybar_jump = ybar_vec(JI);

%% The OLS and QR estimates
bols = (zbar_jump'*ybar_jump)/(zbar_jump'*zbar_jump);
bqr = zeros(size(qlist));
if ~isempty(qlist)
    for i = 1:length(qlist)
        bqr(i) = rq_fnm(zbar_jump,ybar_jump,qlist(i));
    end
end

% Output the point estimates
output.bols = bols;
if ~isempty(qlist)
    output.bqr = bqr;
else
    output.bqr = [];
end


%% Clustering the detected jump returns
jump_cluster = jqr_group(JI,kn); % jqr_group performs the clustering
L = size(jump_cluster,1); % number of clusters

% Estimate the jump sizes for each cluster
deltaZ_hat = zeros(L,1); % Estimated jump size for each cluster
deltaY_hat = zeros(L,1);
for loop = 1:L
    % Verion 1.  
    % For each cluster, get the preaveraged returns
    zbar_cluster = zbar_vec(jump_cluster(loop,1):jump_cluster(loop,2));
    ybar_cluster = ybar_vec(jump_cluster(loop,1):jump_cluster(loop,2));
    % We need to know the number of preavg returns in the cluster to
    % compute the finite-sample adjustment
    nobs_cluster = jump_cluster(loop,3);
    index1 = floor((kn-nobs_cluster)/2);
    index2 = index1 + nobs_cluster - 1;
    jlist = index1:index2;
    deltaZ_hat(loop) = sum(zbar_cluster) / sum(gfun(jlist/kn));
    deltaY_hat(loop) = sum(ybar_cluster) / sum(gfun(jlist/kn));
end

% Output
output.deltaZ_hat = deltaZ_hat;
output.deltaY_hat = deltaY_hat;
output.jump_cluster = jump_cluster;

%%%%%%%%%%%%%%%%%%%%%%%%% Code below compute the confidence intervals %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Not needed for point estimate %%%%%%%%%%%%%%%%%%%%%%%%%

if nboot == 0
    return;
elseif isempty(prebeta)
    prebeta = bols;
end

%% Estimate the spot covariances
% Collect the start and end indices for all clusters
Jmin = jump_cluster(:,1);
Jmax = jump_cluster(:,2);
np = n - kn + 1;
LastOfDay = ceil(Jmax/np)*np;
FirstOfDay = LastOfDay - np + 1;
% The starting index for the left variance
LeftStart = max([Jmin - kn - knp + 1, FirstOfDay],[],2);
% The ending index of the right variance
RightEnd = min([Jmax + kn + knp - 1, LastOfDay],[],2);

% The residual process
du = dymat - dzmat * prebeta;
[ubar,uhat] = jqr_preavgret(du,kn,gfun);
ubar_vec = ubar(:);
uhat_vec = uhat(:);

% The truncation for the residual process
vn = jqr_truncation(ubar,kn);
vn_vec = vn(:);

% Continuous Indicators
truncall = (abs(ubar_vec) <= TruncCont * vn_vec);

% Compute the spot variances
spotvar = zeros(L,4); %[cleft, cright, Aleft, Aright]
glist = gfun((0:kn)/kn);
sumg2 = sum(glist.^2); % \sum_{j} (gn(j))^2
sumgp2 = sum(diff(glist).^2); % sum_j (gn'(j))^2;
for loop = 1:L
    % Looping over all clusters
    % Set up the indices for left and right variances
    LI = (LeftStart(loop):(LeftStart(loop) + knp - 1));
    RI = ((RightEnd(loop) - knp + 1) : RightEnd(loop));
    % Compute left variances
    tmpubar = ubar_vec(LI);
    tmpuhat = uhat_vec(LI);
    tmptrunc = truncall(LI);
    cleft = sum((tmpubar.^2 - tmpuhat/2) .* tmptrunc) ...
            * n / sum(tmptrunc) / sumg2;
    Aleft = sum(tmpuhat/2 .* tmptrunc) / sum(tmptrunc) / sumgp2;
    
    % right
    tmpubar = ubar_vec(RI);
    tmpuhat = uhat_vec(RI);
    tmptrunc = truncall(RI);
    cright = sum((tmpubar.^2 - tmpuhat/2) .* tmptrunc) ...
            * n / sum(tmptrunc) / sumg2;
    Aright = sum(tmpuhat/2 .* tmptrunc) / sum(tmptrunc) / sumgp2;
    spotvar(loop,:) = [max(cleft,0), max(cright,0), Aleft, Aright];
end

%% Simulation Inference
hols_boot = zeros(nboot,1);
if ~isempty(qlist)
    hqr_boot = zeros(nboot,length(qlist));
end

parfor i = 1:nboot
    % We draw the diffusive and the noises for all clusters at once
    
    % Draw diffusive
    dw_mc = randn(2*kn-3,L) / sqrt(n); % Brownians
    dw_mc(1:(kn-2),:) = repmat(sqrt(spotvar(:,1))',kn-2,1) .* dw_mc(1:(kn-2),:); % left
    dw_mc((kn-1):end,:) = repmat(sqrt(spotvar(:,2))',kn-1,1) .* dw_mc((kn-1):end,:); % right
    % Draw noise
    noise_mc = randn(2*kn-2,L);
    noise_mc(1:(kn-1),:) =  repmat(sqrt(spotvar(:,3))',kn-1,1) .* noise_mc(1:(kn-1),:);
    noise_mc(kn:end,:) =  repmat(sqrt(spotvar(:,4))',kn-1,1) .* noise_mc(kn:end,:);
    dnoise_mc = diff(noise_mc);
    % Simulated residual returns
    r_mc = dw_mc + dnoise_mc; % 2kn-3 x L
    % Preaveraing
    rbar_mc = zeros(kn-1,L);
    glist = gfun((1:(kn-1))/kn)';
    for loop = 1:L
        rbar_mc(:,loop) = conv(r_mc(:,loop),glist,'valid');
    end
    rbar_mc_vec = rbar_mc(:) * n^(1/4); % vectorize the preavg residual returns, normalized by 1/4
    deltaz_mc = glist * deltaZ_hat';
    deltaz_mc_vec = deltaz_mc(:);
    
    % Now, regress rbar_mc_vec on deltaz_mc_vec    
    hols_boot(i) = (deltaz_mc_vec' * rbar_mc_vec) / (deltaz_mc_vec' * deltaz_mc_vec);
    if ~isempty(qlist)
        tmph = zeros(1,length(qlist));
        for qloop = 1:length(qlist)
            tmph(qloop) = rq_fnm(deltaz_mc_vec,rbar_mc_vec,qlist(qloop));
        end
        hqr_boot(i,:) = tmph;
    end
end

% Confidence Intervals
olsci90 = bols - 1/n^(1/4) * prctile(hols_boot,[95,5]');
olsci95 = bols - 1/n^(1/4) * prctile(hols_boot,[97.5,2.5]');
olsci99 = bols - 1/n^(1/4) * prctile(hols_boot,[99.5,0.5]');

if ~isempty(qlist)
    qrci90 = zeros(2,length(qlist));
    qrci95 = qrci90;
    qrci99 = qrci90;
    bqr_corrected = zeros(1,length(qlist));
    for qloop = 1:length(qlist)
        bqr_corrected(qloop) =  bqr(qloop) - 1/n^(1/4) * prctile(hqr_boot(:,qloop),50);
        qrci90(:,qloop) = bqr(qloop) - 1/n^(1/4) * prctile(hqr_boot(:,qloop),[95,5]');
        qrci95(:,qloop) = bqr(qloop) - 1/n^(1/4) * prctile(hqr_boot(:,qloop),[97.5,2.5]');
        qrci99(:,qloop) = bqr(qloop) - 1/n^(1/4) * prctile(hqr_boot(:,qloop),[99.5,0.5]');
    end
end


%% Output
% ols results
output.olsci90 = olsci90;
output.olsci95 = olsci95;
output.olsci99 = olsci99;
% qr results
if ~isempty(qlist)
    output.bqr_corrected = bqr_corrected;
    output.qrci90 = qrci90;
    output.qrci95 = qrci95;
    output.qrci99 = qrci99;
end
% simulated h distributions
output.hols_boot = hols_boot;
output.hqr_boot = hqr_boot;
% Spot variances
output.spotvar = spotvar;

