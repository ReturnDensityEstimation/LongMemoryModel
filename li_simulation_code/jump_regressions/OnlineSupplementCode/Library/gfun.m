function output = gfun(x)
output = SCK(2*x-1);


function output = SCK(x)
output = CK(abs(x));

function output = CK(x)
output = (1 - 3 * x.^2 + 2 * x.^3) .* (x<=1);
