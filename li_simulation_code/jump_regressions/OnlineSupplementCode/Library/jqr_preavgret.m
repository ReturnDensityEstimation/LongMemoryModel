function [xbar,xhat] = jqr_preavgret(dx,kn,gfun)
glist = gfun((0:kn)'/kn);
gplist = diff(glist);
glist(1) = [];
[n,d] = size(dx);

xbar = zeros(n-kn+1,d);
if nargout == 2
    xhat = zeros(n-kn+1,d);
end

for i = 1:d
    dxi = dx(:,i);
    xbar(:,i) = conv(dxi,glist,'valid');
    if nargout == 2
        xhat(:,i) = conv(dxi.^2,gplist.^2,'valid');
    end
end