function [output,fullgroup] = jqr_group(JI,kn)
% JI is the indices of the selected jump observations
% kn is the window
% output records 
% [startindex, endindex, number of observations in group]
% for each group
%
% fullgroup extends the cluster to include about kn observations

gap = diff(JI);
index = find(gap > kn/4);
startindex = [JI(1);JI(index+1)];
endindex = [JI(index);JI(end)];
n = length(startindex);
output = zeros(n,3);
output(:,[1,2]) = [startindex, endindex];
for i = 1:n
    output(i,3) = sum(JI>=startindex(i) & JI<=endindex(i));
end

% fullgroup
mid = (output(:,1) + output(:,2))/2;
left = floor(mid-kn/2);
right = ceil(mid+kn/2);
fullgroup = [left,right];
