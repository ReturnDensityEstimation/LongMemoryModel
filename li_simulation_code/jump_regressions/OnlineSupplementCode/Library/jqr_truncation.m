function [output,tod_fit,stddev] = jqr_truncation(zbar,kn)
% We use the bipower preaveraging estimate to tune the truncation
% threshold. This is useful because we do truncation for the preaveraged
% returns.
%
% zbar: the matrix of overlapping preaveraged returns, with each column
% being a day
%
% kn: the preaveraging window

%%
% Compute tod
% Low power variation
q = 1.1;
% We use nonoverlapping preaveraged returns
dx = zbar(1:kn:end,:);
% The scatter plot for tod
tod_scatter = mean( abs(dx) .^ q , 2) .^ (1/q);
% Times used for estimating TOD
n = size(dx,1);
t_est = (1:n)'/n - 1/2/n;
tmat_est = [ones(size(t_est)), t_est, t_est.^2, t_est.^3];
% Fitting coef
b_tod = (tmat_est'*tmat_est)\(tmat_est'*tod_scatter);
% The times to be fitted (determined by the time points of the overlapping)
np = size(zbar,1);
t_fit = linspace(min(t_est),max(t_est),np)';
% Fitted value
tmat_fit = [ones(size(t_fit)),t_fit,t_fit.^2,t_fit.^3];
tod_fit = tmat_fit * b_tod;
% Normalization
tod_fit = tod_fit / sqrt(mean(tod_fit.^2));


%% Set up truncation
% Compute the standard deviation of preavg returns using a BV approach
tmp = abs(dx(1:end-1,:) .* dx(2:end,:));
stddev = sqrt((pi/2) * mean(tmp));
% Combine intraday TOD (column vec) with daily standard deviations (row vec)
output = tod_fit * stddev;

