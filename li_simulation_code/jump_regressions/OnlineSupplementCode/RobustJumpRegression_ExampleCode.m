addpath('./Library')
clear; close all; home;

%% User specified tuning parameters
kn = 36; % preaveraging window
knp = 720; % local window for pre-averaged spot vol estimation
TruncJump = 7; % i.e., 7 standard deviation for detecting big jumps
TruncCont = 4; % i.e., 4 standard deviation for jump-robust vol estimation
nboot = 1000; % Number of "bootstrap" simulations for inference
qlist = [0.1,0.25,0.5,0.75,0.9]; % Estimate quantile regressions at these quantiles.

%% DGP paramters
n = 4680;       % # obs per day
m = 250;        % # days
nall = n * m;   % total # obs
noisesize = 3; % A parameter governing noise size
jsize = 60; % A parameters governing jump size
MC = 1; % Number of Monte Carlo simulations.
% Law
lambda = 20;
rho = -0.7;
betac = 0.89;
betaj = 1;
mu_jmkt = 0.1;
%% Monte Carlo
tic
bols = zeros(1,MC);
bqr = zeros(length(qlist),MC);
bqrc = zeros(length(qlist),MC);
njump_mc = zeros(1,MC);
ciols = zeros(6,MC);
ciqr = zeros(6,length(qlist),MC);
disp('Hello!')
disp('The program will connect to available local workers to use parallel computing.')
disp('This will speed up the simulation-based inference.')
disp('This may take about 10-20 seconds.')
disp('...')
for mc = 1:MC   
    % NOTE: The unit of time is the length of the full sample
    % Jump indicators with lambda jumps in the sample
    dj_unit = binornd(1,lambda/nall,nall+1,1);
    % Brownians used in the volatility
    dB1 = randn(nall+1,1) / sqrt(nall);
    dB2 = randn(nall+1,1) / sqrt(nall);
    % Brownians used in the market returns
    dw_mkt = randn(nall,1) / sqrt(nall);
    dw_mkt = rho * dB1(2:end) + sqrt(1-rho^2) * dw_mkt;
    % Brownians used in the idio stock returns
    dw_stock = randn(nall,1) / sqrt(nall);
    % Market variance (exponetial jump-diffusion model)
    v_mkt = exp(log(18^2) - lambda * (0:nall)'/nall * mu_jmkt + cumsum(0.5 * dB1 + dj_unit .* exprnd(mu_jmkt,nall+1,1)));
    % Idio stock variance
    v_stock = exp(log(26^2 - betac^2 * 18^2) + cumsum(dB2));
    
    % Continuous returns
    dzc = sqrt(v_mkt(1:nall)) .* dw_mkt;
    dyc = betac * dzc + sqrt(v_stock(1:nall)) .* dw_stock;
    
    % Noise
    znoise = noisesize * sqrt(v_mkt) .* randn(nall+1,1) / sqrt(nall);
    ynoise = noisesize * sqrt(v_mkt * betac^2 + v_stock)...
                .* randn(nall+1,1) / sqrt(nall);
    dznoise = diff(znoise);
    dynoise = diff(ynoise);
    
    % Jumps
    tmp_jsize = jsize * sqrt(v_mkt(1:nall)) .* randn(nall,1) / sqrt(nall);
    dzj = dj_unit(1:nall) .* tmp_jsize;
    dyj = betaj * dzj;
    
    % Raw returns
    dz = dzc + dznoise + dzj;
    dy = dyc + dynoise + dyj;
    
    % Raw returns in matrix form
    dzmat = reshape(dz,n,m);
    dymat = reshape(dy,n,m);
    

    
    % Output
    output = jqr_preavg1d_olsqr(dymat,dzmat,[],kn,knp,@gfun,TruncJump,TruncCont,qlist,nboot,[]);
    
    
    % Recording OLS
    bols(mc) = output.bols; % OLS estimates
    ciols(:,mc) = [output.olsci90; output.olsci95; output.olsci99]; % CIs for OLS 
    % Recording QR
    bqr(:,mc) = output.bqr'; % Quantile Jump Regression Estimators
    bqrc(:,mc) = output.bqr_corrected'; % centered (i.e. 50% Confidence Bound)
    ciqr(:,:,mc) = [output.qrci90; output.qrci95; output.qrci99]; % CIs for QR
end
toc

%% Display results

disp('True Value of Beta')
disp(betaj)

disp('OLS Jump Regression Estimate')
disp(bols)

disp('OLS Jump Regression, 95% CI')
disp(output.olsci95')

disp('QR Jump Regression Estimates')
disp(bqr')

disp('QR Jump Regression, 95% CIs')
disp(output.qrci95)

