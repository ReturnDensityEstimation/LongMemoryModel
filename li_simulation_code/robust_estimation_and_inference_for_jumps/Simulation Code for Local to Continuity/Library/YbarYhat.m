function [Ybar, Yhat] = YbarYhat(dY,kn,k)
% [Ybar, Yhat] = YbarYhat(dY,kn,k)
% Compute Ybar, Yhat
%
% dY: the first difference of data
% kn: the window
% k:  the scale parameter

n = length(dY);
m = n - kn + 1;
Ybar = zeros(m,1);
Yhat = zeros(m,1);

% The weighting vector
g = gfun(k*(0:kn)/kn);
gprime2 = (g(2:end) - g(1:end-1)) .^ 2;
g(1) = [];

% Compute Ybar, Yhat
for i=1:m
    tmpbar = 0;
    tmphat = 0;
    for j=1:kn
        tmpbar = tmpbar + g(j) * dY(i+j-1);
        tmphat = tmphat + gprime2(j) * dY(i+j-1) ^ 2;
    end
    Ybar(i) = tmpbar;
    Yhat(i) = tmphat;
end




