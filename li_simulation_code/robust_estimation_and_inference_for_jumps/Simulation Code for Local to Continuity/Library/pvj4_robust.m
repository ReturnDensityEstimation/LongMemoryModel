function [H,H_se,AJL,stuff] = pvj4_robust(dz,deltan,T,kn,knprime,trunc_ratio)

% Preliminary computations
p = 4;
n = length(dz);
% Augment the return series to reduce the edge effect
dz = [dz; zeros(2*kn+knprime,1)];


[gbar2,gprimebar2] = gbar_gprimebar(2);
gbar4 = gbar_gprimebar(4);
gbar6 = gbar_gprimebar(6);

theta = kn * sqrt(deltan);
w1 = -1/theta/gbar4;
w2 = -4 * w1;
wvec = [w1; w2];

m1 = -1/3/gbar6;
m2 = 8/3/gbar6;
mvec = [m1;m2];

rho4 = rho_vec(4);
rho6 = rho_vec(6);

Aprimegg = Aprime(1,1,4);
Aprimegh = Aprime(1,2,4);
Aprimehh = Aprime(2,1,4);

[Psi0_gg,Psi1_gg] = Psi_p4(1,1);
[Psi0_gh,Psi1_gh] = Psi_p4(1,2);
[Psi0_hh,Psi1_hh] = Psi_p4(2,1);

capPsi_minus = [Psi0_gg, Psi0_gh; Psi0_gh, Psi0_hh];
capPsi_plus = capPsi_minus;
capPsiprime_minus = [Psi1_gg, Psi1_gh; Psi1_gh, Psi1_hh];
capPsiprime_plus = capPsiprime_minus;

stuff.capPsi_minus = capPsi_minus;
stuff.capPsi_plus = capPsi_plus;
stuff.capPsiprime_minus = capPsiprime_minus;
stuff.capPsiprime_plus = capPsiprime_plus;

% Small Sample Correction Factors
% sm1 = n / (n - kn + 1);
sm1 = 1;
% sm2 = n / (n - 2*kn - knprime);
sm2 = 1;

% Compute the estimator
[Ybarg, Yhatg] = YbarYhat(dz,kn,1);
[Ybarh, Yhath] = YbarYhat(dz,kn,2);

vvecg_p = Vvec(Ybarg,Yhatg,4) * sm1; % small sample correction made
vvech_p = Vvec(Ybarh,Yhath,4) * sm1; % small sample correction made

vbarg_p = rho4 * vvecg_p;
vbarh_p = rho4 * vvech_p;

H = sqrt(deltan) * (w1 * vbarg_p + w2 * vbarh_p);

stuff.pvj4_g = sqrt(deltan) * vbarg_p / theta / gbar4;
stuff.pvj4_h = sqrt(deltan) * vbarh_p / theta / (gbar4/2);/


stuff.vvecg_p = vvecg_p;
stuff.vvech_p = vvech_p;
stuff.vbarg_p = vbarg_p;
stuff.vbarh_p = vbarh_p;

% Compute the standard error
    % Truncation
        vvecg_2 = Vvec(Ybarg,Yhatg,2) * sm1;
        vbarg_2 = vvecg_2(1) - 0.5 * vvecg_2(2);
        trunc_level = trunc_ratio * sqrt(vbarg_2/T) * deltan^0.49;        
    % The continuous part
        vvec_trunc_2p = Vvec_trunc(Ybarg,Yhatg,2*4,trunc_level) * sm1;
        
        mstar_ggg = Mstar(deltan,kn,p,Aprimegg,gbar2,gprimebar2,vvec_trunc_2p);
        mstar_ghg = Mstar(deltan,kn,p,Aprimegh,gbar2,gprimebar2,vvec_trunc_2p);
        mstar_hhg = Mstar(deltan,kn,p,Aprimehh,gbar2,gprimebar2,vvec_trunc_2p);
        
        stuff.mstar_gg = mstar_ggg;
        stuff.mstar_gh = mstar_ghg;
        stuff.mstar_hh = mstar_hhg;
        
        sigmaC = deltan^(p/2-1) * [mstar_ggg mstar_ghg; mstar_ghg mstar_hhg];
        
%         stuff.sigmaC = sigmaC;
        
    % The jump part
        [eta0,eta1] = eta01(Ybarg,Yhatg,trunc_level,kn,knprime,deltan,gbar2,gprimebar2);
        [N0_minus_g, N1_minus_g, N0_plus_g, N1_plus_g] = capNfun(Ybarg,Yhatg,eta0,eta1,p,deltan,kn,knprime,rho6);
        [N0_minus_h, N1_minus_h, N0_plus_h, N1_plus_h] = capNfun(Ybarh,Yhath,eta0,eta1,p,deltan,kn,knprime,rho6);
        
        stuff.N0_minus = [N0_minus_g N0_minus_h] * sm2;
        stuff.N1_minus = [N1_minus_g N1_minus_h] * sm2;
        stuff.N0_plus = [N0_plus_g N0_plus_h] * sm2;
        stuff.N1_plus = [N1_plus_g N1_plus_h] * sm2;
        
        Nbar0_minus = [N0_minus_g N0_minus_h] * mvec * sm2;
        Nbar1_minus = [N1_minus_g N1_minus_h] * mvec * sm2;
        Nbar0_plus = [N0_plus_g N0_plus_h] * mvec * sm2;
        Nbar1_plus = [N1_plus_g N1_plus_h] * mvec * sm2;
        
        stuff.Nbar0_minus = Nbar0_minus;
        stuff.Nbar1_minus = Nbar1_minus;
        stuff.Nbar0_plus = Nbar0_minus;
        stuff.Nbar1_plus = Nbar1_minus;
        
        sigmaJ = theta^2 * p^2 * (capPsi_minus * Nbar0_minus + capPsi_plus * Nbar0_plus + capPsiprime_minus * Nbar1_minus + capPsiprime_plus * Nbar1_plus);
        
        stuff.sigmaJ = sigmaJ;
    % Combine the continuous and the jump part
        sigmaH = wvec' * sigmaC * wvec + max(wvec' * sigmaJ * wvec,0);
        H_se = deltan^(1/4) * sqrt(sigmaH);
        
% AJL test stat
        AJL = H / deltan^(1/4) / sqrt(wvec' * sigmaC * wvec) * vbarg_p / (4 * vbarh_p);
        stuff.AJL_here = H / deltan^(1/4) / sqrt(wvec' * sigmaC * wvec);
        
        
        