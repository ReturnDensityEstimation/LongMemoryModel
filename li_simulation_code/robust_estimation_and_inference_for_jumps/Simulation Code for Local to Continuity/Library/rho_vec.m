function output = rho_vec(p)
tmp = rho_matrix(p);
output = tmp(1,:);