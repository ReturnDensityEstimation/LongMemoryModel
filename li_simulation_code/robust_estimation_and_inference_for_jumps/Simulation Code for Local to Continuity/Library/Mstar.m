function output = Mstar(deltan,kn,p,Aprimegh,phibar2,phiprimebar2,Vvec_trunc_2p_phi)
% Aprimegh can be computed by
%
% capAprime(ghandle,hhandle,gprimehandle,hprimehandle,p)
%
% Vvec_trunc_2p_phi can be computed by
%
% Vvec_trunc(Ybar,Yhat,2*p,un)

output = 0;
theta = sqrt(deltan) * kn;

for w = 0:p
    windex = w+1;
    rho_2w = rho_vec(2*w);
    sum_rho_vstar = 0;
    for l=0:w
        lindex = l+1;
        sum_rho_vstar = sum_rho_vstar + rho_2w(lindex) * Vvec_trunc_2p_phi(p+lindex-w);  
    end
    output = output + theta * Aprimegh(windex) ...
        / (mfun(2*w) * 2^(p-w) * phibar2^w * phiprimebar2^(p-w))...
        * sum_rho_vstar;
end

output = output * deltan ^ (1-p/2);
