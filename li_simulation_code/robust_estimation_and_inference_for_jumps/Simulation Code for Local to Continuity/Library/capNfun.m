function [N0_minus, N1_minus, N0_plus, N1_plus] = capNfun(Zbar,Zhat,eta0,eta1,p,deltan,kn,knprime,rho_2pm2)
n_Zbar = length(Zbar);
n_eta = length(eta0);
% The local version of Vbar(2p-2)
localvbar2pm2 = zeros(n_Zbar,1);
for l=0:(p-1)
    localvbar2pm2 = localvbar2pm2 + rho_2pm2(l+1) * Zbar.^(2*p-2-2*l) .* Zhat.^l;
end

% N minus
N0_minus = sqrt(deltan) * dot(localvbar2pm2(kn+knprime:n_Zbar), eta0(1:(n_Zbar-kn-knprime+1)));
N1_minus = sqrt(deltan) * dot(localvbar2pm2(kn+knprime:n_Zbar), eta1(1:(n_Zbar-kn-knprime+1)));

% N plus
N0_plus = sqrt(deltan) * dot(localvbar2pm2(1:n_eta-kn), eta0(kn+1:n_eta));
N1_plus = sqrt(deltan) * dot(localvbar2pm2(1:n_eta-kn), eta1(kn+1:n_eta));

