function output = Vvec(Ybar,Yhat,p)
% Compute Vvec
% The jth element, j=0,...,p/2, is V(Y,g,p-2j,j) = sum |Ybar|^(p-2j) Yhat^j
halfp = p/2;
output = zeros(halfp+1,1);
Ybar2 = Ybar .^ 2;

output(1) = sum(Ybar2 .^ halfp);
output(halfp+1) = sum(Yhat .^ halfp);

for j=1:halfp-1
    output(j+1) = sum(Ybar2 .^ (halfp - j) .* Yhat .^ j);
end
