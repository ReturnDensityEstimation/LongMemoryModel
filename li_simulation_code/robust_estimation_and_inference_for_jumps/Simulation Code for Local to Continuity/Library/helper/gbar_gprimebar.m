function [gbarp,gprimebarp] = gbar_gprimebar(p)
gbarp = 4^p * gamma(1+p)^2 / gamma(2+2*p);
gprimebarp = 2^(2*p-1) * (1+(-1)^p) / (p+1);