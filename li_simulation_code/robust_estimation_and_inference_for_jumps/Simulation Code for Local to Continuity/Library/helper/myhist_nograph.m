function [f,X] = myhist_nograph(Y,M,densityhandle,varargin)
% [f,X] = myhist(Y,M,densityhandle,varargin);
[N,X] = hist(Y,M);
width = mean(X(2:end)-X(1:end-1));
n = length(Y);
f = N/n/width;
%bar(X,f);
if nargin == 3
    hold on;
    plot(X,densityhandle(X),'r','LineWidth',2)
    hold off;
elseif nargin > 3
    hold on;
    plot(X,densityhandle(X,varargin{:}),'r','LineWidth',2)
    hold off;
end