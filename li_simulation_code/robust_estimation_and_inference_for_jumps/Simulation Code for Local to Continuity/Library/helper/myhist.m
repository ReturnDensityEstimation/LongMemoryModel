function [f,X] = myhist(Y,M,densityhandle,varargin)
% [f,X] = myhist(Y,M,densityhandle,varargin);
[N,X] = hist(Y,M);
width = mean(X(2:end)-X(1:end-1));
n = length(Y);
f = N/n/width;
area(X,f,'FaceColor',[.5 .9 .6],'EdgeColor',[1 1 1]);
if nargin == 3
    hold on;
    plot(X,densityhandle(X),'k','LineWidth',1)
    hold off;
elseif nargin > 3
    hold on;
    plot(X,densityhandle(X,varargin{:}),'k','LineWidth',1)
    hold off;
end