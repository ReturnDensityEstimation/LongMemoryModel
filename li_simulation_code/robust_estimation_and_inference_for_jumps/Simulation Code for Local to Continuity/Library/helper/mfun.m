function output = mfun(r)
% m(r) = E|N(0,1)|^r
% See Jacod and Ait-Sahalia 2006 equation (10)
output = pi^(-1/2) * 2^(r/2) * gamma((r+1)/2);        
