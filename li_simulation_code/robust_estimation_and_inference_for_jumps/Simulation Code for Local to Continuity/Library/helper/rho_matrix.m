% This function has been updated according to the 10-14-2009 draft.
% Numerically, it is same as before, only the way of writing the formula
% changed.

function output = rho_matrix(p)
output = inv(rho_tilde(p));

function output = rho_tilde(p)
% Compute the rho_tilde matrix in AJL2009

if mod(p,2)~=0
    error('p should be an even integer')
end

output = zeros(p/2+1);
for l = 0:(p/2)
    for j = l:(p/2)
        lindex = l+1;
        jindex = j+1;
        output(lindex,jindex) = 2^l * nchoosek(p-2*l,2*j-2*l) * mfun(p-2*j) * mfun(2*j - 2*l) / mfun(p);
    end
end
        

