function output = gfun(x)
output = (1-(2*x-1).^2) .* (x>=0 & x<=1);
