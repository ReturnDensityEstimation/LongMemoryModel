function [eta0,eta1] = eta01(Zbar,Zhat,un,kn,knprime,deltan,phibar2,phiprimebar2)
trunc = (abs(Zbar)<=un);
Zbar2_trunc = Zbar.^2 .* trunc;
Zhat_trunc = Zhat .* trunc;

% generate eta0 and eta1
n_Zbar = length(Zbar);
n_eta = n_Zbar - knprime + 1;
eta0 = zeros(n_eta,1);
eta1 = zeros(n_eta,1);

for i=1:n_eta
    tmpeta0 = 0;
    tmpeta1 = 0;
    for j=1:knprime
        tmpeta0 = tmpeta0 + Zbar2_trunc(i+j-1) - 0.5 * Zhat_trunc(i+j-1);
        tmpeta1 = tmpeta1 + Zhat_trunc(i+j-1);
    end
    eta0(i) = tmpeta0 / (kn * knprime * deltan);
    eta1(i) = tmpeta1 / (kn * knprime * deltan);
end
eta0 = eta0 / phibar2;
eta1 = eta1 / 2 / phiprimebar2;
