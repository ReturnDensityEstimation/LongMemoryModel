function [llnc,vc] = theory_continuous_p4(sigma,alpha,T,theta,deltan,b,k)
p = 4;
llnc = deltan^(p/4-1) * mfun(4) * (theta * gbar_gprimebar(2) / b ./ [1, k]).^(p/2) * T * sigma^p;

Aprime11_vec = Aprime(b,1,p);
vc11 = 0;
for w=0:p
    vc11 = vc11 ...
        + Aprime11_vec(w+1) * theta^(1-p) * T * (theta * sigma)^(2*w) * alpha^(2*p-2*w);
end

Aprime12_vec = Aprime(b,k,p);
vc12 = 0;
for w=0:p
    vc12 = vc12 ...
        + Aprime12_vec(w+1) * theta^(1-p) * T * (theta * sigma)^(2*w) * alpha^(2*p-2*w);
end

Aprime22_vec = Aprime(b*k,1,p);
vc22 = 0;
for w=0:p
    vc22 = vc22 ...
        + Aprime22_vec(w+1) * theta^(1-p) * T * (theta * sigma)^(2*w) * alpha^(2*p-2*w);
end

vc = [vc11 vc12;vc12 vc22];
vc = vc * deltan^(p/2-3/2);
