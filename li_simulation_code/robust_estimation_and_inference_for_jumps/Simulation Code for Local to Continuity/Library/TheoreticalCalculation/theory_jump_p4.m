function [llnj,vj] = theory_jump_p4(pvj4,pvj6,sigma,alpha,theta,deltan,b,k)

p = 4;
llnj = theta / sqrt(deltan) * gbar_gprimebar(p) / b ./ repmat([1 k],length(pvj4),1) .* repmat(pvj4,1,2);

[Psi0_11,Psi1_11] = Psi_p4(b,1);
vj11 = 1/sqrt(deltan) * theta^2 * p^2 * ...
    (2 * theta * sigma^2 * Psi0_11 + 2 * alpha^2/theta * Psi1_11) * pvj6;
[Psi0_12,Psi1_12] = Psi_p4(b,k);
vj12 = 1/sqrt(deltan) * theta^2 * p^2 * ...
    (2 * theta * sigma^2 * Psi0_12 + 2 * alpha^2/theta * Psi1_12) * pvj6;
[Psi0_22,Psi1_22] = Psi_p4(b*k,1);
vj22 = 1/sqrt(deltan) * theta^2 * p^2 * ...
    (2 * theta * sigma^2 * Psi0_22 + 2 * alpha^2/theta * Psi1_22) * pvj6;

vj = [vj11 vj12 vj22];
