function output = Vvec_trunc(Ybar,Yhat,p,un)
% Compute Vvec
% The jth element, j=0,...,p/2, is 
% V(Y,g,p-2j,j) = sum |Ybar|^(p-2j) Yhat^j 1{|Ybar|<=un}
% 
% Ybar2_trunc = |Ybar|^2 1{|Ybar|<=un}
trunc = (abs(Ybar)<=un);
Ybar2_trunc = Ybar.^2 .* trunc;
Yhat_trunc = Yhat.*trunc;

halfp = p/2;
output = zeros(halfp+1,1);

output(1) = sum(Ybar2_trunc .^ halfp);
output(halfp+1) = sum(Yhat_trunc .^ halfp);

for j=1:halfp-1
    output(j+1) = sum(Ybar2_trunc .^ (halfp - j) .* Yhat_trunc .^ j);
end
