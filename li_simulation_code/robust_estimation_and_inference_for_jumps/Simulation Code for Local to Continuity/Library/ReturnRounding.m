function dlogz_rounded = ReturnRounding(dlogz,z0,rounding_level)
% dlogz: n x M, log returns of the original price (before rounding)
% z0: the initial price level. Set z0 <= 0 if do not want rounding
% rounding_level
if rounding_level == 0 || z0 <= 0
    dlogz_rounded = dlogz;
else
    M = size(dlogz,2);
    z = exp(log(z0)+cumsum([zeros(1,M); dlogz]));
    log_z_rounded = log(round(z/rounding_level) * rounding_level);
    dlogz_rounded = log_z_rounded(2:end,:) - log_z_rounded(1:end-1,:);
end