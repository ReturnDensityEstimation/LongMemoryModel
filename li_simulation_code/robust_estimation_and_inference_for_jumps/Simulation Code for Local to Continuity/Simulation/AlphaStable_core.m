%% Simulation Setup

% % Startup
addpath(genpath('../Library/')); % Unix path convention
%if matlabpool('size') == 0
%    matlabpool local 8
%end
% 
% % Simulation Parameters common to all settings
M = 20; % Monte Carlo # in paper

T = 5/250; % Horizon
deltan = 1/4680/250; % Sampling Frequency
n = 23400;
p = 4;
beta = 0.4^2; % average vol^2

activitylist = [0.5 1 1.5 1.75];
rounding_level = 0.01; % rounding level 1 cent

% Total number of cases
ncases = length(NSRlist) * length(activitylist) * length(jvratiolist) * ...
    length(knlist) * length(knprimeratiolist) * length(truncratiolist) * length(z0list);
% Time used for each case
timevec = zeros(ncases,1);
% 
%% Simulation
% 
SamplePathDir = './';
ResultDir = './';
mkdir('./CSV/');
mkdir('./MAT/');
mkdir('./PDF/');
mkdir('./EPS/');
% Loading dx data
load([SamplePathDir 'dx_' dxmodel '.mat'],'dx','sigma','IV');

% Loading Gaussian Noise
load([SamplePathDir 'noise_GaussianIID.mat'],'dnoise');
dnoise1 = dnoise; 
clear dnoise;
% Loading T noise
load([SamplePathDir 'noise_TruncT.mat'],'dnoise');
% Mixture Noise
dnoise = (dnoise + dnoise1) * sqrt(deltan) .* sigma(1:n,:);

cnt = 1;
% Monte Carlo starts here
for NSRloop = 1:length(NSRlist)
    NSR = NSRlist(NSRloop);
    % Generate Noisy Continuous log return
    dzc_mat = dx + NSR * dnoise; 
    for activityloop = 1:length(activitylist)
        jactivity = activitylist(activityloop);
        % Load the Poisson Jumps
        % Recall that the jumps are scaled so that the realized JQV is 1
        load([SamplePathDir 'dj_AlphaStable' num2str(jactivity) '.mat']);
        for jvratioloop = 1:length(jvratiolist)
            % Rescaling jumps so that the realized JQV / expected QV =
            % jvratio
            jvratio = jvratiolist(jvratioloop);
            jsize = sqrt(jvratio/(1-jvratio) * beta * T);
            % True values of certain functionals
            true_pvj4 = jsize^4 * sum(dj.^4)';
            true_sigma2pvj6 = sum(sigma(1:n,:).^2 .* jsize^6 .* dj.^6)';
            true_alpha2pvj6 = sum(NSR^2 * 2 * deltan * sigma(1:n,:).^2 .* jsize^6 .* dj.^6)';
            % The noisy log return with jumps, before rounding
            dz_mat_norounding = dzc_mat + dj * jsize;
            
            for z0loop = 1:length(z0list)
                % Starting the loop on the intial price level. This is
                % relevant for rounding
                z0 = z0list(z0loop);
                
                % The noisy log return after rounding (rounding is on the
                % price level)
                dz_mat = ReturnRounding(dz_mat_norounding,z0,rounding_level);
                
                for knloop = 1:length(knlist)
                    kn = knlist(knloop);
                    theta = kn * sqrt(deltan);

                    %sigma_jump_true = jumpcase_truevar(dJ,p,gbarp,sigma^2/deltan,alpha^2,deltan,kn,psi_minus,psi_plus,psiprime_minus,psiprime_plus);
                    for knprimeratioloop=1:length(knprimeratiolist)
                        knprimeratio = knprimeratiolist(knprimeratioloop);
                        knprime = kn * knprimeratio;

                        tic
                        for truncloop = 1:length(truncratiolist)
                            trunc_ratio = truncratiolist(truncloop);
                            disp(dxmodel);
                            fprintf('NSRloop = %4.0f, activityloop = %4.0f, jvratioloop = %4.0f, knloop = %4.0f, knprimeloop = %4.0f, truncloop = %4.0f\n',...
                                NSRloop,activityloop,jvratioloop,knloop,knprimeratioloop,truncloop);
                            % simulation
                            pvj4_g = zeros(M,1);
                            pvj4_h = zeros(M,1);
                            H = zeros(M,1);
                            H_se = zeros(M,1);
                            AJL = zeros(M,1);
                            parfor m = 1:M
                                dz = dz_mat(:,m);
                                [H(m), H_se(m), AJL(m), stuff] = pvj4_robust(dz,deltan,T,kn,knprime,trunc_ratio);
                                pvj4_g(m) = stuff.pvj4_g;
                                pvj4_h(m) = stuff.pvj4_h;
                            end
                            save([ResultDir 'MAT/' dxmodel '_AlphaStable_NSR' num2str(NSR)...
                                '_JumpActivity' num2str(jactivity)...
                                '_jvratio' num2str(jvratio)...
                                '_z0' num2str(z0)...
                                '_kn' num2str(kn)...
                                '_knprimeratio' num2str(knprimeratio)...
                                '_trunc' num2str(trunc_ratio) '.mat'],...
                                'H','H_se','AJL','pvj4_g','pvj4_h','true_pvj4','true_sigma2pvj6','true_alpha2pvj6');
                            timeused = floor(toc);
                            timevec(cnt) = timeused;
                            fprintf('Time used %9.0f seconds\n',timeused);
                            fprintf('Will finish in %9.2f hours, at ', sum(timevec) / cnt * (ncases-cnt) / 3600);
                            disp(datestr(addtodate(now,floor(sum(timevec) / cnt * (ncases-cnt)),'second')));
                            cnt = cnt + 1;
                        end
                    end
                end
            end
        end
    end
end

%% Analyze Results

[Psi0_gg,Psi1_gg] = Psi_p4(1,1);
[Psi0_gh,Psi1_gh] = Psi_p4(1,2);
[Psi0_hh,Psi1_hh] = Psi_p4(2,1);
gbar4 = gbar_gprimebar(4);
hbar4 = gbar4/2;


for z0loop = 1:length(z0list)
    z0 = z0list(z0loop);
    for knloop = 1:length(knlist)
        kn = knlist(knloop);
        theta = kn * sqrt(deltan);
        for knprimeratioloop=1:length(knprimeratiolist)
            knprimeratio = knprimeratiolist(knprimeratioloop);
            for truncloop = 1:length(truncratiolist)
                trunc_ratio = truncratiolist(truncloop);
                file1 = [ResultDir 'CSV/' dxmodel '_AlphaStable_MedianRelBias_z0' num2str(z0)...
                                '_kn' num2str(kn)...
                                '_knprimeratio' num2str(knprimeratio)...
                                '_trunc' num2str(trunc_ratio) '.csv'];
                            
                file2 = [ResultDir 'CSV/' dxmodel '_AlphaStable_Coverage_z0' num2str(z0)...
                                '_kn' num2str(kn)...
                                '_knprimeratio' num2str(knprimeratio)...
                                '_trunc' num2str(trunc_ratio) '.csv'];
                
                fid1 = fopen(file1,'wt');
                fprintf(fid1,'Median Relative Bias\n');
                fprintf(fid1,'z0 = %9.4f, kn = %9.0f, knprimeratio = %9.0f, trunc ratio = %9.0f\n\n',z0,kn,knprimeratio,trunc_ratio);
                
                fprintf(fid1,'NSR,Daily Lambda');
                for jvratioloop = 1:length(jvratiolist)
                    jvratio = jvratiolist(jvratioloop);
                    fprintf(fid1,',JV = %5.2f,,',jvratio);
                end
                fprintf(fid1,'\n');
                
                
                fid2 = fopen(file2,'wt');
                fprintf(fid2,'Coverage\n');
                fprintf(fid2,'z0 = %9.4f,kn = %9.0f,knprimeratio = %9.0f,trunc ratio = %9.0f\n\n',z0,kn,knprimeratio,trunc_ratio);

                fprintf(fid2,'NSR,Daily Lambda');
                for jvratioloop = 1:length(jvratiolist)
                    jvratio = jvratiolist(jvratioloop);
                    fprintf(fid2,',JV = %5.2f,,',jvratio);
                end
                fprintf(fid2,'\n');

                for NSRloop = 1:length(NSRlist)
                    NSR = NSRlist(NSRloop);
                    subtable1 = [];
                    subtable2 = [];
                    
                    for jvratioloop = 1:length(jvratiolist)
                        jvratio = jvratiolist(jvratioloop);
                        
                        % Relative Bias
                        tmpblock1 = zeros(length(activitylist),3);
                        % Coverage
                        tmpblock2 = zeros(length(activitylist),3);
                        
                        for activityloop = 1:length(activitylist)
                            jactivity = activitylist(activityloop);
                            load([ResultDir 'MAT/' dxmodel '_AlphaStable_NSR' num2str(NSR)...
                                '_JumpActivity' num2str(jactivity)...
                                '_jvratio' num2str(jvratio)...
                                '_z0' num2str(z0)...
                                '_kn' num2str(kn)...
                                '_knprimeratio' num2str(knprimeratio)...
                                '_trunc' num2str(trunc_ratio) '.mat'],...
                                'H','H_se','AJL','pvj4_g','pvj4_h','true_pvj4','true_sigma2pvj6','true_alpha2pvj6');
                            true_pvj4 = true_pvj4(1:M);
                            true_sigma2pvj6 = true_sigma2pvj6(1:M);
                            true_alpha2pvj6 = true_alpha2pvj6(1:M);
                            filepdf = [ResultDir 'PDF/' dxmodel '_AlphaStable_NSR' num2str(NSR)...
                                '_JumpActivity' num2str(jactivity)...
                                '_jvratio' num2str(jvratio)...
                                '_z0' num2str(z0)...
                                '_kn' num2str(kn)...
                                '_knprimeratio' num2str(knprimeratio)...
                                '_trunc' num2str(trunc_ratio) '.pdf'];
                            
                            fileeps = [ResultDir 'EPS/' dxmodel '_AlphaStable_NSR' num2str(NSR)...
                                '_JumpActivity' num2str(jactivity)...
                                '_jvratio' num2str(jvratio)...
                                '_z0' num2str(z0)...
                                '_kn' num2str(kn)...
                                '_knprimeratio' num2str(knprimeratio)...
                                '_trunc' num2str(trunc_ratio) '.eps'];
                                        
                           
                            
                            % Plotting histogram of bias
                            bias_robust = H(true_pvj4>0) - true_pvj4(true_pvj4>0);
                            bias_g = pvj4_g(true_pvj4>0) - true_pvj4(true_pvj4>0);
                            bias_h = pvj4_h(true_pvj4>0) - true_pvj4(true_pvj4>0);
                            
                            tmp = [bias_robust; bias_g; bias_h];
                            xi = linspace(prctile(tmp,1),prctile(tmp,99),1000);

                            fr = ksdensity(bias_robust,xi);
                            fg = ksdensity(bias_g,xi);
                            fh = ksdensity(bias_h,xi);
                            figure;
                            area(xi,fr,'facecolor',0.618*[1,1,1],'edgecolor','none')
                            hold on;
                            plot(xi,fg,'k-.')
                            plot(xi,fh,'k--')
                            plot([0,0],get(gca,'ylim'),'k-');
                            plot(min(xi)*[1,1],get(gca,'ylim'),'k-');
                            hold off;
                            set(gca,'xlim',[min(xi),max(xi)])
                            legend('Bias-Corrected','Standard 1','Standard 2')
                            xlabel('Bias')
                            box off;
                            legend boxoff;                            
                            
                            saveas(gcf,filepdf,'pdf');
                            saveas(gcf,fileeps,'psc2');
                            close all;
                            
                            % median rel bias
                            
                            mrb = ([H pvj4_g pvj4_h] - repmat(true_pvj4,1,3)) ./ repmat(true_pvj4,1,3);                            
                            tmpblock1(activityloop,:) = median(mrb(true_pvj4>0,:));
                            
                            
                            % coverage
                            coverage_robust = mean(abs(H - true_pvj4)<=norminv(0.975) * H_se);
                            
                            % g
                            sigmaJ_theory_g ...
                                = theta^2 * p^2 * (2 * Psi0_gg * true_sigma2pvj6 * theta + ...
                                                   2 * Psi1_gg * true_alpha2pvj6 / theta);
                            coverage_g = (abs(pvj4_g - true_pvj4) * theta * gbar4 / deltan^(1/4) ./ sqrt(sigmaJ_theory_g) <= norminv(0.975));
                            coverage_rate_g = mean(coverage_g);
                            % h
                            sigmaJ_theory_h ...
                                = theta^2 * p^2 * (2 * Psi0_hh * true_sigma2pvj6 * theta + ...
                                                   2 * Psi1_hh * true_alpha2pvj6 / theta);
                            coverage_h = (abs(pvj4_h - true_pvj4) * theta * hbar4 / deltan^(1/4) ./ sqrt(sigmaJ_theory_h) <= norminv(0.975));
                            coverage_rate_h = mean(coverage_h);

                            tmpblock2(activityloop,:) = [coverage_robust coverage_rate_g coverage_rate_h];
                        end
                        
                        subtable1 = [subtable1 tmpblock1];
                        subtable2 = [subtable2 tmpblock2];
                    end
                    
                    MRB_TABLE{z0loop,knloop,knprimeratioloop,truncloop,NSRloop} = subtable1;
                    COVERAGE_TABLE{z0loop,knloop,knprimeratioloop,truncloop,NSRloop} = subtable2;
                    
                    for activityloop = 1:length(activitylist)
                        jactivity = activitylist(activityloop);
                        fprintf(fid1,'%9.0f,%9.4f',NSR,jactivity);
                        fmt = [repmat(',%9.1f',1,length(jvratiolist)*3) '\n'];
                        fprintf(fid1,fmt,subtable1(activityloop,:)*100);
                        fprintf(fid2,'%9.0f,%9.4f',NSR,jactivity);
                        fmt = [repmat(',%9.1f',1,length(jvratiolist)*3) '\n'];
                        fprintf(fid2,fmt,subtable2(activityloop,:)*100);
                    end
                    
                    
                end
                fclose all;
            end
        end
    end
end
