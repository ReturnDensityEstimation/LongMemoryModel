
%% Setup
clear;
close all;

% Horizon
n_days = 5;
T = n_days/250;

% Sampling frequency
n_daily = 4680;

deltan = 1/250/n_daily;

% Number of Observations
n = n_days * n_daily;

% Monte Carlo Number
M = 20;

%% Generate log returns with stochastic volatility

% Parameters
beta = 0.4^2;
gamma = 0.5;
kappa = 5;
rho = -0.5;

% Simulating Path
v = zeros(n+1,1);
dx = zeros(n,M);
sigma = zeros(n+1,M);

for m=1:M
    disp(m)
    v(1) = beta;   
    dW = randn(n,1) * sqrt(deltan);
    dB = rho * dW + sqrt(1-rho^2) * randn(n,1) * sqrt(deltan);
    for t=2:(n+1)
        v(t) = v(t-1) + kappa * (beta - v(t-1)) * deltan + gamma * sqrt(v(t-1)) * dB(t-1);
    end
    sigma(:,m) = sqrt(v);
    dx(:,m) = sigma(1:n,m) .* dW;
end
IV = sum(dx.^2);
save('dx_Heston','dx','sigma','IV');


%% Generate IID Gaussian Noise
% The std deviation is 1
noise = randn(n+1,M);
dnoise = noise(2:end,:) - noise(1:n,:);
save('noise_GaussianIID','dnoise');

%% Generate Truncated T noise
% Truncate at 25, df = 2.5

df = 2.5;
bound = 25;
t_variance = 3.84995;
noise = trnd(df,n+1,M);
noise(noise > bound) = bound;
noise(noise < -bound) = -bound;
noise = noise / sqrt(t_variance);
dnoise = noise(2:end,:) - noise(1:n,:);
save('noise_TruncT','dnoise');


%% Generate alpha stable jumps
activity_list = [0.5 1 1.5 1.75];
Vmat = unifrnd(-pi/2,pi/2,n,M);
Wmat = exprnd(1,n,M);
dj = zeros(n,M);
for i = 1:length(activity_list)    
    a = activity_list(i);    
    for m = 1:M
        disp(m)
        V = Vmat(:,m);
        W = Wmat(:,m);
        dj(:,m) = sin(a * V) ./ (cos(V).^(1/a)) .* (cos((1-a)*V) ./ W).^((1-a)/a) * deltan^(1/a);
    end
    JQV = sum(dj.^2);
    % Rescaling such that realized jump qv is 1
    dj = dj ./ sqrt(repmat(JQV,n,1));
    save(['dj_AlphaStable' num2str(a) '.mat'],'dj');
end

%% Plot sample paths for alpha stable jump processes
clear dj;
figure;
for i = 1:length(activity_list)
    a = activity_list(i);
    load(['dj_AlphaStable' num2str(a) '.mat'],'dj');
    figure
    plot(dj(:,1))
    title(['JQV = ' num2str(sum(dj.^2))]);
end
