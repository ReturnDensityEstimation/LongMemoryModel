\documentclass[fleqn,t,smaller,xcolor=dvipsnames]{beamer}
\usetheme{Warsaw}
\usecolortheme{dolphin}
\usepackage{datetime}
\usepackage{graphicx}
\usepackage{sidecap}
\usepackage{commath}
\usepackage{mathtools}
\usepackage{caption}
\usepackage{amsmath}
\usepackage{siunitx}
\usepackage{ragged2e}
\usepackage{changepage}
\usepackage[backend=biber, autopunct=true, authordate]{biblatex-chicago}  

\graphicspath {{figures/}}


\newcommand{\E}{\mathbb{E}}                                                                                        
\newcommand{\Var}{\mathbb{V}\mathrm{ar}}                                                                           
\newcommand{\Cov}{\mathbb{C}\mathrm{ov}}                                                                           
\newcommand{\N}{\mathcal{N}}                                                                                       
\newcommand{\lequals}{\stackrel{\Lap}{=}}                                                                   
\newcommand{\aequals}{\stackrel{a.s.}{=}}                                                                          
\newcommand{\I}{\mathbb{I}}                                                                                        
\newcommand{\F}{\mathcal{F}}                                                                                       
\newcommand{\Lap}{\mathcal{L}}
\newcommand{\qt}[1]{\lq\lq#1\rq\rq}  


\setbeamertemplate{navigation symbols}{}  
\setbeamertemplate{headline}{}   
\captionsetup{labelformat=empty}
\defbeamertemplate*{footline}{shadow theme}
{%
  \leavevmode%
  \hbox{\begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm plus1fil,rightskip=.3cm]{author in head/foot}%
    \usebeamerfont{author in head/foot}\hfill\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm,rightskip=.3cm plus1fil]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle\hfill\insertframenumber\,/\,\inserttotalframenumber
  \end{beamercolorbox}}%
  \vskip0pt%
}


\newcommand\itembullet{
  \begingroup
  \leavevmode
  \usebeamerfont*{itemize item}%
  \usebeamercolor[fg]{itemize item}%
  \usebeamertemplate**{itemize item}%
  \setlength\labelsep   {\dimexpr\labelsep + 0.5em\relax} 
  \endgroup
}
\newcommand{\spitem}{\vspace{.3cm}\item}

\AtBeginSection[]{
{
  \begin{frame}[c,plain, noframenumbering]
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \end{frame}
}
}


\title[]{A Tale of Tails: Using High-Frequency Data to Estimate the Daily Return Density}
\author[Paul Sangrey]{Paul Sangrey}
\institute{University of Pennsylvania \\ \textit{sangrey@sas.upenn.edu}}
\date{December 7, 2016}
\addbibresource{references.bib}

\begin{document}

\begin{frame}[plain, noframenumbering]
	\maketitle
\end{frame}


\begin{frame}[c]{Daily Return Density}
\begin{itemize}
	\item They key characterization of financial risk is the discrete-time 
		density of the log-return:
		\color{blue} 
			$p_t = p\left( Y_t \middle\vert \F_{t-1}\right)$. 
		\color{black}
	\item That is we have a sequence of daily densities $p_{t}, p_{t+1}, \ldots$
	\item Also known as the physical measure. 
\end{itemize}
\end{frame}

\begin{frame}[c]{The Issues}
\begin{itemize}
	\item We are estimating an infinite sequence of latent,
		infinite-dimensional objects with only one observation each.
	\item In addition, we need good estimates of the tails since they drive most 			utility risk.
	\item However the tails, by definition, are when we don't have much data.
\end{itemize}
\end{frame}


\begin{frame}[c]{This Paper}
\begin{itemize}
	\item This paper bridges the gap between discrete-time models for the 					daily density and the continuous-time models of the high-frequency literature.
	\item We develop a novel model-free characterization of time-varying financial
		 risk in terms of the integrated volatility $V_t$ and jump volatility 					$K_t$.
	\item In so doing, we develop a parsimonious nonparametric model for the daily 		return density. 
\end{itemize}
\end{frame}

\begin{frame}[c]{Model}
\begin{itemize}
	\item Exploits the structure implied by no-arbitrage.
	\item Can be efficiently estimated using high-frequency data.
	\item Is parsimonious and extensible, and its sufficient statistics 
	(the volatility measures) are persistent and economically interpretable.
\end{itemize}
\end{frame}

\begin{frame}[c]{Two Models for the Dynamics}
\begin{itemize}
	\item A nonparametric model using Bayesian nonparametric methods that 
			consistently estimates all of the quantities of interest.
	\item A standard parametric model that is simple to estimate.
\end{itemize}
\end{frame}

\begin{frame}[c]{A \qt{Simple} Model}
\begin{itemize}
	\item \color{blue} $Y_t = \sqrt{V_t} \N(0,1) + \sqrt{K_t} \Lap\left(0, 1\right) $ 
		\color{black}
	\item $\begin{pmatrix} \log V_t \\ \log K_t \end{pmatrix} = A_0 + 
		A_1 \begin{pmatrix} \log V_{t-1} \\ \log K_{t-1} \end{pmatrix}	+
		A_2  \begin{pmatrix} \log V_{t-2} \\ \log K_{t-2} \end{pmatrix}	+
		\Sigma^{-1/2} \N\left( 0, \mathbb{I}_2 \right) $
	\item $V_t = \int_{t-1}^t \sigma^2(s) ds $ (The integrated volatility)
	\item \color{blue} $K_t = \E\left[\sum_{t-1 < s \leq t} \Delta {J^Y(s)}^2 \middle\vert \F_{t-} \right] $ \color{black} (The jump volatility.) 
\end{itemize}
\end{frame}


\section{Literature Review}

\begin{frame}[c]{Smoothing the Tails}
\begin{itemize}
	\item Parametric models, use information in the center of the distribution to 				estimate the tails
	\item As heavily regularized models often do, they work quite well most of the 
		time, but ignore risk arising from distributional uncertainty.
	\item According to \textcite{righi2015comparison}, parametric models such 
		as $SV$ and $GARCH$ have the best practical performance of current models.
		 (As opposed to something like CAViaR which models the tails directly.)
	\item How close is the true innovation distribution to skewed-$t$ in really 			bad times?
	\item How much can we trust the backtesting results? Again, we are 
		backtesting in the tails, when we don't have much data.
\end{itemize}
\end{frame}

\begin{frame}[c]{Volatility Literature}
\begin{itemize}
	\item Has done a good job of modeling the volatility, in papers including, but by 			no means limiited to the following \textcite{andersen2001distribution},
		\textcite{corsi2009simple}, and \textcite{andersen2010continuous}.
	\item Has also done a great deal of theoretical work on estimation and
		inference in these models. Notably, a few papers have disentangled the 
		jumps from the continuous part in the presence of market microstructure,
		including  \textcite{li2013robust} and \textcite{li2016robust}. 
\end{itemize}
\end{frame}



\section{Model}

\begin{frame}[c]{Model}
\begin{align*}
	\color{blue} Y(t) &= \color{blue}  Y_0 + \int_0^t b^{Y}(s) d(s) + 
		\int_0^t dV^{1/2} (s) dW^v(s) + \sum_{s \leq t} J^{Y}(s) \\
	V(t) &= V_0 +  \int_0^t b^{V}(s) d(s) + \int_0^t dU^{V^{1/2}} dW^v(s) + \sum_{s 			\leq t} J^{V}(s) \\
	K(t) &=  K_0 + \int_0^t b^K(s) d(s) + \int_0^t dU^{K^{1/2}} (s) dW^K(s) \\ 
\end{align*}
\end{frame}

\begin{frame}[c]{Jump Part}
\begin{center}
$$J^Y(t) = \sum_{s \leq t} \Delta J^Y(s) $$
\color{blue} $$K(t) = \E\left[\sum_{s \leq t} {\Delta J^Y(s)}^2 \middle\vert \F_{t-} \right] $$ \color{black} 
The 2\textsuperscript{nd} power variation is $\sum_{s \leq t} {\Delta J^Y(s)}^2$.
\end{center} 
\end{frame}


\begin{frame}[c]{Relationship Between Discrete and Continuous-Time}
\begin{itemize}
	\item \color{blue} $Y_t = \int_{t-1}^t dY(s)$, \color{black} 
		and similarly for other processes.
	\item The discrete time variables are increments (integrals) of the continuous 
		process.
	\item So we must relate the distributions of the integrals to the
		distributions of the processes.
	\end{itemize}
\end{frame}

\begin{frame}[c]{Key Assumptions}
\begin{itemize}
	\item The inter-day dynamics of the drift $BY_t$ are driven by $V_t$ \& $K_t$. 
	\item $W^V(t)$ \& $W^K(t)$ are independent of $W^Y(t)$ and $B^Y(t)$. 
		Effectively, there is no leverage effect in the innovation parts of the 
		processes, i.e. the discrete-time innovation is independent of the 
		discrete-time volatility. The relationship between the drift parts is left
		arbitrary.
	\item $Y(t)$ is an infinite activity process. (We have infinitely many jumps in 					every period.)
\end{itemize}
\end{frame}


\begin{frame}[c]{Distribution of the Increments}
\includegraphics[width=4in]{jump_brownian_motion_decomposition.png}
\footfullcite[1012]{aitsahalia2012analyzing}
\end{frame}


\begin{frame}[c]{Number of Jumps}
\begin{itemize}
	\item \textcite{aitsahalia2012analyzing} find that jumps have infinite 		
		activity, and account for between 5\%-15\% of the variation the quadratic 
		variation, and even higher proportion for non-equities.
	\item Using $1$-second data, we find about $28\%$ of the variation is explained 
		by the jumps in the SPY during $2013-2014$.
	\item The jumps have fatter tails, and so this is an underestimate of the 
	percentage of risk explained.
	\item We also find jumps in everyone one of the days, with a median number of 				$944$.
	\item Our numbers are likely higher than those in the previous literature because 			we are using a less conservative truncation rule, which we derived using the 			additional structure in our model to unbiasedly distribute the volatility 				between the two components.
\end{itemize}
\end{frame}

\section{Relating $p_t$ to $V_t$ and $K_t$}

\begin{frame}[c]{Time-Changes}
\begin{itemize}
	\item A time-change that is independent of the process being transformed is 				called a subordinator.
	\item We proceed by a series of two changes.
	\item The method of representing the continuous part as a time-changed Brownian 			motion, has been remarked on before, and we flesh it out further.
	\item We develop a novel change for the jumps to relate $K_t$ to the 
		distribution of the jump part, and thereby derive the a model-free measure of 			jump volatility.
\end{itemize}
\end{frame}


\begin{frame}[c]{The Continuous Part.}
\begin{itemize}
	\item By The Dambis, Dubins \& Schwarz Theorem, \color{blue} 
		$$\int_0^t dV^{1/2}(s)  dW(s) = W(V(t))$$ \color{black}
	\item $W(t)$ is a Wiener process, so $W(V(t)) - W(V(t-1)) \sim \N(0, V_t)$. 
	\item The main issue here is we need a conditional distribution, not a 					marginal one.
	\item $W^v(t)$ is independent of $W^Y(t)$ and $B^Y(t)$, so the marginal and 				conditional distributions coincide.
	\item Consequently,  \color{blue} $$\left(Y_t - B^Y_t - J^Y_t \right) | V_{t} \sim \N(0, V_{t})$$
\end{itemize}
\end{frame}

\begin{frame}[c]{The Discontinuous Part}
\begin{theorem}
Let $J(t)$ be a pure jump local martingale that is bounded in probability for all $t$ with predictable quadratic covariation: $K(t) = \langle J \rangle(t)$.
Let its intrinsic filtration be denoted $\F^J_t$. 
In addition, define the auxiliary processes $K(s)$ and $T(s)$ such that 
$$J(t) = \int_{0}^t  \int_{\mathcal{K}} k \textbf{1}_{\lbrace \Delta T(s) = 1 \rbrace} dK(k, s) dT(s)$$
Under some technical conditions and mean independence between $T(t)$ and $K(t)$ with respect to  $\F^J_{t-1}$, $J\left( K^{-1}(t)\right) \lequals \Lap(t)$, where $\Lap(t)$ is a Laplace process with  drift $0$ and scale parameter $1$.
\end{theorem}
\end{frame}


\begin{frame}[c]{Properties of Laplace Distributions}
\begin{itemize}
	\item if $Z \sim \exp(1)$ and $X \sim \N(0,1)$ independently, 				
		then \color{blue} $$\sqrt{Z} \times X \sim \Lap\left(0, 1\right)$$ 
		\color{black}
	\item Laplace is the finite variance geometric stable 
		distribution.
	\item A geometric stable distribution is what limits of sums appropriately 
		scaled random variables converge to when the number of summands is itself
		random. 
\end{itemize}
\end{frame}

\begin{frame}[c]{Proof Sketch}
\begin{itemize}
	\item A jump process has to two types of variation, the jump magnitudes and 		jump locations.
	\item According to \textcite{meyer1971demonstration}, you can convert any 
		finite \qt{subset} of \color{blue} $T(t)$ \color{black} into a unit-rate 				\color{blue} Poisson \color{black} process by rescaling
		$T(t)$ by $\langle T \rangle (t)$. 
	\item By conditioning on the locations, you can show $K(t)$ rescaled by 
		$\langle K 	\rangle(t0)$ is the limit of a martingale difference sequence and 
		hence $K(t)$ a time-changed Wiener process.
	\item Then by \color{blue} integrating \color{black} out the locations, and taking
		limits appropriately,you recover that the original process is a 
		\color{blue} time-changed Laplace process. \color{black} 
\end{itemize}
\end{frame}


\begin{frame}[c]{What is the Appropriate Time Change}
\begin{itemize}
	\item What we need is the \color{blue} composition of the two time-changes 
		\color{black} used above, not them independently.
	\item Variances do not care if you increase the magnitude of each jump or increase
		the number of jumps.
	\item Hence, we need the predictable quadratic variation of the original process,
		i.e. \color{blue} $K(t)$ \color{black}. 
\end{itemize}
\end{frame}


\begin{frame}[c]{Construction of a $t-1$ Measurable Density.}
\begin{itemize}
	\item Again, by independence assumptions above, we can integrate $\Lap(K(t))$. 
	\item Hence, \color{blue} $J^Y_t \vert K_t \sim \Lap(K_t)$ \color{black} 
	\item $V_t$ and $K_t$ are only known at the end of date $t$.
	\item \color{blue}  $$p_t = \int_{V_t, K_t} \left[N(f(V_t, K_t), V_t)  \ast 
		\Lap(K_t) \right] g\left(V_t, K_t \middle\vert \F_{t-1} \right) 				d(V_t, K_t)$$  \color{black}
	\item We have reduced the problem of estimating the dynamics of $p_t$ to 					estimating the dynamics of $V_t$ and $K_t$. 
	\item This is substantially easier because $K_t$ and $V_t$ are reasonably 
		well-behaved, predictable objects. $Y_t$ is not. 
\end{itemize}
\end{frame}


\section{Estimation and Empirical Results}


\begin{frame}[c]{Data}
\begin{itemize}
	\item \color{blue} SPY S\&P 500 ETF  \color{black} 
	\item Tick data from TAQ 2013-2014, downsampled to \color{blue} 
		$1$-second \color{black} intervals.
	\item Used the standard filters to remove extraneous trades. 
\end{itemize}
\end{frame}

\begin{frame}[c]{Estimation Procedure}
\begin{itemize}
	\item Used pre-averaging and truncation to disentangle the jumps 				
		from the continuous part in the presence of market microstructure noise. 
	\item Essentially a combination of \textcite{li2013robust} and 
		\textcite{li2016robust}
	\item Use a preaveraged power estimator to estimate $K_t$. 
	\begin{itemize}
		\item Numerically, it's identical to a heavily smoothed one.
		\item Effectively, you are either integrating the jumps or integrating the 					smoothed jumps, but the integration itself smooths so it doesn't matter.
		\item Under some mild conditions, most notable that the last return's 						contribution to the integral is asymptotically negligible.
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[c]{$K_t$ and $V_t$} 
\includegraphics[width=4in]{volatilities.png}
\end{frame}





\begin{frame}[c]{$VAR(2)$ Specification}
\begin{align*}
\begin{pmatrix} \log V_t  \\ \log K_t  \end{pmatrix} 
&= \begin{pmatrix} -3.6 \\  -5.5 \end{pmatrix}
+ \begin{pmatrix} 0.50 & -0.08 \\ 0.43 & -0.07 \end{pmatrix} 
\begin{pmatrix} \log V_{t-1} \\  \log K_{t-1}  \end{pmatrix} \\
&+ \begin{pmatrix} 0.23 & 0.03 \\ 0.23 & 0.01 \end{pmatrix} 
\begin{pmatrix} \log V_{t-2} \\  \log K_{t-2}  \end{pmatrix} 
+ \N\left(\begin{pmatrix} 0 \\ 0 \end{pmatrix}, \Sigma\right)  
\end{align*}
$$ \Sigma = \begin{pmatrix} 0.39 & 0.17 \\ 0.17 & 0.26 \end{pmatrix} $$
\end{frame}


\begin{frame}[c]{Results}
\begin{itemize}
	\item $BIC$ chooses $2$ lags, but $V_t$ and likely $K_t$ has long memory, so 
		we should probably do something more sophisticated here. 
	\item The correlation between $\log K_t$ and $\log V_t$ is $0.71$.
	\item The in-sample $\mathbb{R}^2$ is $0.44$ for $K_t$, and $0.40$ for $V_t$.
\end{itemize}
\end{frame}


\begin{frame}[c]{Nonparametric Model for the Dynamics}
\begin{itemize}
	\item One can model the dynamics of $K_t$ \& $V_t$ nonparametrically using 
		Bayesian nonparametric conditional density estimation.
	\item If you do this, you can show that $p_t$ is estimated consistently.
\end{itemize}
\end{frame}

\begin{frame}[c]{Density Forecast}
\begin{adjustwidth}{-.25in}{-.25in}
\begin{tabular}{cc}
Jump Volatility Model & $RV - SV $ with $t(4)$ errors \\ 
\includegraphics[width=2.25in]{jv_model_forecast.png}  &
\includegraphics[width=2.25in]{t_dist_sv_comparison.png} \\
LPS: -$3.63$ \footnotemark[1] & LPS: $-3.57$ \footnotemark[1] \\
\end{tabular}
\end{adjustwidth}
\footnotetext[1]{Mean Log Predictive Score (Lower is Better)}
\end{frame}



\begin{frame}[c]{Density Forecast Evaluation}
\begin{adjustwidth}{-.25in}{-.25in}
\begin{tabular}{ccc}
PIT Autocorrelations &  PIT & QQ Plot \\ 
\includegraphics[width=1.4in]{var_pit_autocorrelation.png} &
\includegraphics[width=1.4in]{var_pit_density.png} &
\includegraphics[width=1.4in]{var_pit_qq.png}
\end{tabular}
\end{adjustwidth}
\end{frame}



\section{Wrapping Up}

\begin{frame}[c]{Conclusion}
\begin{itemize}
	\item We characterize the discrete-time daily return density using the 					nonparametric structure and data studied in the high frequency literature. 
	\item In so doing we provide a novel parsimonious model in terms of 						economically interpretable, well-behaved variables that can be estimated 			using high-frequency data. 
	\item We then show that the model has good theoretical and practical properties.
\end{itemize}
\end{frame}


\begin{frame}[c]{Future Work}
\begin{itemize}
	\item Much of the results here are partial, and need to be furthered to ensure 
		their practical and theoretical robustness.
	\item Additionally, one can generalize this characterization to higher dimensions, 		and thereby characterize all of the dynamics and cross-sectional 						relationships. Since $V_t$ and $K_t$ have factor structure, we can 
		practically and rigorously characterize the entire high-dimensional density.
\end{itemize}
\end{frame}



\end{document}

