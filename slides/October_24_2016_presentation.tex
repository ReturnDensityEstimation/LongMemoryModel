\documentclass[fleqn,t,smaller,xcolor=dvipsnames]{beamer}
\usetheme{Warsaw}
\usecolortheme{dolphin}
\usepackage{datetime}
\usepackage{graphicx}
\usepackage{sidecap}
\usepackage{commath}
\usepackage{mathtools}
\usepackage{caption}


\graphicspath {{figures/}}


\newcommand{\E}{\mathbb{E}}                                                                                        
\newcommand{\Var}{\mathbb{V}\mathrm{ar}}                                                                           
\newcommand{\Cov}{\mathbb{C}\mathrm{ov}}                                                                           
\newcommand{\N}{\mathcal{N}}                                                                                       
\newcommand{\lequals}{\stackrel{\mathcal{L}}{=}}                                                                   
\newcommand{\aequals}{\stackrel{a.s.}{=}}                                                                          
\newcommand{\I}{\mathbb{I}}                                                                                        
\newcommand{\F}{\mathcal{F}}                                                                                       
\newcommand{\qt}[1]{\lq\lq#1\rq\rq}  


\setbeamertemplate{navigation symbols}{}  
\setbeamertemplate{headline}{}   
\captionsetup{labelformat=empty}
\defbeamertemplate*{footline}{shadow theme}
{%
  \leavevmode%
  \hbox{\begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm plus1fil,rightskip=.3cm]{author in head/foot}%
    \usebeamerfont{author in head/foot}\hfill\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm,rightskip=.3cm plus1fil]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle\hfill\insertframenumber\,/\,\inserttotalframenumber
  \end{beamercolorbox}}%
  \vskip0pt%
}


\newcommand\itembullet{
  \begingroup
  \leavevmode
  \usebeamerfont*{itemize item}%
  \usebeamercolor[fg]{itemize item}%
  \usebeamertemplate**{itemize item}%
  \setlength\labelsep   {\dimexpr\labelsep + 0.5em\relax} 
  \endgroup
}
\newcommand{\spitem}{\vspace{.3cm}\item}

\AtBeginSection[]{
{
  \begin{frame}[c,plain, noframenumbering]
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \end{frame}
}
}


\title[]{A Tale of Two Infinities: Using High-frequency Data to Estimate the Daily Return Density}
\author[Paul Sangrey]{Paul Sangrey}
\institute{University of Pennsylvania \\ \textit{sangrey@sas.upenn.edu}}
\date{October 24, 2016}


\begin{document}

\begin{frame}[plain, noframenumbering]
	\maketitle
\end{frame}


\begin{frame}[c]{Daily Return Density}
\begin{itemize}
	\item They key characterization of financial risk is the discrete-time 
		density of the log-return:
		\color{blue} 
			$f_t = p\left( Y_t \middle\vert \F_{t-1}\right)$. 
		\color{black}
	\item So we have a sequence of daily densities $f_{t}, f_{t+1}, \ldots$
	\item Also known as the physical measure. 

	\item Continuous time Representation
	\begin{itemize}
		\item $Y(s) = Y_0 + \int_0^t b^{Y}(s) d(s) + \int_0^t dV^{1/2} (s) dW^Y(s) + 					\sum_{s \leq t} J^{Y}(s) $
		\item \color{blue}
			$f_t = p\left(Y(t) - Y(t-1) \middle\vert \F_{t-1} \right)$
			\color{black}
	\end{itemize}
	\item We focus on the daily frequency. 
		I use $Y(t)$ to refer to a stochastic processes and $Y_t$ to refer to its 			discrete time increments.
\end{itemize}
\end{frame}


\begin{frame}[c]{The Issues}
\begin{itemize}
	\item Financial data is not very well-behaved and so most distributional / 
		functional-form assumptions are not good approximations.
	\item In fact, even most nonparametric assumptions, such smoothness of the 				price path or multiple absolute moments,  do not hold.
	\item Jumps in particular violate most regularity conditions one might want to 			employ. 
	\item $f_t$ is an infinite-dimensional object. So we are estimating a sequence 			of time-varying, latent infinite-dimensional objects.
\end{itemize}
\end{frame}

\begin{frame}[c]{Current State of the Literature: Parametric Models}
	\begin{itemize}
		\item Include \color{blue} GARCH \color{black} and stochastic volatility 				\color{blue} (SV) \color{black} models.
		\item Has good practical performance most of the time
		\item Puts more restrictions on the density than hold in practice.
	\end{itemize}
\end{frame}

\begin{frame}[c]{ Models for specific statistics}
\begin{itemize}
	\item More flexible than the above models. 
	\item Realized volatility \color{blue} (RV) \color{black} and
		\color{blue} CAViaR \color{black} are notable examples.
	\item Have two issues:
	\begin{enumerate} 
		\item Often model the dynamics parametrically.
		\item Unable to utilize information in the center of the distribution 					to estimate the tails.
	\end{enumerate}
\end{itemize}
Note: It is not obvious how to extend either of these classes of models to high 		dimensions.
	Although there has been some good work on this topic. 
\end{frame}
	
\begin{frame}[c]{Stochastic Discount Factor $(SDF)$ estimation}
\begin{itemize}
	\item One notable place a good estimate of $f_t$ is necessary is $SDF$ 					estimation.
	\item The \color{blue} $SDF$ \color{black} is estimated as a ratio of the 					risk-neutral measure and the physical measure.
	\item The literature estimates the risk-neutral measure using option prices in			a non-parametric time-varying way. 
	\item However, because no good general method for estimating the physical 				measure exists, the \color{blue} $SDF$ \color{black} estimates in the 				literature are puzzling
\end{itemize}
\end{frame}



\begin{frame}[c]{This approach}
We estimate $f_t$ nonparametrically from high-frequency data in a manner analogous to how realized volatility estimates volatility.
Our estimator has several nice properties. 
\begin{itemize}
	\item Uses a relatively standard continuous-time \color{blue} nonparametric 				\color{black} model for $f_t$. 
	\item Allows for \color{blue} jumps. \color{black}
	\item Utilizes stochastic process representation theorems to reduce the  				dynamic infinite-dimensional problem to a low-dimensional one in terms of 			economically relevant variables.
	\item Provides a \color{blue} consistent \color{black} Bayesian posterior 				estimator for $f_t$.
	\item Utilizes modern Bayesian computational techniques to estimate the model 			efficiently.
	\item Is straightforwardly \color{blue} extensible \color{black} to different 				specifications for the dynamics or towards high-dimensions.
\end{itemize}
\end{frame}	
	
\begin{frame}[c]{Contributions}
\begin{itemize}
	\item Characterize the dynamics of $f_t$ in terms of a \color{blue} 
		low-dimensional \color{black} object.
	\item Model the dynamics of those objects nonparametrically, and then 
		integrate them out to estimate $f_t$. 
	\item Provide a \color{blue} consistent \color{black} posterior estimator of 			$f_t$. 
	\item Provide a consistent posterior estimator for the transition density of 			the volatility $g(V_t | V_{t-1} \ldots)$.
	\item Define the \color{blue} integrated jump mean $N_t$  \color{black} and 				show that it fully characterizes the discrete-time dynamics of the the 				jumps.
	\begin{itemize}
		\item $N_t = \int_{t-1}^t \E\left[J^Y(s) | \F_{s-} \right] ds$. 
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[c]{Integrated jump mean}
\begin{itemize}
	\item $N_t = \int_{t-1}^t \E\left[J^Y(s) \middle\vert \F_{s-} \right] ds$.
	\item $N(t)$ \color{blue} explains the dynamics of the jumps \color{black} 				similar to how volatility explains the dynamics of the continuous part. 
	\item It's a first moment instead of the second because of the difference in 			scale between the jump part and the continuous part.
	\item It is \color{blue} likely predictable \color{black} because the second 			moments of the jumps are predictable.
	\item Very preliminary empirical investigations reveal an $AR(1)$ coefficient 			of approximately $.6$.
	\item It can be estimated by exploiting its known smoothness and then 					integrating.
	\item This estimation is non-trivial because we must disentangle the jumps 			from the continuous part in the presence of microstructure noise.
\end{itemize}
\end{frame}


\section{Model}

\begin{frame}[c]{Model}
\begin{align*}
Y(t) &= Y_0 + \int_0^t b^{Y}(s) d(s) + \int_0^t dV^{1/2} (s) dW^v(s) + \sum_{s \leq t} J^{Y}(s) \\
V(t) &= V_0 +  \int_0^t b^{V}(s) d(s) + \int_0^t dU^{V^{1/2}} dW^v(s) + \sum_{s \leq t} J^{V}(s) \\
N(t) &=  N_0 + \int_0^t b^N(s) d(s) + \int_0^t dU^{N^{1/2}} (s) dW^N(s) \\ 
\end{align*}
\end{frame}

\begin{frame}[c]{Jump Part}
\begin{align*}
&J^Y = \int_0^t \int_E \delta(s,z) 1_{\lbrace \vert \delta(s,z) \vert \leq 1\rbrace} (\mu - \nu)(ds, dz)   \\
&\hspace{.5cm} +  \int_0^1 \int_E \delta(s,z) 1_{\lbrace \vert \delta(s,z) > 1 \vert \rbrace} \mu(ds,dz) \\
&\mu(ds,dz) \mbox{ is a Poisson random measure with compensator } \nu(ds,dz) \\
&N(s) = \E\left[\nu(ds,dz) \middle\vert \F_{s-} \right] 
\end{align*}
\end{frame}


\begin{frame}[c]{Key Assumptions}
\begin{itemize}
	\item The drift part $B_t = \int_t^{t-1} b(s) ds$ is continuous. 
		That is, there are no predictable jumps. 
	\item The inter-day dynamics of $B_t$ are driven by $V_t$. 
	\item $W^V(t)$ is independent of $W^Y(t)$ and $B^Y(t)$. 
		Effectively, there is no leverage effect in the Wiener part of the 					volatility.
		The relationship between the drift parts is left arbitrary.
	\item A particular kind of independence between $N(t)$ and the increments of			 the jump part. Effectively, we need the conditional mean and the 					innovation of the price process to be independent.
\end{itemize}
\end{frame}




%\section{Relating $f_t$ to $V_t$ and $N_t$}. 


\begin{frame}[c]{Representing the continuous part}
\begin{theorem}[Dambis, Dubins \& Schwarz]
	Let $M(t)$ be a continuous martingale with quadratic variation $\left[M				\right](t)$ such that $\left[M\right](\infty)  = \infty$ that is nowhere 			constant.
	Then $M(t) = \widetilde{W}(\left[M\right](t))$ where $\widetilde{W}$ is a 			Wiener process.
\end{theorem}
\begin{itemize}
	\item $Y(t) = \int_{t-1}^t b^Y(s) ds + \int_{t-1}^t dV^{1/2}(s) dW(s)$. 
	\item Specifically, \color{blue} $\int_0^t dV^{1/2}(s)dW(s) = 
		\widetilde{W}(V(t))$. \color{black}
\end{itemize}
\end{frame}

\begin{frame}[c]{Integrating the time-changed process.}
\begin{itemize}
	\item We know the distribution of increments of a Wiener process.
	\item \color{blue} 
		$\widetilde{W}(V(t)) - \widetilde{W}(V(t-1)) \sim \N(0, V_t)$.
		\color{black} 
	\item The main issue here is we need a conditional distribution, not a 					marginal one.
	\item In general, $V_t$ and the implied increments of $\widetilde{W}$ are not 			necessarily independent.
	\item However, since $W^v(s)$ are assumed independent of $W^Y(s)$ and 
		$B^Y(s)$, they are. 
	\item Thus, $Y_{t} | (V_{t}, B_t) = B_t + \N(0, V_{t})  = 
		\N\left(B_{t} , V_{t} \right)$.
	\item However, we assumed that all of the inter-day dynamics of the drift were 		driven by the volatility. 
		Thus \color{blue} $Y_{t} | V_{t}  = \N(f(V_{t}), V_{t}))$. \color{black}
\end{itemize}
\end{frame}




\begin{frame}[c]{Representing the discontinuous Part}
\begin{theorem}[Meyer 1971]
	Let $X(t)$ be a quasi-left-continuous point process with continuous compensator $N(t)$, then there exists a Poisson process $\mathcal{P}$ of rate $1$ such that $X(t) = \mathcal{P}(N(t))$.
\end{theorem}
\begin{itemize}
	\item The definition of $N(t)$ above implies that $N(\infty) = \infty$ and 
		that $N(t)$ is a continuous process. 	
	\item Thus \color{blue} $J^Y(t) = \mathcal{P}(N(t))$, \color{black} where $				\mathcal{P}$ is a Poisson process.
\end{itemize}
\end{frame}


\begin{frame}[c]{Integrating the jump process}
\begin{itemize}
	\item $J^Y(t) = \mathcal{P}(N(t)) \implies J^Y_t \sim
		\textit{p}(N_t)$ in distribution where $\textit{p}$ is a 
		Poisson distribution.
	\item As above, we need an independence of the increments of $\mathcal{P}$ and 			$N(t)$. 
			This is a rather weak assumption because it is essentially assuming 
			that the innovations to the jump process are independent of their 					conditional expectations.	
	\item Thus \color{blue} $J^Y_t \vert N_t \sim \textit{p}(N_t)$.\color{black} 
\end{itemize}
\end{frame}


\begin{frame}[c]{Construction of a $t-1$ measurable density. }
\begin{itemize}
	\item $V_t$ and $N_t$ are only known at the end of date $t$.
	\item Thus $f_t \neq \N(f(V_t), V_t) \ast \textit{p}(N_t)$.
	\item \color{blue} $f_t = \int_{V_t, N_t} \left[N(f(V_t), V_t)  \ast 
		\textit{p}(N_t) \right] g\left(V_t, N_t \middle\vert \F_{t-1} \right) 				d(V_t, N_t)$.  \color{black}
	\item Thus we have reduced the problem of estimating the dynamics of $f_t$ to 			estimating the dynamics of $V_t$ and $N_t$. 
	\item This should be substantially easier because $V_t$ is predictable and 
		$N_t$ likely is as well. 
		$Y_t$
		 is not. 
\end{itemize}
\end{frame}

\section{Bayesian Estimation of $f_t$ when paths are continuous.}



\begin{frame}[c]{Assumptions on Volatility}
\begin{itemize}
	\item \color{blue} Stationary. \color{black}
	\item \color{blue} Markov \color{black} of order $k$. 
	\item The transition density $g$ implied by the above conditions satisfies 
		the following.
	\item Let $\omega_{t-1} = V_{t-1} \ldots V_{t-k}$, and $q(\omega_{t-1})$ be 			its implied unconditional distribution.
	\begin{itemize}
		\item $g$ is \color{blue} nowhere zero and bounded \color{black}
			 by $M < \infty$.
		\item $(\omega_{t-1}, V_t) \to g(V_t | \omega_{t-1})$ is \color{blue} 					jointly continuous. \color{black}
		\item Some tail conditions.
		\begin{itemize}
			\item $\left\vert \int_{\omega_{t-1}} \int_{V_t} g(V_t | \omega_t) 
				\log g(V_t | \omega_{t-1}) dV_t q(\omega_{t-1}) d\omega_{t-1}
				\right\vert < \infty$
			\item $\left\vert \int_{\omega_{t-1}} \int_{V_t} g(V_t | \omega_{t-1}) 					\log\frac{g(V_t | \omega_{t-1}}{\psi_{\omega_{t-1}}(V_t)} dV_t 						q(\omega_{t-1}) d\omega_{t-1}\right\vert < \infty$ where 
				$\psi_{\omega_{t-1}} (V_t) = \inf_{u \in [V_t - 1, V_t + 1]} 
				g(u | \omega_{t-1})$.
			\item There exists $\eta > 0$ such that $\int_{\omega_{t-1}} 
				\int_{V_t} | V_t|^{2 + \eta} g(V_t | \omega_{t-1}) dV_t 								q(\omega_{t-1}) d\omega_{t-1} < \infty$
		\end{itemize}	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[c]{Construction of $f_t$}
\begin{theorem}
Let $\phi$ be the Gaussian density and $g$ the transition density of the $V_t$ process.
Assume that $Y_t$ has continuous price-paths and $B_t = A\log(V_t)$.
Then we can represent the the true density $f_{0,t}$ as follows. 
$$f_{0,t}(x) = \int_{z} \phi\left(\frac{x - A_0\log(z)}{\sqrt{z}} \right) g_0\left(z \middle\vert V_{0,t-1} \ldots V_{0,t-k} \right) dz$$

\end{theorem}
\end{frame}

\begin{frame}[c]{Consistent Posterior Estimation of $f_t$}

\begin{theorem}

Define $\Pi^f_{m,t}\left(f_t \middle\vert \mathcal{X}\right)$ as the posterior density over $f_t$ implied by the following.

\begin{align*}
f_t(x) &= \int_{\lbrace V_t \rbrace } \int_A \int_G \int_{z} \phi\left(\frac{x - A\log(z)}{\sqrt{z}} \right) g\left(z\middle\vert V_{t-1} \ldots V_{t-k} \right) \\
 &\hspace{.5cm} dz d\Pi^G_{t}(g \vert \lbrace V_t \rbrace ) 
d\Pi^A_{t}(A | \lbrace V_t \rbrace, \mathcal{X}) d\Pi^{\lbrace V_t\rbrace}_{m}(\lbrace V_t \rbrace | \mathcal{X}) 
\end{align*}

Let $B_{\epsilon}$ be an $L_{1}$ neighborhood of $f_{0,t}$ in the space of densities.
Assume that $\Pi^G_t$ and $\Pi^A_t$ are strongly consistent and that $\Pi^H_m$ is uniformly consistent in probability, i.e. for any $\delta > 0$, $\Pi^H_{m}\left(\left\lbrace V_t : \sup_{t} \left\vert V_t - V_{0,t} \right\vert < \delta \right\rbrace \middle\vert \mathcal{X} \right) \to_{p} 1$. 
Then $\lim_{t,m \to \infty} \Pi_{t,m}^{f}(B_{\epsilon}) \to_p 1$ for all $\epsilon > 0$. 
\end{theorem}\end{frame}

\begin{frame}[c]{Notes on the Assumptions}
\begin{itemize}
	\item Standard arguments concerning Bayesian conditional density estimation 			imply consistency for  $g$. 
	\item Standard Bayesian parametric arguments imply consistency for $A$.
	\item Frequentist arguments for consistency of  $RV_t$ imply 							consistency for  $V_t$ when given a Bayesian 	interpretation. 
\end{itemize}
\end{frame}

\begin{frame}[c]{Notes on the proof}
\begin{itemize}
	\item We take the distance between a draw from the posterior and a $f_{0,t}$ 			and relate it to the difference between draws of the posteriors of parts of 		the model and their implied true parameters.
	\item Because realized volatility converges uniformly in probability, not 				almost surely, we cannot get almost sure convergence.
\end{itemize}	
\end{frame}

\section{Estimation}


\begin{frame}[c]{Computation}
Sampler
\begin{enumerate}
	\item Draw $V_t$ from the density implied by a frequentist estimator given
		the data and other parameters.
	\item Draw $g \vert \lbrace V_t \rbrace_{t=1}^T$.
	\item Draw $y_t \sim N(A \log(V_t), V_t)$. 
	\item Draw hyperparameters given everything else. 	
\end{enumerate}
Notes
\begin{itemize}
	\item We develop a new method for drawing conditional densities using 					Dirichlet process mixtures to compute step $2$ efficiently with fewer 
		Gibbs approximations than the samplers in the literature.
\end{itemize}
\end{frame}


\section{Some Preliminary Empirics}


\begin{frame}[c]{Parametric Model for the Dynamics}
\begin{itemize}
	\item As of currently, we do not have a working implementation of the 
		nonparametric conditional density for the dynamics.
	\item Instead, we show you results where we estimate the above model, 
		but with a parametric model for a the volatility.
	\item We also use slightly different assumptions on the dynamics of the
		drift, but not consequentially so. 
\end{itemize}
\end{frame}


\begin{frame}[c]{Forecast Density of $Y_t$}
\includegraphics[width=4in]{stochastic_density_forecasted.png}
\end{frame}

\begin{frame}[c]{Evaluation of Log-Return Forecast}
\begin{tabular}{cc}
Probability Integral Transform (PIT) & PIT Autocorrelation \\
\includegraphics[width=2in]{stochastic_density_pit.png} &
\includegraphics[width=2in]{stochastic_density_pit_autocorr.png}
\end{tabular}
\end{frame}

\begin{frame}[c]{Forecast Density of $V_t$}
\includegraphics[width=4in]{stochastic_vols_forecasted.png}
\end{frame}

\begin{frame}[c]{Evaluation of Volatility Forecast}
\begin{tabular}{cc}
Probability Integral Transform (PIT) & PIT Autocorrelation \\
\includegraphics[width=2in]{stochastic_vols_pit.png} &
\includegraphics[width=2in]{stochastic_vols_pit_autocorr.png}
\end{tabular}
\end{frame}


\section{Conclusion}

\begin{frame}[c]{Framework}
We develop an high-frequency estimator of the daily return density $f_t$ with the following properties.
\begin{itemize}
	\item Nonparametric
	\item Consistent
	\item Parsimonious
	\item Economically interpretable
	\item Extensible
\end{itemize}
\end{frame}

\begin{frame}[c]{Other Contributions}
\begin{itemize}
	\item Provide a \color{blue} consistent nonparametric estimator for $g$,					\color{black} the transition density  of the volatility process.
	\item Provide a scalar quantity \color{blue} $N_t$ \color{black} that is a 				\color{blue} sufficient statistic \color{black}  for the 							discrete-time dynamics of the jump process.
\end{itemize} 	
\end{frame}

\begin{frame}[c]{Future Work}
\begin{itemize}
	\item Develop a consistent Bayesian estimator for $N_t$.
	\item Use that to incorporate jumps in our estimate of $f_t$.  
	\item Provide a fuller empirical analysis and compare with other estimators
		in the literature.
	\item Consider a parametric model for the dynamics while remaining 
		nonparametric regarding the intra-day variation.
	\item Exploit factor structure in $V_t$ and $N_t$ to create a high-dimensional 		generalization.
\end{itemize}
\end{frame}


\end{document}

