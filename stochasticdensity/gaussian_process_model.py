""" This module contains code to estimate the gaussian process model described in the accompanying notes. """

import numpy as np
from scipy import stats
from bayesiankalman import mcmc
from bayesiankalman.kalmanfilter import kalman_smoother
from cdpm import CondDirichletProcessMix
import cdpm


# noinspection PyTypeChecker
class GPModel:
    """ Holds the state of the sampler and provides methods to draw from the posterior. """

    def __init__(self, data, ivol_coeff=None, imean_innov_var=1, num_lags=1,
                 imeans=None, ivols=None, imean_coeff=None, data_loadings=1, ranges=None,
                 leverage=False, sampler_cov=None, ivol_model=None, imean_model=None, alpha=None,
                 tau_h=None, prob_kernel_weights=None, tau_alpha=None):

        self._data = np.atleast_2d(data)
        self.leverage = leverage
        if ivol_model is not None:
            self._ivol_object = ivol_model
        else:
            self._ivol_object = IntegratedVolatility(ivols=ivols, ivol_coeff=ivol_coeff, data=data, num_lags=num_lags,
                                                     sampler_cov=sampler_cov, ranges=ranges, alpha=alpha, tau_h=tau_h,
                                                     prob_kernel_weights=prob_kernel_weights, tau_alpha=tau_alpha)

        if imean_model is not None:
            self._imean_object = imean_model
        else:
            self._imean_object = IntegratedMean(imeans=imeans, mean_coeff=imean_coeff, data=data,
                                                innov_var=imean_innov_var,
                                                data_loadings=data_loadings)
        self.counter = 0

    def __call__(self, prior):
        """
        Returns a draw from the posterior and updates the state.

        Returns
        -------
        draw: dictionary of the parameters and whether they were accepted.
        """
        draw = {}
        if 'ivols' in prior:
            draw.update(self._ivol_object(data=self.data, prior=prior['ivols'],
                                          imeans=self.data_loadings * self.imeans))

        if 'imeans' in prior:
            if self.leverage:
                if self.counter < 500:
                    external_regressor = np.log(self.ivols) - np.mean(np.log(self.ivols))
                else:
                    external_regressor = np.log(self.ivols) - self._ivol_object.log_vol_uncond_mean
                draw.update(self._imean_object(data=np.sum(self.data, axis=1),
                                               ivols=self.ivols,
                                               prior=prior['imeans'],
                                               external_regressor=external_regressor))
            else:
                draw.update(self._imean_object(data=np.sum(self.data, axis=1), ivols=self.ivols, prior=prior['imeans']))
        return draw

    def forecast(self, num_periods, prior):

        ivol_model = self._ivol_object.forecast(num_periods, prior)
        log_vol_deviations = np.log(ivol_model.ivols) - ivol_model.log_vol_uncond_mean
        imean_model = self._imean_object.forecast(num_periods, external_regressors=log_vol_deviations)

        data = compute_data(ivol_model.ivols, imean_model.imeans, num_obs_per_day=self.num_obs_per_day)

        return_model = GPModel(ivol_model=ivol_model, imean_model=imean_model, data=data, leverage=self.leverage)

        return return_model

    def forecast_draws(self, num_periods, prior):

        model = self.forecast(num_periods=num_periods, prior=prior)

        return model.data, model.ivols, model.imeans

    @property
    def num_periods(self):
        return self.data.shape[0]

    @property
    def num_obs_per_day(self):
        return self.data.shape[1]

    @property
    def ivols(self):
        return self._ivol_object.ivols

    @ivols.setter
    def ivols(self, val):
        self._ivol_object.ivols = val

    @property
    def imeans(self):
        return self._imean_object.imeans

    @imeans.setter
    def imeans(self, val):
        self._imean_object.imeans = val

    @property
    def data_innov_var(self):
        return self._imean_object.innov_var

    @data_innov_var.setter
    def data_innov_var(self, val):
        self._imean_object.innov_var = val

    @property
    def data(self):
        return self._data

    @property
    def imean_coeff(self):
        return self._imean_object.imean_coeff

    @property
    def imean_innov_var(self):
        return self._imean_object.innov_var

    @property
    def data_loadings(self):
        return self._imean_object.data_loadings

    @data_loadings.setter
    def data_loadings(self, val):
        self._imean_object.data_loadings = val

    @property
    def drift_innov_var(self):
        return self._imean_object.innov_var


# noinspection PyTypeChecker,PyUnresolvedReferences
class IntegratedMean:
    """ This class holds the mean and contains methods to update its state  """

    # noinspection PyUnresolvedReferences
    def __init__(self, imeans=None, mean_coeff=0, data=None, innov_var=1, num_periods=None, data_loadings=1):
        """
        Setups the class with given parameters or reasonable defaults.

        Parameters
        ----------
        data_loadings
        imeans : array-like or double
            The time-varying mean of the process.

        data : 1d or 2d array of floats
            The data we are considering. It if is two dimensional the first dimension is assumed to be the number of
            days, and the second the observations per day. Otherwise, we have one observation per day.
        num_periods : positive int
        """
        if imeans is not None:
            self.imeans = np.asarray(imeans)
        elif data is not None:
            self.imeans = np.asarray(np.sum(data, axis=1)) / 10 if data.ndim == 2 else np.asarray(data) / 10
        elif num_periods is not None:
            self.imeans = np.zeros(num_periods)
        else:
            raise ValueError('Incorrect Arguments. Either imeans, data or time_dim must be given.')

        self.data_loadings = np.asarray(data_loadings)
        self.innov_var = np.asarray(innov_var)
        self.imean_coeff = np.asarray(mean_coeff)
        self.counter = 0

    def __call__(self, data, prior, ivols, external_regressor=None, ivol_innov_var=None):
        """
        Returns a dictionary of draws from the conditional posterior of its parameters, and updates its state.

        Parameters
        ----------
        ivol_innov_var
        data : 2d array-like
        external_regressor : 1d array-like, optional
            The integrated volatility in each day.
        ivol_coeff : 1d array-like, optional

        Returns
         -------
        draw : dict
            The parameters that are drawn
        """
        self.counter += 1
        self.imeans = self.draw_means(data, ivols, external_regressor)

        if 'imean_coeff' in prior:
            self.imean_coeff = self.draw_imean_coeff(prior['imean_coeff'], external_regressor)
            imean_coeff = self.imean_coeff
            imean_coeff[0] *= self.data_loadings
            if external_regressor is not None:
                imean_coeff[-1] *= self.data_loadings
        else:
            imean_coeff = self.imean_coeff

        if external_regressor is not None:
            r_squared = 1 - ((self.innov_var + self.imean_coeff[-1] * ivol_innov_var) / np.var(self.imeans))
        else:
            r_squared = 1 - (self.innov_var / np.var(self.imeans))

        if 'innov_var' in prior:  # and self.counter > 100:
            self.innov_var = self.draw_innov_var(prior['innov_var'], external_regressor)

        if 'data_loadings' in prior:
            data_loadings_draw = self.draw_data_loadings(data, ivols, prior['data_loadings'])
            self.data_loadings = data_loadings_draw['data_loadings']
            loadings_draw = {'data_loadings_accepted': data_loadings_draw['data_loadings_accepted']}
        else:
            loadings_draw = {'data_loadings_accepted': np.asarray(float(False))}

        draw = {'imean_innov_var': self.innov_var * self.data_loadings ** 2, 'imeans': self.imeans * self.data_loadings,
                'imean_coeff': imean_coeff, 'imean_r_squared': r_squared}
        draw.update(loadings_draw)

        return draw

    def draw_means(self, data, ivols, external_regressor=None):
        """
        Uses a kalman smoothing algorithm to draw the time-varying imeans.

        Parameters
        ----------
        ivols
        data : 2d array-like
        external_regressor : 1d array-like, optional
            The integrated volatility in each day.

        Returns
        -------
        draw = ndarray
            A draw from the conditional posterior of the imeans.

        """
        if external_regressor is not None:
            external_regressor = np.ravel(external_regressor)
            drift = self.imean_coeff[0] + self.imean_coeff[-1] * external_regressor
            state_dim = self.imean_coeff.size - 2
            state_mean = np.hstack([drift.reshape(self.num_periods, 1), np.zeros((self.num_periods, state_dim - 1))])

        else:
            drift = self.imean_coeff[0]
            state_dim = self.imean_coeff.size - 1
            state_mean = np.hstack([drift, np.zeros(np.max([state_dim - 1, 0]))])

        data_loadings = np.hstack([np.asscalar(self.data_loadings), np.zeros(np.max([state_dim - 1, 0]))])
        state_trans = mcmc.factor_model_state_trans(self.imean_coeff[1:state_dim + 1])
        state_innov_var = np.atleast_2d(np.zeros_like(state_trans))
        state_innov_var[0, 0] = self.innov_var
        scaling_factor = 1

        draw, _, _ = kalman_smoother(data=data,
                                     data_loadings=data_loadings / scaling_factor,
                                     data_mean=0,
                                     data_innov_var=ivols.reshape(self.num_periods, 1),
                                     state_trans=state_trans,
                                     state_mean=state_mean * scaling_factor,
                                     state_innov_var=state_innov_var * scaling_factor ** 2,
                                     stationary=True,
                                     time_varying=True
                                     )
        imeans = np.squeeze(draw[:, 0] / scaling_factor)

        return imeans

    def draw_innov_var(self, prior, external_regressor=None):
        """
        Uses conjugacy to return a draw from the conditional posterior of the data innovation variance.

        Parameters
        ---------
        external_regressor
        prior : dict
            Contains the prior parameters.
        external_regressor: array-like, optional
        Returns
        -------
        draw, positive double
        """

        if external_regressor is not None:
            num_lags = self.imean_coeff.size - 2
            external_regressor = np.ravel(external_regressor)
            regressor = mcmc.create_lag_matrix(data=self.imeans, lags=num_lags, external_regressors=external_regressor)
        else:
            num_lags = self.imean_coeff.size - 1
            regressor = mcmc.create_lag_matrix(self.imeans, num_lags)

        draw = mcmc.draw_innovation_var(regressor, self.imeans[num_lags:], self.imean_coeff, prior['scale'],
                                        prior['shape'])

        return np.ravel(draw)

    def draw_imean_coeff(self, prior, external_regressor=None):
        """
        Draws the imean_coeff parameter from its conditional posterior.

        It estimates both the autoregressive and drift parameters.

        We accept three different types of priors. If prior['var'] is a scalar it is assumed to be the variance of the
        imean's drift, and the autoregressive coefficient is assumed to be fixed. Else if prior is callable, we
        assume that it returns the prior of the variance, and a metropolis hastings step is used. Otherwise, we
        assume that it is a positive-definite covariance matrix for the prior coefficients.

        Parameters
        ----------
        external_regressor
        prior : dict
        external_regressor : array-like, optional

        Returns
        -------
        draw : 1d ndarray
        """

        if external_regressor is not None:
            external_regressor = np.ravel(external_regressor)
            num_lags = self.imean_coeff.size - 2
            regressor = mcmc.create_lag_matrix(data=self.imeans,
                                               lags=num_lags,
                                               external_regressors=external_regressor,
                                               prepend_ones=True)
        else:
            num_lags = self.imean_coeff.size - 1
            regressor = mcmc.create_lag_matrix(self.imeans, num_lags, prepend_ones=True)

        regressand = self.imeans[num_lags:]
        draw = self.coeff_draw(prior, regressand, regressor)

        return np.ravel(draw)

    def coeff_draw(self, prior, regressand, regressor):
        """
        Computes the coefficient by regressing regrassand and regressor and then re-weighting by the prior.

        Parameters
        ----------
        prior : callable
        regressand : ndarray
        regressor : ndarray

        Returns
        -------
        draw : ndarray
        """
        draw = mcmc.draw_matrix_coefficient(prior_mean=np.zeros_like(self.imean_coeff),
                                            prior_var=np.zeros((self.imean_coeff.size, self.imean_coeff.size)),
                                            regressand=regressand,
                                            regressor=regressor,
                                            innovation_var=self.innov_var)
        metrop_alpha = np.exp(np.log(prior(draw)) - np.log(prior(self.imean_coeff)))
        if not metrop_alpha > np.random.uniform():
            draw = self.imean_coeff

        return np.squeeze(draw)

    def draw_data_loadings(self, data, ivols, prior):
        """
        Draws the data loading parameter.

        Parameters
        ----------
        ivols
        data : array-like
        prior: dict # I rescaled the variance so that the innovation variance is 1.

        Returns
        -------
        draw: dict
        """
        regressand = data / np.sqrt(ivols)
        regressor = self.imeans / np.sqrt(ivols)
        draw = np.squeeze(mcmc.draw_matrix_coefficient(prior_mean=prior['mean'],
                                                       prior_var=prior['cov'],
                                                       regressand=regressand,
                                                       regressor=regressor,
                                                       innovation_var=1))

        if draw > prior['min']:
            return {'data_loadings': draw, 'data_loadings_accepted': np.asarray(float(True))}
        else:
            return {'data_loadings': self.data_loadings, 'data_loadings_accepted': np.asarray(float(False))}

    def forecast(self, num_periods, external_regressors):

        num_lags = np.amax(self.imean_coeff.shape[1] - 1 - external_regressors.shape[1], 0)
        future_imeans = forecast_imeans(num_periods, self.imean_coeff, self.innov_var,
                                        starting_point=self.imeans[-num_lags:], external_regressors=external_regressors)

        model = IntegratedMean(imeans=future_imeans, mean_coeff=self.imean_coef, innov_var=self.innov_var,
                               data_loadings=self.data_loadings)

        return model

    @property
    def num_periods(self):
        return self.imeans.shape[0]


# noinspection PyTypeChecker
class IntegratedVolatility:
    """ This class contains methods to hold the realized volatilities and update its state.   """

    # noinspection PyTypeChecker
    def __init__(self, dynamic_model=None, ivols=None, ivol_coeff=None, data=None, gamma=None, num_periods=None,
                 num_lags=1, sampler_cov=None, ranges=None, alpha=None, tau_h=None, prob_kernel_weights=None,
                 tau_alpha=None):

        if dynamic_model is not None:
            self._dynamics_model = dynamic_model
            self._ivols = np.exp(dynamic_model.regressand.ravel())

        else:
            if ivols is not None:
                if np.isscalar(ivols) and num_periods is not None:
                    self._ivols = np.full(num_periods, fill_value=ivols, dtype=np.double)
                self._ivols = np.asarray(ivols, dtype=np.double).ravel()
            elif data is not None:
                self._ivols = np.ascontiguousarray(np.asarray(data).shape[1] * np.var(data, axis=1), np.float64)
            else:
                raise ValueError('Either data or ivols must be specified.')

            ivol_coeff = (np.atleast_2d(ivol_coeff).T if ivol_coeff is not None else np.atleast_2d([.9, 0, 0]).T)

            alpha = alpha if alpha is not None else np.zeros((self.time_dim - (ivol_coeff.shape[0] - 1), 1))
            tau_h = tau_h if tau_h is not None else 10
            tau_alpha = tau_alpha if tau_alpha is not None else 1
            gamma = gamma if gamma is not None else np.zeros(self.time_dim - (ivol_coeff.shape[0] - 1))
            prob_kernel_weights = prob_kernel_weights if prob_kernel_weights is not None else 1
            regressor = mcmc.create_lag_matrix(np.log(self.ivols), lags=ivol_coeff.shape[0] - 1, prepend_ones=True)
            regressand = np.log(self.ivols[ivol_coeff.shape[0] - 1:])
            self._dynamics_model = CondDirichletProcessMix(alpha=alpha, beta=ivol_coeff.T, gamma=gamma,
                                                           tau_vec=tau_h,
                                                           tau_alpha=tau_alpha, A_weights=prob_kernel_weights,
                                                           regressor=regressor, regressand=regressand)

        self.ranges = np.ones(self.time_dim) if ranges is not None else np.asarray(ranges)

        self.sampler_cov = sampler_cov if sampler_cov is not None else .2 * np.std(self.ivols) * np.eye(2)
        self.sampler_scale = .5

        self.acceptance_rate = np.array(0)
        self.counter = 0

        # TODO Set the sample cov.

    def __call__(self, data, prior, imeans=0):
        """
        Updates the state and returns a draw from the posterior.

        Parameters
        ----------
        data : 2d ndarray of floats
            The data organized into time_dim rows and num_obs columns.

        Returns
        -------
            dictionary of draws.

        """
        draw = {}
        self.counter += 1

        ivols_draw = self.draw_ivols(data=data, imeans=imeans)
        self.acceptance_rate = ivols_draw['ivols_accepted']
        self.ivols = ivols_draw['ivols']
        draw.update({'ivols_accepted': np.asarray([self.acceptance_rate]).copy(), 'ivols': self.ivols.copy()})

        dynamics = dict(zip(['alpha', 'gamma', 'beta', 'tau_alpha', 'A_weights', 'tau_alpha'],
                            self._dynamics_model(prior['dynamics_model'])))

        draw.update(dynamics)

        return draw

    # TODO Stop ignoring the likelihood part of the model.
    # noinspection PyTypeChecker,PyTypeChecker
    def draw_ivols(self, data, imeans=0):
        """
        Draws the integrated volatility.


        It uses the ivol_log_prior argument as the prior function and the likelihood as described in the notes.
        Parameters
        ----------

        data : 2d array of floats

        imeans : float or array-like, optional
            Gives the value of the mean in each day.
        Returns
        -------
        results : dict
            Contains a draw of the posterior parameters.
        """

        results = {}
        num_obs_per_day = data.shape[1]
        imeans = (np.full(self.time_dim, imeans, dtype=np.float64) if np.isscalar(imeans)
                  else imeans.astype(np.float64))

        shape = num_obs_per_day / 2 - 1

        def drawing_func(x):
            return stats.invgamma.rvs(a=shape, scale=(num_obs_per_day / 2 * x))

        day_it = zip(data, imeans / num_obs_per_day)

        draw = np.asarray([drawing_func(np.sum((day - mean) ** 2)) for day, mean in day_it])

        draw *= self.ranges
        results.update({'ivols': np.squeeze(draw), 'ivols_accepted': True})

        return results

    def forecast(self, forecast_dim, prior):

        dynamic_model = cdpm.forecast(self._dynamics_model, forecast_dim, prior)
        data = np.exp(dynamic_model.regressand[-forecast_dim:])

        volatility_model = IntegratedVolatility(dynamic_model=dynamic_model, data=data)
        return volatility_model

    def forecast_data(self, forecast_dim, prior):

        log_vols = self.forecast(forecast_dim, prior).ivols

        return np.exp(log_vols)

    @property
    def ivols(self):
        return self._ivols

    @ivols.setter
    def ivols(self, val):
        self._ivols = np.asarray(val)
        self._dynamics_model.regressor = self.regressor
        self._dynamics_model.regressand = self.regressand

    @property
    def coeffs(self):
        return self._dynamics_model.coeffs

    @property
    def num_lags(self):
        return self.coeffs.shape[1]

    @property
    def log_vol_uncond_mean(self):
        probability_weights = np.mean(self._dynamics_model, axis=0)
        comp_means = [coeff[0] / (1 - np.sum(coeff[1:])) for coeff in self.coeffs]

        return np.dot(comp_means, probability_weights)

    @property
    def time_dim(self):
        return self.ivols.shape[0]

    @property
    def regressor(self):
        return mcmc.create_lag_matrix(np.log(self.ivols), lags=self.num_lags, prepend_ones=True)

    @property
    def regressand(self):
        return np.log(self.ivols[self.num_lags:])


# noinspection PyTypeChecker
class PreaveragedIntegratedVol:
    def __init__(self, data):
        """
        Setups the preaveraged estimator using the default weights recommended in Hatch Podolskij (2015)
        Parameters
        ----------
        data
        Returns
        -------
        """
        self._data = np.atleast_2d(data)
        self.theta = .4
        # Make sure this isn't looking at the last elements.
        weights = self.weight(np.arange(1, self.kn + 1))
        # Since kn is even, we can use and we are using the triangular g function, we can use formula given in JLMPV
        self.preaveraged_rtns = np.asarray([self.data[:, i:i + self.kn] @ weights
                                            for i in range(self.num_periods - self.kn)]).T

        self.psi1 = self.kn * np.sum((self.weight(j + 1) - self.weight(j)) ** 2 for j in range(1, self.kn + 1))
        self.psi2 = self.kn ** (-1) * np.sum(self.weight(j) ** 2 for j in range(1, self.kn))

        self.Phi11 = self.kn * (np.sum([self.phi1(j) ** 2 for j in range(self.kn)]) - .5 * self.phi1(0) ** 2)
        self.Phi22 = self.kn ** (-3) * (np.sum(self.phi2(j) ** 2 for j in range(self.kn))) - .5 * self.phi2(0) ** 2
        self.Phi12 = self.kn ** (-1) * (np.sum(self.phi1(j) * self.phi2(j) for j in range(self.kn)))

        self.variances = self.compute_variances()
        self.means = self.compute_means()

    def __call__(self):
        return self.means, self.variances

    def compute_variances(self):
        """
        Computes the variances given using the formula in the paper.

        Returns
        -------
        ndarray
        """

        chi = (np.abs(self.preaveraged_rtns[:, :-self.kn]) *
               np.abs(self.preaveraged_rtns[:, self.kn:]))
        # since l = j I simplify the formula in PV2009.  I also take absolute values. This will not affect
        # The math at all, and shouldn't substantively affect the results either because there are in the case I am
        # considering here 3 / 1383 values.
        w_ij = np.abs(np.sum([chi[:, m] * (np.sum(chi[:, m:m + 2 * self.kn - 1], axis=1) -
                                           (2 * self.kn - 1) * chi[:, m + 2 * self.kn - 1])
                              for m in range(self.num_periods - 4 * self.kn + 1)], axis=0))

        w_ij *= 2 * self.num_periods ** (-.5)

        return w_ij

    def compute_means(self):
        """
        Computes the adjusted weights as defined in the paper.

        Returns
        -------
        """
        preaveraged_est = np.sum(np.abs(self.preaveraged_rtns[:, self.kn:] * self.preaveraged_rtns[:, :-self.kn]),
                                 axis=1)
        real_vols = np.sum(self.data ** 2, axis=1)
        preaveraged_est_weight = (np.sqrt(self.num_periods) /
                                  ((self.num_periods - 2 * self.kn + 2) * self.theta * self.psi2 * (2 / np.pi)))
        real_vols_weight = self.psi1 / (self.num_periods * 2 * self.theta ** 2 * self.psi2)
        preweight = (1 - real_vols_weight) ** (-1)

        means = preweight * (preaveraged_est_weight * preaveraged_est - real_vols_weight * real_vols)

        return np.maximum(0, means)

    def weight(self, val):
        true_vals = val / self.kn
        return np.maximum(0, np.minimum(true_vals, 1 - true_vals))

    def phi1(self, j):
        return np.sum([(self.weight(i - 1) - self.weight(i)) * (self.weight(i - (j + 1)) - self.weight(i - j))
                       for i in range(j + 1, self.kn + 1)])

    def phi2(self, j):
        return np.sum(self.weight(i) * self.weight(j) for i in range(j + 1, self.kn + 1))

    @property
    def num_periods(self):
        return self.data.shape[1]

    @property
    def data(self):
        return self._data

    @property
    def kn(self):
        val = self.theta * np.sqrt(self.num_periods)
        return_val = int((val // 2) * 2)
        return return_val


def simulate_ivols(coeff, prior, time_dim=None, alpha=None, kernel_precn=None, kernel_weight=None, kernel_scale=None,
                   initial_ivols=None):

    coeff = np.atleast_2d(coeff).T

    # I am assuming I am only predicting one thing here
    # TOOD switch this.
    initial_ivols = np.exp(np.random.standard_normal(coeff.shape[0] + coeff.shape[1]))

    alpha = alpha if alpha is not None else 1
    kernel_precn = kernel_precn if kernel_precn is not None else 1
    kernel_weight = kernel_weight if kernel_weight is not None else 1
    kernel_scale = kernel_scale if kernel_scale is not None else 1
    time_dim = time_dim if time_dim is not None else 1

    dynamic_model = cdpm.simulate(alpha=alpha, beta=coeff, tau_h=kernel_precn,
                                  tau_alpha=kernel_scale, A_h=kernel_weight,
                                  regressor=np.log(initial_ivols[:-1]), regressand=np.log(initial_ivols[-1]),
                                  time_dim=time_dim, prior=prior)

    volatility_model = IntegratedVolatility(dynamic_model=dynamic_model)

    return volatility_model


# noinspection pytypechecker
def simulate_gp_model(prior, ivol_coeff=None, alpha=None, imean_innov_var=1, ivol_mean=None, kernel_precn=1,
                      kernel_scale=1, num_periods=100, num_obs_per_day=100, imeans=None, imeans_coeff=None,
                      initial_ivols=None, kernel_weight=1, initial_imean=None, leverage=False,
                      intra_day_log_vol_var=None):
    """ simulates the gaussian process model

    parameters
    ----------
    kernel_weight
    alpha
    kernel_precn
    prior
    kernel_scale
    ivol_mean
    intra_day_log_vol_var
    imeans_coeff
    imeans
    num_obs_per_day
    num_periods
    imean_innov_var
    ivol_coeff
    leverage
    initial_ivols
    initial_imean
    """
    ivol_coeff = np.asarray(ivol_coeff)
    imean_innov_var = np.atleast_2d(imean_innov_var if imean_innov_var is not None else 1).astype(np.float64)
    if ivol_mean is not None:
        ivol_mean = np.ravel(ivol_mean)
    elif initial_ivols is not None:
        ivol_mean = np.mean(np.log(initial_ivols))
    else:
        ivol_mean = 0

    ivol_coeff = ivol_coeff if ivol_coeff is not None else np.exp(prior['comp']['coeff']['mean'])
    alpha = alpha if alpha is not None else 0

    ivol_model = simulate_ivols(coeff=ivol_coeff, prior=prior['ivols'], time_dim=num_periods, alpha=alpha,
                                kernel_precn=kernel_precn, kernel_weight=kernel_weight, kernel_scale=kernel_scale,
                                initial_ivols=initial_ivols)

    return_shape = num_periods, num_obs_per_day

    if intra_day_log_vol_var is not None:
        vols = np.exp(
            np.broadcast_to(np.log(ivol_model.ivols / num_obs_per_day).ravel(), (num_obs_per_day, num_periods)).T +
            np.sqrt(intra_day_log_vol_var) * np.random.standard_normal(return_shape) -
            (intra_day_log_vol_var / 2))

    else:
        vols = np.broadcast_to(ivol_model.ivols.ravel() / num_obs_per_day, (num_obs_per_day, num_periods)).T
    if imeans is None:
        imeans_coeff = np.zeros(2 + leverage) if imeans_coeff is None else np.asarray(imeans_coeff)
        imeans_uncond_mean = (
            (imeans_coeff[0] + imeans_coeff[-1] * np.mean(ivol_model.ivols)) / (1 - np.sum(imeans_coeff[1:-1]))
            if leverage else imeans_coeff[0] / (1 - np.sum(imeans_coeff[1:])))
        initial_imean = (np.ravel(initial_imean).astype(np.float64) if initial_imean is not None
                         else np.full(imeans_coeff.size - 1 - leverage, imeans_uncond_mean, dtype=np.float64))
        if leverage:
            imeans = forecast_imeans(num_periods, imeans_coeff, imean_innov_var, starting_point=initial_imean,
                                     external_regressors=np.log(ivol_model.ivols) - ivol_mean)
        else:
            imeans = forecast_imeans(num_periods, imeans_coeff, imean_innov_var, starting_point=initial_imean)
    else:
        imeans = np.ravel(imeans)

    data = compute_data(vols, imeans, num_obs_per_day)

    simulated_model = GPModel(data=data, ivol_coeff=ivol_coeff, imean_innov_var=imean_innov_var,
                              imeans=imeans[-num_periods:], ivols=ivol_model.ivols, imean_coeff=imeans_coeff,
                              leverage=leverage)
    return simulated_model


def compute_data(ivols, imeans, num_obs_per_day):
    """
    Computes the data given the integrated means and integrated volatilities.
    Parameters
    ----------
    imeans
    num_obs_per_day
    ivols

    Returns
    -------
    data
    """
    # TODO Check if I am dividing scaling the high-frequency increments by the right amount.
    num_periods = ivols.shape[0]
    means = np.broadcast_to(imeans[-num_periods:].ravel() / num_obs_per_day, (num_obs_per_day, num_periods)).T
    data = means + np.random.standard_normal((num_periods, num_obs_per_day)) * np.sqrt(ivols / num_obs_per_day)
    return data


def forecast_imeans(horizon, imean_coeff, innov_var, starting_point=None, external_regressors=0):
    """
    Rolls the model forward to create a forecast of the model.

    Parameters
    ----------
    external_regressors
    starting_point
    innov_var
    imean_coeff
    horizon : positive int
    ivol_coeff, 1d array-like, optional
        If ivol_forecast is given, ivol_coeff must be given as well.

    Returns
    -------
    future_imeans : 2d ndarray
    """

    future_imeans = list(starting_point)
    # noinspection PyTypeChecker

    if external_regressors is 0 or external_regressors is None:
        num_lags = imean_coeff.size - 1
        for h in range(horizon):
            future_imeans.append(np.squeeze(imean_coeff[0] +
                                            np.flipud(imean_coeff[1:]).dot(future_imeans[-num_lags:]) +
                                            np.sqrt(innov_var) * np.random.standard_normal()))
    else:
        num_lags = imean_coeff.size - 2
        # noinspection PyTypeChecker
        external_regressors = np.ravel(external_regressors)
        for h in range(horizon):
            future_imeans.append(np.squeeze(imean_coeff[0] + imean_coeff[-1] * external_regressors[h] +
                                            np.flipud(imean_coeff[1:-1]).dot(future_imeans[-num_lags:]) +
                                            np.sqrt(innov_var) * np.random.standard_normal()))

    imeans = np.squeeze(future_imeans[num_lags:])

    return imeans


def forecast_draws(estimates, model, horizon, prior):
    """
    Creates a model with the parameters given by the estimates and forecasts it forward.
    Parameters
    ----------
    estimates
    model
    horizon
    prior

    -------
    """
    current_model = GPModel(data=model.data, ivol_coeff=estimates['ivol_coeff'],
                            imean_innov_var=estimates['imean_innov_var'],
                            imeans=estimates['imeans'], data_loadings=estimates['data_loadings'],
                            alpha=estimates['alpha'], tau_h=estimates['tau_h'],
                            prob_kernel_weights=estimates['prob_kernel_weights'],
                            tau_alpha=estimates['tau_alpha'],
                            ranges=model.ranges, num_lags=model.num_lags,
                            sampler_cov=model.sampler_cov, leverage=model.leverage)

    forecasted_imeans, forecasted_ivols, forecasted_data = current_model.forecast_draws(num_periods=horizon,
                                                                                        prior=prior)

    return forecasted_imeans, forecasted_ivols, forecasted_data
