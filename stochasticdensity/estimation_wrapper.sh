#!/bin/bash 
unset modules
# Exports environmental variables
#$ -V

# Execute from current directory 
#$ -cwd

#Specifies that MPI will be used with 24 workers
#$ -pe openmp 10

# Requests more Ram
#$ -l m_mem_free=12G


#Join output and erro files 
#$ -j y
###############################
###############################
python ./estimation.py
