""" This module contains a number of functions that can be used to clean the data. """
# noinspection PyPackageRequirements
import numpy as np
# noinspection PyPackageRequirements
from scipy.optimize import minimize
from bayesiankalman import kalmanfilter as kf
import warnings
# noinspection PyPackageRequirements
import pandas as pd
# noinspection PyPackageRequirements
import statsmodels.tsa.api as tsa


def arima_filter(data, num_ar_lags=0, num_diff=0, num_ma_lags=0):
    """
    This function takes the data and runs it through an arima filter. It returns the residuals from that regression.

    I am assuming that the model does not have any deterministic part. If a pandas dataframe is given, it will return a
    dataframe as well with the correct index.

    Parameters
    ----------
    data : 1d array-like
    num_ma_lags : positive int
    num_ar_lags : positive int
    num_diff : positive int

    Returns
    -------
    filtered data: 1d numpy array

    The data filtered by an arima process with the given order.

    """

    order = (num_ar_lags, num_ma_lags, num_diff)
    data = pd.DataFrame(data)

    arima_model = tsa.ARIMA(data, order=order)

    arima_results = arima_model.fit()

    return arima_results.resid


# noinspection PyTypeChecker,PyArgumentList
def remove_ma_noise(data):
    """
    This function filters out moving average noise from the model using a kalman smoother.

    It assumes Y_obs = Y + e_t - e_{t-1}

    Parameters
    ----------
    data : 1d array-like
        The data to filter.

    Returns
    -------
    filtered_data :
        1d numpy array : The filtered data

    noise_var : positive double
        The estimated variance of the


    """
    data = np.asanyarray(data)
    data_var = 1 / 2 * np.var(data)
    state_var = 1 / 3 * np.var(data)
    data_mean = np.mean(data)

    result = minimize(
        fun=lambda x: - kf.kalman_log_like(data, **make_ma_statespace_representation(x)),
        x0=[data_var, state_var, data_mean],
        bounds=[(np.var(data) / 1000, 10 * np.var(data)), (np.var(data) / 1000, 10 * np.var(data)),
                (np.mean(data) - 10 * np.std(data), np.mean(data) + 10 * np.std(data))]
    )

    # noinspection PyArgumentList
    states, _, _ = kf.kalman_filter(data=data, **make_ma_statespace_representation([1, 1, 0]))

    filtered_data = data - np.array(states @ np.array([1, -1])).reshape(data.size, 1)

    return filtered_data, result.x


def make_ma_statespace_representation(params):
    """
        Computes the statespace representation of the model

        Parameters
        ----------
        params : A tuple
            data_var, state_var, data_mean

        Returns
        -------
        filtered_data: ndarray of floats
                       The statespace representation of the model as a dictionary of numpy arrays.

        result.x: positive float
                   The variance of the measurement error.

        """

    arg_dict = {'state_innov_var': np.array([[0, 0], [0, params[1]]]), 'data_innov_var': np.asarray(params[0]),
                'state_trans': np.array([[0, 1], [0, 0]]), 'data_mean': np.asarray(params[2]),
                'data_loadings': np.array([1, -1])}

    return arg_dict


def downsample(data, num):
    """
    This function takes the data and returns a equally-spaced  selection  of size num_datapoints

    Parameters
    ----------
    data : pandas DataFrame
        The data to subset.
    num : positive int
        The number of points in the selection.

    Returns
    -------
    subsetted_data : pandas DataFrame
        The data to return

    """
    try:
        subsetted_data = data.iloc[np.linspace(0, data.size, num=num, endpoint=False, dtype='int64')]
    except IndexError:
        warnings.warn('You cannot downsample to a larger value. The index of the observation in this data subset is' +
                      str(data.index[0]))
        subsetted_data = data

    return subsetted_data
