import numpy as np
from scipy import stats
import pandas as pd
import os
from stochasticdensity import gaussian_process_model as gpm
from bayesiankalman import mcmc
from tqdm import tqdm
from multiprocessing import Pool
from functools import partial


def imean_prior(imean):
    imean = np.ravel(imean)
    priorval = stats.multivariate_normal.pdf(x=np.atleast_2d(imean), mean=np.zeros(imean.size),
                                             cov=.0625 * np.eye(imean.size)) * float(imean[0] >= 0)

    return priorval


if __name__ == "__main__":

    simulate = True
    result_file = '../results/cond_gp_model.tmp.hdf'
    nsims = 2
    twenty_sec = False
    real_vol_likelihood = True

    horizon = 0
    pre_forecast_period_length = 1250
    regressand_dim=1
    regressor_dim=2

    prior = {
        'ivols': {"comp": {"coeff": {"mean": list(np.ravel(np.zeros((regressand_dim)))),
                            "cov" : list(np.ravel(np.eye(regressand_dim)))},
                  "precn": {"shape": [1] , "scale": [10]}},
                    "alpha": {"A_h": {"lambda": [.1], "eta": [.5], "eta_1": [.5], "d_rescaling": [2]},
                   "tau_alpha": {"nu": [2]}}
            },
        'imeans': {'imean_coeff': imean_prior,
                   'data_loadings': {'mean': .0031, 'cov': 2.5e-5, 'min': 0}
                   }
        }


    if simulate:
        if twenty_sec:
            num_obs_per_day = 450 * 16
        else:
            num_obs_per_day = 450

        starting_date = '2005-01-01'
        num_days = 20
        assert (86400.0 / num_obs_per_day).is_integer(), 'There needs to tbe the same number of observations every day.'

        date_index = pd.date_range(starting_date, periods=num_obs_per_day * num_days,
                                   freq=(str(int(86400 / num_obs_per_day)) + 's')).values.astype('datetime64[s]')

        simulated_model = gpm.simulate_gp_model(prior=prior, ivol_coeff=np.array([-.2, -.15]),
                                                imean_innov_var=3e-7, ivol_mean=-10, num_periods=num_days,
                                                num_obs_per_day=num_obs_per_day, imeans_coeff=[0, 0], leverage=False,
                                                intra_day_log_vol_var=0)

        entire_data = pd.Series(simulated_model.data.ravel(), date_index)
    else:
        dates1 = '2004-01'
        dates2 = '2009-12'
        if twenty_sec:
            data_source = '../data/spy_2001_2013.1750.hdf'
        else:
            data_source = '../data/spy_2001_2013.90.hdf'
        entire_data = pd.read_hdf(data_source, 'table').loc[dates1:dates2]
        date_index = entire_data.index.values.astype('datetime64[s]')
        num_obs_per_day = np.amax(entire_data.groupby(pd.TimeGrouper('D')).count().values)

    def fracdiff_prior(x):
        return np.asscalar(stats.norm.pdf(x, loc=.30, scale=.25) * stats.uniform.pdf(x, loc=0, scale=1))



    grouped_data = entire_data.groupby(pd.TimeGrouper('D'))

    day_index = grouped_data.filter(lambda x: x.size == num_obs_per_day).resample('D').mean(
    ).dropna().index.values.astype('datetime64[s]')
    data_df = pd.DataFrame([np.ravel(group.values) for _, group in grouped_data if group.size == num_obs_per_day],
                           index=day_index)

    timestamps = np.asarray([[np.datetime64(time) for time in group.index] for date, group in
                             grouped_data if group.size == num_obs_per_day])
    ranges = (np.asarray([np.timedelta64(np.amax(day) - np.amin(day), 's') for day in timestamps]) /
              np.timedelta64(1, 's'))
    ranges /= np.mean(ranges)
    ranges_df = pd.DataFrame(ranges, index=day_index)

    # noinspection PyTypeChecker
    def estimate(day, only_forecast=False):
        day_np = pd.to_datetime(day).strftime(format='%B_%d_%Y')
        result_vals = {day_np: {'dates': data_df.loc[:day].index.values.astype(
            'datetime64[D]').astype(np.float64)}}

        data_loadings = prior['imeans']['data_loadings']['mean']

        gp_model = gpm.GPModel(data=data_df.loc[:day], ivol_coeff=np.array([-.15, -.05]), imean_innov_var=1,
                               imean_coeff=[0, 0], data_loadings=data_loadings,
                               ranges=ranges_df.loc[:day], leverage=True)

        parameter_estimates = mcmc.estimate_model(gp_model, nsims, prior)
        if not only_forecast:
            result_vals[day_np].update(parameter_estimates)

        if horizon > 0:
            forecasted_data, forecasted_ivols, forecasted_imeans = gpm.forecast_draws(parameter_estimates, gp_model,
                                                                                      horizon, prior=prior)
            forecast_dict = {'forecasted_data': forecasted_data, 'forecasted_imeans':
                             np.squeeze(forecasted_imeans), 'forecasted_ivols': forecasted_ivols}
            result_vals[day_np].update(forecast_dict)

        return result_vals


    try:
        os.remove(result_file)
    except FileNotFoundError:
        pass

    print('The number of days is ' + str(len(day_index)))
    mcmc.save_hdf({'values': entire_data.values, 'index': date_index.astype(np.float64)}, result_file, group='data')
    mcmc.save_hdf(estimate(day_index[-1], only_forecast=False), result_file, group='results')

    if horizon > 0:
        forecast_estimate = partial(estimate, only_forecast=True)
        with Pool(10) as p:
            for results in tqdm(p.imap_unordered(forecast_estimate, day_index[pre_forecast_period_length:-1])):
                mcmc.save_hdf(results, result_file, group='results')

    if simulate:
        # noinspection PyUnboundLocalVariable
        mcmc.save_hdf({'values': simulated_model.ivols, 'index': day_index.astype(np.float64)}, result_file,
                      group='ivols')
        mcmc.save_hdf({'values': simulated_model.imeans, 'index': day_index.astype(np.float64)}, result_file,
                      group='imeans')

    print('Everything seemed to work!')
